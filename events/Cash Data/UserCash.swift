//
//  UserCash.swift
//  events
//
//  Created by Macbook Pro on 2/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//
//

import Foundation
import SwiftyJSON
import Realm
import RealmSwift

class UserCash: Object {
    
    var username = RealmOptional<Int>()
    var email =  RealmOptional<Int>()
    @objc dynamic var phone:String?
    var activation = RealmOptional<Int>()
    var image = RealmOptional<Int>()
    @objc dynamic var api_key:String?
    var verification_code = RealmOptional<Int>()
    
    required  init(_ data:JSON) {
        super.init()
        self.email = RealmOptional(data["email"].int)
        self.phone = data["phone"].string
        self.verification_code = RealmOptional(data["verification_code"].int)
        self.image = RealmOptional(data["image"].int)
        self.api_key = data["api_key"].string
    }
  
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value:value , schema:schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm , schema:schema)
    }
    
}
