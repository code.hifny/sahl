//
//  Sponsers.swift
//  events
//
//  Created by Macbook Pro on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import Realm
import RealmSwift

class SponsersCash: Object {
    
    var id = RealmOptional<Int>()
    @objc dynamic var image:String?
    @objc dynamic var category_name_ar:String?
    @objc dynamic var category_name_en:String?
    var type = RealmOptional<Int>()
    var category_id = RealmOptional<Int>()
    @objc dynamic var updated_at:String?
    @objc dynamic var created_at:String?

    
    required  init(_ data:JSON) {
        super.init()
        self.id = RealmOptional(data["id"].int)
        self.category_name_ar = data["category_name_ar"].string
        self.category_name_en = data["category_name_en"].string
        self.category_id = RealmOptional(data["category_id"].int)
        self.type = RealmOptional(data["type"].int)
        self.image = data["image"].string
        self.updated_at = data["updated_at"].string
        self.created_at = data["created_at"].string
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value:value , schema:schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm , schema:schema)
    }
}
