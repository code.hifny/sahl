//
//  GoingCash.swift
//  events
//
//  Created by Macbook Pro on 2/18/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import Realm
import RealmSwift

class GoingCash: Object {
    
    var id = RealmOptional<Int>()
    @objc dynamic var username:String?
    @objc dynamic var email:String?
    @objc dynamic var phone:String?
    @objc dynamic var company:String?
    @objc dynamic var job_title:String?
    var activation = RealmOptional<Int>()
    @objc dynamic var twitter:String?
    @objc dynamic var linkedin:String?
    var type = RealmOptional<Int>()
    var social_id = RealmOptional<Int>()
    @objc dynamic var social_type:String?
    @objc dynamic var image:String?
    @objc dynamic var bio:String?
    var reset_password_code = RealmOptional<Int>()
    @objc dynamic var api_token:String?
    @objc dynamic var updated_at:String?
    @objc dynamic var created_at:String?
    @objc dynamic var temp_phone:String?
    var reset_phone_code = RealmOptional<Int>()
    
    required  init(_ data:JSON) {
        super.init()
        self.id = RealmOptional(data["id"].int)
        self.username = data["username"].string
        self.email = data["email"].string
        self.phone = data["phone"].string
        self.activation = RealmOptional(data["activation"].int)
        self.type = RealmOptional(data["type"].int)
        self.image = data["image"].string
        self.reset_password_code = RealmOptional(data["reset_password_code"].int)
        self.api_token = data["api_token"].string
        self.updated_at = data["updated_at"].string
        self.created_at = data["created_at"].string
        self.temp_phone = data["temp_phone"].string
        self.reset_phone_code = RealmOptional(data["reset_phone_code"].int)
        self.company = data["company"].string
        self.job_title = data["job_title"].string
        self.twitter = data["twitter"].string
        self.linkedin = data["linkedin"].string
        self.social_id = RealmOptional(data["social_id"].int)
        self.social_type = data["social_type"].string
        self.bio = data["bio"].string
    }
    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value:value , schema:schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm , schema:schema)
    }

}
