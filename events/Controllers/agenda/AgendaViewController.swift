//
//  AgendaViewController.swift
//  events
//
//  Created by abdelrahman on 2/21/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift
import UserNotifications

class AgendaViewController: GarlandViewController  {
  
    private let header: HeaderView = UIView.loadFromNib(withName: "HeaderView")!
    fileprivate let scrollViewContentOffsetMargin: CGFloat = -150.0
    fileprivate var headerIsSmall: Bool = false
    var index = 0
//    var shared:AgendaViewController { return AgendaViewController()}
    var agendaItems:[AgendaItem]?
    
   
    override func viewDidLoad() {
        localSessionsNotify()
        GarlandConfig.shared.backgroundHeaderColor = UIColor.fromGradientWithDirection(.leftToRight, frame: self.view.frame, colors:[ Color.blueForGradiantColor,Color.greenForGradiantColor])!
        
        super.viewDidLoad()
        
        nextViewController = { _ in
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "AgendaViewController") as! AgendaViewController
            controller.index = self.index
            controller.agendaItems = self.agendaItems
            return controller
        }
        config()
        setupAndCheckDirection()
        setupHeader(header)
        
        
    }
    func localSessionsNotify(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound , .alert]) { (dane, error) in
            if error != nil{
                print("Notification Error")
            }else{
                print("Notification Sucess")
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        garlandCollection.reloadData()
//        loadData(false)
//        setupAndCheckDirection()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        setupAndCheckDirection()
//        garlandCollection.reloadData()
    }
    
    func setupAndCheckDirection() {
        print("agendaItems?.count",agendaItems?.count ?? 0)
        if self.direction != nil
        {
            if self.direction == .right
            {
                if self.index == 0
                {
                    self.index = (agendaItems?.count ?? 1) - 1
                }
                else
                {
                    self.index -= 1
                }
            }
            else
            {
                if self.index != (agendaItems?.count ?? 1) - 1
                {
                    self.index += 1
                }
                else
                {
                    self.index = 0
                }
            }
            print("self.index",self.index)
            
            self.header.titleLabel.text = agendaItems?.count ?? 0 > 0 ? self.agendaItems?[self.index].date : "agenda not found".locale
        }else{
            if let agenda = AgendaUtil.load(){
                self.agendaItems = agenda
                if self.agendaItems?.count ?? 0 > 0 {
                    self.header.titleLabel.text = self.agendaItems?[0].date
                }
                self.garlandCollection.reloadData()
                let date = todayFormated(time: Date())
                print("Now" , date)
                print("self.index" , self.index)

                DispatchQueue.main.async {
                    if let index = agenda.index(where: {$0.date == date}) , index != self.index {
                        print("index222====>",index)
                        self.index = index
                        self.performTransition(direction: .right)
                    }
                }
                loadData(false)
            }else{
                loadData()
            }
        }
    }
    
    func config() {
        let nib = UINib(nibName: "CollectionCell", bundle: nil)
        garlandCollection.register(nib, forCellWithReuseIdentifier: "Cell")
        garlandCollection.delegate = self
        garlandCollection.dataSource = self
        GarlandConfig.shared.headerSize = CGSize(width: round(UIScreen.main.bounds.width * 0.8) + 8, height: 60)
        
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst {
            self.StartLoading()
            
        }
        Requests.instance.request(link: "agenda".route, method: .get,parameters: ["api_token":UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    DispatchQueue.main.async {
                        let allData = response.data!["data"].dictionaryObject ?? [:]
                        self.agendaItems = allData.map({ (arg: (key: String, value: Any)) ->  AgendaItem in
                            let (key, value) = arg
                            return AgendaItem(key , JSON(value as? Array ?? []))
                        })
                        self.agendaItems?.sort{($0.date) ?? "" < ($1.date) ?? ""}
                        AgendaUtil.save(self.agendaItems)
                        
                        let date = self.todayFormated(time: Date())
                        print("Now" , date)
                        
                        if let index = self.agendaItems?.index(where: {$0.date == date}) {
                            print("index====>",index)
                            self.index = index
                            self.performTransition(direction: .right)
                        }
                        
                        self.StopLoading()
                        self.garlandCollection.reloadData()
                    }
                }
            }
        }
    }
    
    func todayFormated(time: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let result =  dateFormatter.string(from: time)
        return result
    }

}
extension AgendaViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return agendaItems?.count ?? 0 > 0 ? agendaItems?[index].agenda?.count ?? 0 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CollectionCell else { return UICollectionViewCell() }
    
        self.agendaItems?[index].agenda?.sort{($0.formatted_time_from ?? "") < ($1.formatted_time_from ?? "")}

        
        cell.config((agendaItems?[index].agenda?[indexPath.row])!)
        cell.notificationBtn.tag = indexPath.row
        cell.notificationBtn.addTarget(self, action: #selector(notificationAction), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCardIndex = indexPath
        let cardController = UserCardViewController.init(nibName: "UserCardViewController", bundle: nil)
        cardController.agenda = agendaItems?[index].agenda?[indexPath.row]
        self.present(cardController, animated: true, completion: nil)
    }
    
    @objc func notificationAction(_ sender:UIButton) {
        // notify user in time for session
        StartLoading()
        
        let sessionId = agendaItems?[index].agenda?[sender.tag].id ?? 0
        print("sessionId",sessionId)
        Requests.instance.request(link: Config.apiUrl + "user/session/\(sessionId)/remind", method: .post ,parameters: ["api_token":UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    let sessionId = "\(self.agendaItems?[self.index].agenda?[sender.tag].id ?? 0)"
                    let sessionDateString = (self.agendaItems?[self.index].agenda?[sender.tag].date ?? "") + " " + (self.agendaItems?[self.index].agenda?[sender.tag].time_from ?? "")
                    let sessionTitle = "\(self.agendaItems?[self.index].agenda?[sender.tag].title ?? "")"
                    let sessionDate = sessionDateString.timestamp
                    let sessionDescrip = ""
                    
                    let dateBefore15Minute = Calendar.current.date(byAdding: .minute , value: -15, to: sessionDate)
                    let dateBefore5Minute = Calendar.current.date(byAdding: .minute, value: -5, to: sessionDate)
                    
                    if self.agendaItems?[self.index].agenda?[sender.tag].is_reminded ?? true {
                        self.agendaItems?[self.index].agenda?[sender.tag].is_reminded  = false
                        NotificationsUtil.remove(sessionId)
                        print("session remove ====>",sessionId)
                        AgendaUtil.save(self.agendaItems)
                    }else{
                        self.agendaItems?[self.index].agenda?[sender.tag].is_reminded  = true
                        AgendaUtil.save(self.agendaItems)

                        print("dateBefore15Minute" , dateBefore15Minute!)
                        print("dateBefore5Minute" , dateBefore5Minute!)
                        
                        NotificationsUtil.set(sessionId+"-15", title: sessionTitle, body: sessionDescrip, date: dateBefore15Minute!, userInfo: ["sessionId" : sessionId+"-15" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 15 minutes" , "minutesType": "15" , "created_at" : dateBefore15Minute!  , "status": true , "type" : "local" , "startSession": sessionDateString])


                        NotificationsUtil.set(sessionId+"-5", title: sessionTitle, body: sessionDescrip, date: dateBefore5Minute!, userInfo: ["sessionId" : sessionId+"-5" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 5 minutes" ,"minutesType":"5", "created_at" : dateBefore5Minute! , "status": true , "type" : "local" , "startSession": sessionDateString])
                        
                        let allAgendForNotify = NotificationsUtil.loadNotifications() ?? [] + [(self.agendaItems?[self.index].agenda?[sender.tag])!]
                        NotificationsUtil.saveNotifcation(allAgendForNotify)
                    }
                    self.StopLoading()
                    self.garlandCollection.reloadData()
                    print("response.data",response.data?["data"] ?? [])
                }
                self.StopLoading()

            }
        }
    }
    
    
    
}


