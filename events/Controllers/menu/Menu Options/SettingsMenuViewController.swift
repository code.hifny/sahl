//
//  SettingsMenuViewController.swift
//  events
//
//  Created by Mina Gad on 2/18/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import EasyLocalization
import NVActivityIndicatorView
import BetterSegmentedControl
import UserNotifications

class SettingsMenuViewController: UITableViewController {
    
    
    var languageSegmentControl = BetterSegmentedControl()
    var notificationSegmentControl = BetterSegmentedControl()
    
    
    @IBOutlet weak var changeLangLabel: UILabel!
    @IBOutlet weak var langView: UIView!
    @IBOutlet weak var notifiView: UIView!
    
    var selectedIndexForLanguage: UInt = 1
    var selectedIndexForNotifications: UInt = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navConfig()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        selectedIndexForLanguage =  EasyLocalization.getLanguage() == .en ? 1 : 0
        selectedIndexForNotifications = UInt(UserDefaults.standard.integer(forKey: "selectedNotifiy"))
        setupLangSwitch()
        setupNotificationSwitch()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        checkUserExists()
        
    }
    
    func checkUserExists() {
        if UserUtil.loadUser() == nil {
            let cells = [tableView.visibleCells[0],tableView.visibleCells[1],tableView.visibleCells[3]]
            cells.forEach({ (cell) in
                cell.isHidden = true
                cell.removeFromSuperview()
            })
        }
    }
    
    func setupLangSwitch() {
        languageSegmentControl = BetterSegmentedControl(
            frame: CGRect(x: 0, y: 0, width: 70, height: 30),
            titles: ["ع","EN"],
            index: selectedIndexForLanguage,
            options: [.backgroundColor(Color.greenForGradiantColor),
                      .titleColor(.white),
                      .indicatorViewBackgroundColor(.white),
                      .selectedTitleColor(.black),
                      .cornerRadius(15),
                      .titleFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
                      .selectedTitleFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!)]
        )
        languageSegmentControl.addTarget(self, action: #selector(changeLanguageButton), for: .valueChanged)
        languageSegmentControl.layer.cornerRadius = 15
        languageSegmentControl.clipsToBounds = true
        langView.addSubview(languageSegmentControl)
    }
    func setupNotificationSwitch() {
        notificationSegmentControl = BetterSegmentedControl(
            frame: CGRect(x: 0, y: 0, width: 70, height: 30),
            titles: ["OFF".locale,"ON".locale],
            index: selectedIndexForNotifications, // selected notifi
            options: [.backgroundColor(Color.greenForGradiantColor),
                      .titleColor(.white),
                      .indicatorViewBackgroundColor(.white),
                      .selectedTitleColor(.black),
                      .cornerRadius(15),
                      .titleFont(UIFont(name: "HelveticaNeue", size: 14.0)!),
                      .selectedTitleFont(UIFont(name: "HelveticaNeue-Medium", size: 14.0)!)]
        )
        notificationSegmentControl.addTarget(self, action: #selector(changeNotificationState), for: .valueChanged)
        notificationSegmentControl.layer.cornerRadius = 15
        notificationSegmentControl.clipsToBounds = true
        notifiView.addSubview(notificationSegmentControl)
    }
    
    
    
    @objc func changeLanguageButton() {
        switchLang()
    }
    

    @objc func changeNotificationState() {
        switchNotifications()
        
        if UserDefaults.standard.integer(forKey: "selectedNotifiy") == 1 {
            self.SuccessAlert(message: "Notification is turned on".locale )
        }else{
            self.SuccessAlert(message: "Notification is turned off".locale)
        }
        

//        let size = CGSize(width: 50, height: 50)
//        NVActivityIndicatorView.DEFAULT_COLOR = Color.blueColor
//        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = .black
//        self.startAnimating(size, message: "Waiting...".locale, type: NVActivityIndicatorType.ballBeat )
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
//            self.StopLoading()
//            NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
//        }
    }
    
    func switchLang()
    {
        DispatchQueue.main.async {
            let size = CGSize(width: 50, height: 50)
            NVActivityIndicatorView.DEFAULT_COLOR = Color.blueColor
            NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = .black
            self.startAnimating(size, message: "Waiting...".locale, type: NVActivityIndicatorType.ballBeat )
            
            let currentLanguage = EasyLocalization.getLanguage()
            if currentLanguage == .en {
                self.selectedIndexForLanguage = 0
                EasyLocalization.setLanguage(.ar)
                self.prepareAppFont("NeoSansArabic")
                UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
            }else{
                EasyLocalization.setLanguage(.en)
                UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
                self.selectedIndexForLanguage = 1
                self.prepareAppFont("Helvetica")
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            }
            UserDefaults.standard.synchronize()
            
            if (UserUtil.loadUser() != nil) {
                let lang = EasyLocalization.getLanguage() == .en ? "en" : "ar"
                Requests.instance.request(link: "language".route, method: .post , parameters: ["language":lang, "api_token":UserUtil.loadUser()?.api_token ?? ""]) { (resonse) in
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        self.StopLoading()
                        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                        self.goToHome(true)
                    }
                }
                
            }else{
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                    self.StopLoading()
                    NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
                    self.goToHome(true)
                }
            }
        }
    }
    func switchNotifications()
    {
        print("selectedIndexForNotifications" , selectedIndexForNotifications)
        if selectedIndexForNotifications == 0 {
            selectedIndexForNotifications = 1
            UserDefaults.standard.set(selectedIndexForNotifications, forKey: "selectedNotifiy")
            UIApplication.shared.registerForRemoteNotifications()
            NotificationsUtil.appendAllWithLogin()
        }else {
            selectedIndexForNotifications = 0
            UserDefaults.standard.set(selectedIndexForNotifications, forKey: "selectedNotifiy")
            UIApplication.shared.unregisterForRemoteNotifications()
            NotificationsUtil.removeAll()
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UserUtil.loadUser() == nil && [0,1,3].contains(indexPath.row) ? 0 : UITableViewAutomaticDimension
    }
    
    
}



