//
//  changePhoneNumberViewController.swift
//  events
//
//  Created by Mina Gad on 2/18/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class changePhoneNumberViewController: UIViewController {
    
    @IBOutlet weak var newPhoneNumberTextField: EventsTextField!
    var tempCode = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Change Phone No.".locale
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.tabBarController?.tabBar.isHidden = true
        navConfig()
        newPhoneNumberTextField.text = UserUtil.loadUser()?.phone
        DispatchQueue.main.async {
            self.newPhoneNumberTextField.setIcon(self.newPhoneNumberTextField.rightIcon)
        }

    }
    
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        self.tabBarController?.tabBar.isHidden = false
    //
    //    }
    
    
    @IBAction func updatePhoneNumberButtonPressed(_ sender: EventsButton) {
        view.endEditing(true)
        if validation() {
            StartLoading()
            let parameters = [
                "api_token": UserUtil.loadUser()?.api_token ?? "",
                "phone" : newPhoneNumberTextField.text!,
                ]
            
            Requests.instance.request(link: "requestUpdatePhone".route, method: .post, parameters: parameters, completion: { (response) in
                if response.haveError {
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["status"].bool == true
                    {
                        print(response)
                        self.dismissKeyboard()
                        let code = response.data!["code"].int
                        self.tempCode = code ?? 0
                        self.performSegue(withIdentifier: Config.present_phone_activation_code, sender: code)
                        // TODO:- perform segue.......
                    }
                    else{
                        self.validationShowMessage(response.data!, "phone")
                    }
                }
                self.StopLoading()
            })
            
        }
    }
    //MARK:- update phone number validation
    private func validation() -> Bool {
        
        var fields:[ValidationData] = []
        
        fields.append(
            ValidationData(
                value: newPhoneNumberTextField.text! ,
                name : "new number".locale ,
                type: "required|phone"))
        
        return  Validation.instance.SetValidation(ValidationData: fields)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == Config.present_phone_activation_code {
//            if let destination = segue.destination as? ActiveViewController {
//                destination.activeType = .update_phone
//                destination.phone = newPhoneNumberTextField.text!
//                destination.tempCode = "\(tempCode)"//sender as? String ?? ""
//                
//            }
//        }
//    }
    
}

