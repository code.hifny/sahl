//
//  updatePasswordViewController.swift
//  events
//
//  Created by Mina Gad on 2/18/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class updatePasswordViewController: UIViewController {
    
    @IBOutlet weak var enterOldPasswordTextField: EventsTextField!
    @IBOutlet weak var enterNewPasswordTextField: EventsTextField!
    @IBOutlet weak var reEnterNewPasswordTextField: EventsTextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        navConfig()
        navigationItem.title = "Change Password".locale
        navConfig()


    }
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        self.tabBarController?.tabBar.isHidden = true
    //
    //    }
    //
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        self.tabBarController?.tabBar.isHidden = false
    //
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.enterOldPasswordTextField.setIcon(self.enterOldPasswordTextField.rightIcon)
            self.enterNewPasswordTextField.setIcon(self.enterNewPasswordTextField.rightIcon)
            self.reEnterNewPasswordTextField.setIcon(self.reEnterNewPasswordTextField.rightIcon)
        }

    }
    
    @IBAction func updatePasswordButtonPresses(_ sender: EventsButton) {
        view.endEditing(true)
        
        if validation() {
            StartLoading()
            let parameters = [
                "api_token": UserUtil.loadUser()?.api_token ?? "",
                "old_password": enterOldPasswordTextField.text!,
                "new_password":enterNewPasswordTextField.text!
            ]
            
            Requests.instance.request(link: "updatePassword".route, method: .post, parameters: parameters, completion: { (response) in
                if response.haveError {
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["status"].bool == true
                    {
                        print(response)
                        self.navigationController?.popViewController(animated: true)
                        self.InfoAlert("Success", message: "Update successfully".locale)
                        self.dismissKeyboard()
                    }
                    else
                    {
                        self.validationShowMessage(response.data!, "old_password")
                        self.validationShowMessage(response.data!, "new_password")
                        self.validationShowMessage(response.data!, "message")
                    }
                }
                self.StopLoading()
            })
            
        }
        
    }
    //MARK:- update password validation
    private func validation() -> Bool {
        
        var fields:[ValidationData] = []
        
        fields.append(
            ValidationData(
                value: enterNewPasswordTextField.text! ,
                name : "new password".locale ,
                type: "required|min:6|password"))
        
        fields.append(
            ValidationData(
                value: reEnterNewPasswordTextField.text! ,
                name : "re - new password".locale ,
                type: "required|min:6|password"))
        if Validation.instance.SetValidation(ValidationData: fields) {
            if enterNewPasswordTextField.text != reEnterNewPasswordTextField.text {
                DangerAlert("Match", message: "Worng Confirm Password".locale)
                return false
            }
            return true
        }
        
        return Validation.instance.SetValidation(ValidationData: fields)
    }
}

