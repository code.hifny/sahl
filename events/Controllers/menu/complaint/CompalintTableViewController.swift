//
//  CompalintTableViewController.swift
//  events
//
//  Created by Macbook Pro on 3/4/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class CompalintTableViewController: UITableViewController {
    

    
    var aboutApplication: about!
    var facebook: String!
    var twitter: String!
    var youtube: String!
    
    @IBOutlet weak var contentViewEmai: UIView!
    @IBOutlet weak var contentViewTitle: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var titleCompalint: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var decriptionOFComplaint: EventsTextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Contact Us".locale
        setup()
        getRequest()
        addData()

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func setup(){
        
        // MARK:- Shadow
        contentViewEmai.layer.masksToBounds = false
        contentViewEmai.layer.shadowRadius = 6.0
        contentViewEmai.layer.shadowColor = UIColor.black.cgColor //Color.grayColor.cgColor
        contentViewEmai.layer.shadowOpacity = 0.2
        contentViewEmai.layer.shadowOffset = CGSize(width:0,height:0)
        
        contentViewTitle.layer.masksToBounds = false
        contentViewTitle.layer.shadowRadius = 6.0
        contentViewTitle.layer.shadowColor = UIColor.black.cgColor //Color.grayColor.cgColor
        contentViewTitle.layer.shadowOpacity = 0.2
        contentViewTitle.layer.shadowOffset = CGSize(width:0,height:0)
        
        // MARK:- Shadow
        
        decriptionOFComplaint.clipsToBounds = true
        contentView.clipsToBounds = true
        contentView.layer.masksToBounds = false
        contentView.layer.shadowRadius = 6.0
        contentView.layer.shadowColor =  UIColor.black.cgColor  // Color.grayColor.cgColor
        contentView.layer.shadowOpacity = 0.2
        contentView.layer.shadowOffset = CGSize(width:0,height:0)
    }
    // MARK: - Table view data source
    
    
    @IBAction func sendRequest(_ sender: Any) {
        
        if UserUtil.loadUser()?.api_token == nil{
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            if validation() {
                sendRequest()
            }
        }
        
    }
    
    
    
    @IBAction func openFaceBookAction(_ sender: Any) {

        let url = facebook
        print(url ?? "")
        openLinkWithSafari(link: url ?? "")
    }
    
    @IBAction func openTwitterAction(_ sender: Any) {
        let url = twitter
        print(url ?? "")
        openLinkWithSafari(link: url ?? "")
    }
    
    @IBAction func openYoutubeAction(_ sender: Any) {
        let url = youtube
        print(url ?? "")
        openLinkWithSafari(link: url ?? "")
    }
    
    func validation() -> Bool {
        var fields = [ValidationData]()
        
        fields.append(ValidationData(value:titleCompalint.text! ,name : "Title".locale , type:"required"))
        
        fields.append(ValidationData(value:email.text! ,name : "Email".locale , type:"required|email"))
        
        fields.append(ValidationData(value:decriptionOFComplaint.text! ,name : "Content".locale , type:"required"))
        
        return Validation.instance.SetValidation(ValidationData: fields)
    }
    
    
    // MARK:- Request
    func sendRequest(){
        StartLoading()
        let param:[String: Any] = ["title":titleCompalint.text ?? "",
                                   "email":email.text ?? "" ,
                                   "content": decriptionOFComplaint.text ?? ""]
        
        Requests.instance.request(link: "complaint".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
                    self.titleCompalint.text = ""
                    self.email.text = ""
                    self.decriptionOFComplaint.text = ""
                    self.SuccessAlert(message: "Message Send Sucessfully".locale )
                }
            }
            self.StopLoading()
        }
    }

    func addData()  {
        facebook = aboutApplication.facebook ?? ""
        twitter = aboutApplication.twitter ?? ""
        youtube = aboutApplication.youtube ?? ""
    }
    
    func getRequest()  {
        if AboutUtil.load() != nil {
            self.aboutApplication = AboutUtil.load()
            self.view.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    func loadData(_ isFirst:Bool = true) {
    
        Requests.instance.request(link: "About".route, method: .get,parameters: [:]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let info = about(response.data!["data"]["about"])
                    AboutUtil.save(info)
                    self.aboutApplication = info
                    self.addData()
                    self.tableView?.reloadData()
                }
            }
        }
    }
    
    
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

