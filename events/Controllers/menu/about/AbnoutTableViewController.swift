//
//  AbnoutTableViewController.swift
//  events
//
//  Created by Intcore on 3/27/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class AbnoutTableViewController: UITableViewController {

    @IBOutlet weak var desApplications: UITextView!
    
    var aboutApplication: about!
    var facebook: String!
    var twitter: String!
    var youtube: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "About us".locale
        getRequest()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func addData()  {
        desApplications.text = aboutApplication.content ?? ""
        facebook = aboutApplication.facebook ?? ""
        twitter = aboutApplication.twitter ?? ""
        youtube = aboutApplication.youtube ?? ""
    }
    
    // MARK: - Table view data source
    @IBAction func youtubeAction(_ sender: UIButton) {
        let url = youtube
        print(url ?? "")
        openLinkWithSafari(link: url ?? "")
    }
    @IBAction func twitterAction(_ sender: UIButton) {
        let url = twitter
        print(url ?? "")
        openLinkWithSafari(link: url ?? "" )
        
    }
    
    @IBAction func facebookAction(_ sender: UIButton) {
        let url = facebook
        print(url ?? "")
        openLinkWithSafari(link: url ?? "")
        
    }
    
    
    func getRequest()  {
        if AboutUtil.load() != nil {
            self.aboutApplication = AboutUtil.load()
            self.view.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "About".route, method: .get ,parameters: [:]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let info = about(response.data!["data"]["about"])
                    AboutUtil.save(info)
                    self.aboutApplication = info
                    self.addData()
                    self.view.stopLoading()
                    self.tableView?.reloadData()
                }
            }
        }
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
