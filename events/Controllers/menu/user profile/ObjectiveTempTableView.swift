//
//  ObjectiveTempTableView.swift
//  events
//
//  Created by Macbook Pro on 2/27/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Foundation

class ObjectiveTempTableView: NSObject , UITableViewDelegate , UITableViewDataSource {
    
    var objectiveArray: [Objectives] = UserUtil.loadUser()?.objectives ?? []
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objectiveArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "objectiveCell") as! ShowObjectiveViewProfileTableViewCell
        print("objectiveArray[indexPath.row].title_en" , objectiveArray[indexPath.row].title_en ?? "")
        cell.objectives.text = objectiveArray[indexPath.row].title_en
        return cell

    }

}
