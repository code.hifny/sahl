//
//  ViewProfileTableViewController.swift
//  events
//
//  Created by abdelrahman on 2/18/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON
import MessageUI
import SDWebImage

class ViewProfileTableViewController: BaseStaticTableViewController , MFMailComposeViewControllerDelegate {
    
    static var canSendMessageOrRequest: Int? = nil
    
    // MARK: - refrenaces
    @IBOutlet weak var userBioTitleLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userJobNameLabel: UILabel!
    @IBOutlet weak var userCompanyNameLabel: UILabel!
    @IBOutlet weak var userBioLabel: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var genderStuts: UILabel!
    @IBOutlet weak var ageStutas: UILabel!
    @IBOutlet weak var snedRequsetMessage: EventsButton! {
        didSet {
            snedRequsetMessage.backgroundColor = Color.greenForGradiantColor
            snedRequsetMessage.setTitle("Send Request Message", for: .normal)
        }
    }
    
    @IBOutlet weak var objdectiveTableView: UITableView!
    
    @IBOutlet weak var linkedInButtonOutlet: UIButton!
    @IBOutlet weak var twitterButtonOutlet: UIButton!
    @IBOutlet weak var emailButtonOutlet: UIButton!
    
    var messages = [Message]()
    
    var userData: User?
    var type: String!
    var info: [String: Any]? // for
    var userFriendID: Int!
    var dataSource = ObjectiveTempTableView()
    var urlLinkedIn: String = ""
    var urlTwitter: String = ""
    var email: String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        objdectiveTableView.dataSource = dataSource
        objdectiveTableView.delegate = dataSource
        helper.addNetworkObserver(on: self)
        if type == "Alert"{
            let my_token = UserUtil.loadUser()?.api_token ?? ""
            print("userFriendID" , userFriendID)
            ChatAPI.getProfileOfUserForDisplay(id: userFriendID, token: my_token) { (user, success, error) in
                if let error = error {
                    self.DangerAlert("Error", message: error)
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                    return
                }
                if let user = user {
                    self.addUserData(user)
                    self.setupButtonState(messagable: user.messageable, user: user)
                }
            }
        }
        setup()

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        if let info = info, let state = info["MyProfile"] as? Bool {
            if state {
                helper.connected ? getMessagableButtonStatus() : loadOfflineState()
            }
            else {
                showUserData() // for open friend profile
            }
        }
    }
    
    
    func showUserData() {
        if type != "Profile" {
            navigationItem.rightBarButtonItem?.isEnabled = false
            navigationItem.rightBarButtonItem?.tintColor = .clear
            
        }
        if let info = info , let nweUser = info["user"] as? User {
            
            self.childViewControllers.forEach({ (child) in
                if let vc = child as? ShowObjectivesTableViewController {
                    vc.objectivesArray = nweUser.objectives ?? []
                    vc.setup()
                }
            })
            print(" info[user] " , nweUser )
            if let img = nweUser.image{
                self.userImageView.af_setImage(withURL: URL(string: (Config.imageUrl + img))!)
            }
            self.usernameLabel.text = nweUser.username
            self.userJobNameLabel.text = nweUser.job_title
            self.userCompanyNameLabel.text = nweUser.company
            self.userBioLabel.text =  nweUser.bio

        }
    }
    
    func loadOfflineState() {
        if let ud = userData {
            userData = PeopleStore.shared.getMessageable(for: ud, messageable: userData?.messageable)
            loadOfflineUser()

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if type == "Alert"{
            let my_token = UserUtil.loadUser()?.api_token ?? ""
            print("userFriendID" , userFriendID)
            ChatAPI.getProfileOfUserForDisplay(id: userFriendID, token: my_token) { (user, success, error) in
                if let error = error {
                    self.DangerAlert("Error", message: error)
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                    return
                }
                if let user = user {
                    self.addUserData(user)
                    self.setupButtonState(messagable: user.messageable, user: user)
                }
            }
        }
        setup()

        helper.connected ? getMessagableButtonStatus() : loadOfflineState()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        helper.removeNetworkObserver(from: self)
    }
    
    func getMessagableButtonStatus()
    {
        if type == "Going"{
            self.view.startLoading()
            snedRequsetMessage.isHidden = true
            ViewProfileTableViewController.canSendMessageOrRequest = nil
            let my_token = UserUtil.loadUser()?.api_token ?? ""
            guard let user_data = userData , let id  = user_data.id else { return }
            
            ChatAPI.getProfileOfUser(for: user_data, id: id, token: my_token) { (user, success, error) in
                if let error = error {
                    self.DangerAlert("Error", message: error)
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                    return
                }
                if let user = user {
                    PeopleStore.shared.updateUser(forUser: user)

                    self.setupButtonState(messagable: user.messageable, user: user)
                }
            }
        }
    }
    
    private func setupButtonState(messagable: Messageable?, user: User) {
        if messagable == nil {
            snedRequsetMessage.setTitle("Send Request Message", for: .normal)
            ViewProfileTableViewController.canSendMessageOrRequest = nil
            snedRequsetMessage.isHidden = false
            view.stopLoading()
        }else if let messagable = messagable {
            if let status = messagable.status {
                if status == 0 && user.autherized_to_accept_chat ?? false {

                    ViewProfileTableViewController.canSendMessageOrRequest = 0
                    snedRequsetMessage.setTitle("Pendeing", for: .normal)
                    snedRequsetMessage.isEnabled = false
                    snedRequsetMessage.backgroundColor = .white
                    snedRequsetMessage.titleColor = .black
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                }
                else if status == 1  {
                    let myToken = UserUtil.loadUser()?.api_token ?? ""
                    let user_id = userData?.id ?? 0
                    self.snedRequsetMessage.setTitle("Send Message", for: .normal)
                    self.snedRequsetMessage.isEnabled = true // reject me for ever
                    self.snedRequsetMessage.backgroundColor = Color.greenForGradiantColor
                    self.snedRequsetMessage.titleLabel?.textColor = .white
                    ViewProfileTableViewController.canSendMessageOrRequest = 1
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                    ChatAPI.getRoomWithMessages(room: nil, user_id: user_id, my_token: myToken, completion: { (messages,room, success, error) in
                        if let error = error {
                            self.DangerAlert("Error", message: error)
                            return
                        }
                        if let messages = messages {
                            self.messages = messages
                            ViewProfileTableViewController.canSendMessageOrRequest = 1
                        }
                    })
                    
                    
                }
                else if status == 2 &&  user.autherized_to_accept_chat ?? false {

                    ViewProfileTableViewController.canSendMessageOrRequest = 2
                    snedRequsetMessage.setTitle("You have a request", for: .normal) //  sent and not accept yet.....
                    snedRequsetMessage.isEnabled = false
                    snedRequsetMessage.backgroundColor = UIColor.white
                    snedRequsetMessage.layer.borderColor = UIColor.white.cgColor
                    snedRequsetMessage.layer.borderWidth = 0
                    snedRequsetMessage.shadowColor = .clear
                    snedRequsetMessage.titleColor = .black
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                }
                else if status == 2 && ( user.autherized_to_accept_chat ?? false) == false {
                    ViewProfileTableViewController.canSendMessageOrRequest = 2
                    snedRequsetMessage.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                    snedRequsetMessage.setTitle("Pending", for: .normal) //  me has a pending and accept or reject.....

                    snedRequsetMessage.isEnabled = false
                    snedRequsetMessage.titleColor = .black
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                    
                }
            }else{
                snedRequsetMessage.setTitle("Send Request Message", for: .normal)
                ViewProfileTableViewController.canSendMessageOrRequest = nil
                self.snedRequsetMessage.isHidden = false
                self.view.stopLoading()
            }
        }
        
    }
    func loadOfflineUser() {
        self.setupButtonState(messagable: userData?.messageable, user: userData!)
    }
    
    // MARK: - update profile acction
    override func editNavBtn() {
        self.performSegue(withIdentifier: "updateProfile", sender: nil)
    }
    
    // MARK: - Setup
    func setup() {
        RightNavBarItems(editBtn())

        title = "Profile".locale
        if type != "Going"{
            self.view.stopLoading()
            RightNavBarItems(editBtn())
            snedRequsetMessage.alpha = 0
        }else{
            snedRequsetMessage.alpha = 1
        }
        
        objdectiveTableView.rowHeight = UITableViewAutomaticDimension
        objdectiveTableView.estimatedRowHeight = 100

        let data = type == "Going" ? userData : UserUtil.loadUser()
        addUserData(data!)
        showUserData()
        tableView.reloadDataAnimatedKeepingOffset()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4 {
            objdectiveTableView.layoutIfNeeded()
            print("objdectiveTableView.contentSize.height" , objdectiveTableView.contentSize.height)
            return objdectiveTableView.contentSize.height + 50
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    @IBAction func sendRequestOrMessageButton(_ sender: EventsButton)
    {
        if ViewProfileTableViewController.canSendMessageOrRequest == 1 {
            performSegue(withIdentifier: "GoToChat", sender: messages)
            return
        }
        else if ViewProfileTableViewController.canSendMessageOrRequest == nil{
            self.snedRequsetMessage.isEnabled = false
            if let id = userData?.id {
                ChatAPI.sendMessageRequest(id: id, completion: { (room,user, granted, error) in
                    

                    if let error = error {
                        self.DangerAlert("Error", message: error)
                        self.snedRequsetMessage.isEnabled = true
                        return
                    }

                    if let granted = granted
                    {
                        if granted
                        {
                            PeopleStore.shared.updateUser(forUser: user!)

                            self.snedRequsetMessage.setTitle("Pending", for: .normal)
                            self.snedRequsetMessage.isEnabled = false
                            self.snedRequsetMessage.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                            self.snedRequsetMessage.titleColor = .black
                        }
                    }
                })
            }
        }
        else if ViewProfileTableViewController.canSendMessageOrRequest == 0 || ViewProfileTableViewController.canSendMessageOrRequest == 2 {
            self.snedRequsetMessage.isEnabled = false
            
            return
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToChat" {
            if let vc = segue.destination as? ChatViewController {
                vc.messages = (sender as? [Message])!
                vc.come_from = "Profile"
                vc.user = self.userData
            }
            
        }
    }
    
    @IBAction func linkedInShatActio(_ sender: UIButton) {
        let url = "https://www.linkedin.com/in/\(urlLinkedIn))"
        print(url)
        openLinkWithSafari(link: url )
    }
    @IBAction func twitterAction(_ sender: UIButton) {
        let url = "https://www.twitter.com/\(urlTwitter)"
        print(url)
        openLinkWithSafari(link: url )
        
    }
    
    @IBAction func googleMailAction(_ sender: UIButton) {
        sendEmail()
    }
    
    func sendEmail() {
        let googleUrlString = "googlegmail:///co?to=\(email)&subject=Hello&body=Hi" //
        if let googleUrl = NSURL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl as URL)
                }
            }else{
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["\(email)"])
                    
                    self.present(mail, animated: true, completion: nil)
                } else {
                    // show failure alert
                }
                
            }
        }
        
    }
    
    func addUserData(_ user: User){
         urlLinkedIn = user.linkedin ?? ""
         urlTwitter = user.twitter ?? ""
         email = user.email ?? ""
        let img = "\(Config.imageUrl + (user.image)!)"
        self.userImageView.sd_setImage(with: URL(string: img))
//        af_setImage(withURL: URL(string: img)!)

        print(img)
        if UserUtil.loadUser()?.id == user.id{
            snedRequsetMessage.alpha = 0
            linkedInButtonOutlet.alpha = 0
            twitterButtonOutlet.alpha = 0
            emailButtonOutlet.alpha = 0
        }else{
            linkedInButtonOutlet.alpha = 1
            twitterButtonOutlet.alpha = 1
            emailButtonOutlet.alpha = 1
            snedRequsetMessage.alpha = 1
            if urlLinkedIn == "" {
                linkedInButtonOutlet.isEnabled = false
                if urlTwitter == ""{
                    twitterButtonOutlet.isEnabled = false
                }else{
                    twitterButtonOutlet.isEnabled = true
                    
                }
            }else{
                linkedInButtonOutlet.isEnabled = true
                if urlTwitter == ""{
                    twitterButtonOutlet.isEnabled = false
                }else{
                    twitterButtonOutlet.isEnabled = true
                    
                }
            }
            
            if urlTwitter == ""{
                twitterButtonOutlet.isEnabled = false
                if urlLinkedIn == "" {
                    linkedInButtonOutlet.isEnabled = false
                }else{
                    linkedInButtonOutlet.isEnabled = true
                }
                
            }else{
                twitterButtonOutlet.isEnabled = true
                if urlLinkedIn == "" {
                    linkedInButtonOutlet.isEnabled = false
                }else{
                    linkedInButtonOutlet.isEnabled = true
                }
                
            }
            emailButtonOutlet.isEnabled = true
        }
        
        self.usernameLabel.text = user.username
        self.userJobNameLabel.text = user.job_title
        self.userCompanyNameLabel.text = user.company
        self.userBioLabel.text = user.bio
        if user.gender == nil{
            self.gender.text = "____"
        }else{
            self.genderStuts.alpha = 1
            if user.gender == "1" {
                self.gender.text = "Male"
            }else{
                self.gender.text = "Female"
            }
        }
        
        if user.age == 0{
//            self.ageStutas.alpha = "____"
            self.age.text = "____"
        }else{

            self.ageStutas.alpha = 1
            self.age.alpha = 1
            self.age.text = "\(user.age ?? 0)"
        }
        if user.bio == nil{
            userBioLabel.text = "____"
        }else{
            userBioLabel.text = user.bio

        }
//        self.userBioTitleLabel.isHidden = user.bio == nil || user.bio == ""

    }
}

