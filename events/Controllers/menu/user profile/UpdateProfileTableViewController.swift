//
//  UpdateProfileTableViewController.swift
//  events
//
//  Created by abdelrahman on 2/19/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Material

enum gender {
    case male
    case female
}

class UpdateProfileTableViewController: UITableViewController ,TextViewDelegate {
    
    // MARK: - text field refrences
    @IBOutlet weak var birthDayTextField: EventsProfileTextField!
    @IBOutlet weak var usernameTextField: EventsProfileTextField!
    @IBOutlet weak var emailTextField: EventsProfileTextField!
    @IBOutlet weak var jobTitleTextField: EventsProfileTextField!
    @IBOutlet weak var companyTextField: EventsProfileTextField!
    @IBOutlet weak var twitterTextField: EventsProfileTextField!
    @IBOutlet weak var linkedinTextField: EventsProfileTextField!
    @IBOutlet weak var bioTextView: TextView!
    
    // MARK: - user image refrences
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var uploader: SVUploader!
    @IBOutlet weak var uploadBtn: UIButton!
    var imageSelected: UIImage = UIImage()
    
    @IBOutlet weak var maleButton: UIButton!
    @IBOutlet weak var femaleButton: UIButton!
    //    @IBOutlet weak var objectiveTableView: UITableView!
    
    // MARK: - Date Picker
    let datePicker = UIDatePicker()
    var birthDayDate: String!
    var genderValue:gender = .male
    var genderSelected: String = "1"
    //    var objectiveEvent = ObjectiveTempTableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    // MARK: - setup
    
    func setup() {
        //        objectiveTableView.delegate = objectiveEvent
        //        objectiveTableView.dataSource = objectiveEvent
        
        // TODO: - set datePicker
        createDatePicker()
        // TODO: - set edit Button
        editButtonConfig()
        // TODO: - set bio Text View configration
        textViewConfig()
        // TODO: - set user data
        userDataConfig()
        // TODO: - set table view configration
        tableViewConfig()
    }
    
    
    
    // MARK: - view configrations
    
    // MARK: - edit Button configrations
    
    func editButtonConfig()  {
        let button: UIButton = UIButton (type:.custom)
//        button =  IconButton(image:  UIImage(named:"true") , tintColor: Color.greenColor)
        button.setBackgroundImage( #imageLiteral(resourceName: "rightMark") , for: .normal)
        button.frame = CGRect(x:0, y:0, width:10, height:15)
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(editNavBtn), for: .touchUpInside)
        RightNavBarItems(BarBtnConfig(button))
    }
    
    // MARK: -  bio Text View configration
    
    func textViewConfig()  {
        bioTextView.delegate = self
        bioTextView.textColor = .darkGray
        //        bioTextView.placeholderColor = .lightGray
        bioTextView.placeholder = "Enter Bio".locale
    }
    
    // MARK: - user data configrations
    
    func userDataConfig()  {
        let img = "\( Config.imageUrl + (UserUtil.loadUser()?.image ?? ""))"
        print( "image" , img)
//        userImageView.af_setImage(withURL: URL(string: img)!)
        self.userImageView.sd_setImage(with: URL(string: img))
        usernameTextField.text = UserUtil.loadUser()?.username
        emailTextField.text = UserUtil.loadUser()?.email
        jobTitleTextField.text = UserUtil.loadUser()?.job_title
        companyTextField.text = UserUtil.loadUser()?.company
        twitterTextField.text = UserUtil.loadUser()?.twitter
        linkedinTextField.text = UserUtil.loadUser()?.linkedin
        bioTextView.text = UserUtil.loadUser()?.bio
        birthDayTextField.text = UserUtil.loadUser()?.birth_date ?? ""
        
        genderSelected = UserUtil.loadUser()?.gender ?? ""
        print("genderSelected" , genderSelected)
        
        if UserUtil.loadUser()?.gender == "1" || UserUtil.loadUser()?.gender == nil {
            genderValue = .male
            genderSelected = "1"
        }else{
            genderValue = .female
            genderSelected = "2"
        }
        
        if genderValue == .male{
            maleButton.setBackgroundImage( #imageLiteral(resourceName: "tick"), for: .normal)
            femaleButton.setBackgroundImage( #imageLiteral(resourceName: "Black & White 1") , for: .normal)
        }else{
            maleButton.setBackgroundImage(#imageLiteral(resourceName: "Black & White 1") , for: .normal)
            femaleButton.setBackgroundImage( #imageLiteral(resourceName: "tick"), for: .normal)
        }
        
    }
    
    // MARK: - table view configration
    
    func tableViewConfig()  {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    // MARK: - Actions
    
    override func editNavBtn() {
        if validation(){
            let parameters:[String : Any] = [
                "api_token":UserUtil.loadUser()?.api_token ?? "",
                "username":usernameTextField.text!,
                "email":emailTextField.text!,
                "job_title":jobTitleTextField.text!,
                "company":companyTextField.text!,
                "twitter":twitterTextField.text!,
                "linkedin":linkedinTextField.text!,
                "bio":bioTextView.text!,
                "birth_date": birthDayTextField.text!,
                "gender": genderSelected
            ]
            self.view.endEditing(true)
            self.StartLoading()
            requestWithUploadImage(
                "UpdateProfile".route,
                image: imageSelected,
                imageName: "image",
                parameters: parameters,
                method:.post,
                uploader: uploader,
                imageView: userImageView,
                imageBtn: uploadBtn,
                completion: { (data) in
                    print(data["error"])
                    if data["data"].dictionary != nil {
                        let user = User(data["data"])
                        UserUtil.saveUser(user)
                        self.SuccessAlert(message: "Update successfully".locale)
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.view.endEditing(true)
                        
                        self.validationShowMessage(data["data"], "name")
                        self.validationShowMessage(data["data"], "email")
                    }
                    self.view.isUserInteractionEnabled = true
                    self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    self.imageSelected = UIImage()
                    self.StopLoading()
            })
        }
    }
    
    @IBAction func selectImageAction(_ sender: Any) {
        presentImagePicker()
    }
    
    @IBAction func genderAction(_ sender: UIButton) {
        if genderValue == .male {
            maleButton.setBackgroundImage(#imageLiteral(resourceName: "tick") , for: .normal)
            femaleButton.setBackgroundImage(#imageLiteral(resourceName: "Black & White 1") , for: .normal)
            genderSelected = "1"
            genderValue = .female
        }else{
            maleButton.setBackgroundImage(#imageLiteral(resourceName: "Black & White 1") , for: .normal)
            femaleButton.setBackgroundImage( #imageLiteral(resourceName: "tick") , for: .normal)
            genderSelected = "2"
            genderValue = .male
            
        }
    }
    
    // MARK: - image Picker Controller Did Finish Picking Media With Info
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info[UIImagePickerControllerEditedImage] as! UIImage
        imageSelected = selectedImage
        userImageView.image = selectedImage
        // Dismiss the picker.
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Validation
    func validation() -> Bool {
        
        var fields:[ValidationData] = []
        
        fields.append(
            ValidationData(
                value:birthDayTextField.text! ,
                name : "Birth Day".locale ,
                type:"required"))
        
        fields.append(
            ValidationData(
                value:usernameTextField.text! ,
                name : "Full Name".locale ,
                type:"required|notOnlyNumber|notOnlyNumberOrSpacialChr|min:3"))
        fields.append(
            ValidationData(
                value:emailTextField.text! ,
                name : "Email".locale ,
                type:"required|email"))
        fields.append(
            ValidationData(
                value:jobTitleTextField.text! ,
                name : "Job Title".locale ,
                type:"required"))
        fields.append(
            ValidationData(
                value:birthDayTextField.text! ,
                name : "Birth Day".locale ,
                type:"required"))
        return  Validation.instance.SetValidation(ValidationData: fields)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if indexPath.row != 9 {
        return UITableViewAutomaticDimension
        //        }else{
        //            return 250 // the second cell has the dynamic table view in it
        //        }
    }
    
    // MARK: - Text view delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        tableView.reloadDataAnimatedKeepingOffset()
        bioTextView.becomeFirstResponder() //Optional
    }
    
    func textViewDidChange(_ textView: UITextView) {
        tableView.reloadDataAnimatedKeepingOffset()
        bioTextView.becomeFirstResponder() //Optional
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        tableView.reloadDataAnimatedKeepingOffset()
    }
    
    // MARK:- Add Birthday and date picker
    
    func createDatePicker(){
        datePicker.datePickerMode = .date
        birthDayTextField.inputView = datePicker
        datePicker.maximumDate = Date()
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneClick))
        
        toolBar.setItems([doneButton], animated: true)
        birthDayTextField.inputAccessoryView = toolBar
    }
    
    @objc func doneClick(){
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "yyyy-MM-dd"
        let firtDate = dateFormat.string(from: datePicker.date)

        if convertDate(datePicker.date){
            self.DangerAlert(message: "You cannot choose this year for a birthdate".locale)
        }else{
            birthDayDate = "\(firtDate)"
            birthDayTextField.text = dateFormat.string(from: datePicker.date)
            self.view.endEditing(true)
        }
        
//        let secondDate = dateFormat.string(from: Date())
//
//        print("firtDate" , firtDate , "secondDate" , secondDate)
//        if firtDate.compare(secondDate) == .orderedSame || firtDate.compare(secondDate) == .orderedDescending {
//            self.DangerAlert(message: "You cannot choose this year for a birthdate".locale)
//        }else{
//            birthDayDate = "\(firtDate)"
//            birthDayTextField.text = dateFormat.string(from: datePicker.date)
//            self.view.endEditing(true)
//        }
    }
    
    func convertDate(_ datePicker: Date) -> Bool{
        let date = Date()
        let calendar = Calendar.current
        let yearNow = calendar.component(.year, from: date)
        let yearDatePicker = calendar.component(.year, from: datePicker)
        
        if yearNow == yearDatePicker  {
            return true
        }else{
            return false
        }
    }
    
    
}

