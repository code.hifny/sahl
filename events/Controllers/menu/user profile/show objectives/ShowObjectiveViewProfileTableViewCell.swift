//
//  ShowObjectiveViewProfileTableViewCell.swift
//  events
//
//  Created by Intcore on 3/26/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class ShowObjectiveViewProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var objectives: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
