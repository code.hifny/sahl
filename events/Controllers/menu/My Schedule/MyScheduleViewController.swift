//
//  MyScheduleViewController.swift
//  events
//
//  Created by Macbook Pro on 3/4/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class MyScheduleViewController: UIViewController , UICollectionViewDelegate , UICollectionViewDataSource {
    let itemsPerRow: CGFloat = 2
    let sectionInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var speaker: UIButton!
    @IBOutlet weak var agenda: UIButton!
    var pageType: String = "My Agenda"
    @objc var refresh: UIRefreshControl!
    var agendaArray: [Agenda] = []
    var speakersArray: [Speakers] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPage()
        //        config()
        refresh = UIRefreshControl()
        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        refresh.tintColor = UIColor.black
        collectionView?.addSubview(refresh)
        self.collectionView?.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPage()
        //        self.tabBarController?.tabBar.isHidden = true
        
        //
        //        refresh = UIRefreshControl()
        //        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        //        refresh.tintColor = UIColor.black
        //        collectionView?.addSubview(refresh)
        //        self.collectionView?.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        self.tabBarController?.tabBar.isHidden = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupPage(){
        title = "My Favourite".locale
        if pageType == "My Agenda"{
            getRequest()
        }else{
            getRequestSpeakers()
        }
        
    }
    
    @objc func refreshData() {
        
        if pageType == "My Agenda"{
            getRequest()
        }else{
            getRequestSpeakers()
        }
        self.setup()
        
    }
    
    func setup() {
        refresh.endRefreshing()
//        collectionView?.reloadData()
    }
    
    
    //    func config() {
    //        let nib = UINib(nibName: "CollectionCell", bundle: nil)
    //        self.collectionView.register(nib, forCellWithReuseIdentifier: "Cell")
    //        self.collectionView.delegate = self
    //        self.collectionView.dataSource = self
    //    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if pageType == "My Agenda"{
            if agendaArray.count == 0 && self.view.viewWithTag(112233) == nil{
                BackNotFounCollection(collectionView: self.collectionView!, text: "No Data Available".locale)
            }else {
                BackNotFounCollection(collectionView: self.collectionView!, text: "")
            }
            return agendaArray.count
        }else{
            if speakersArray.count == 0 && self.view.viewWithTag(112233) == nil{
                BackNotFounCollection(collectionView: self.collectionView!, text: "No Speakers Available".locale)
            }else {
                BackNotFounCollection(collectionView: self.collectionView!, text: "")
            }
            return speakersArray.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if pageType == "My Agenda" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "agendaCell", for: indexPath) as! SessionAgendaCollectionViewCell
            cell.config(agendaArray[indexPath.row])
            cell.notificationBtn.addTarget(self, action: #selector(notificationAction), for: .touchUpInside)
            cell.notificationBtn.tag = indexPath.row
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! SpeakersDataCollectionViewCell
            
            let imageSpeaker = "\(Config.imageUrl + (speakersArray[indexPath.row].image ?? ""))"
            
            cell.speakerImage.af_setImage(withURL: URL(string: imageSpeaker)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.4), runImageTransitionIfCached: false, completion: nil)
            
            cell.speakerName.text = speakersArray[indexPath.row].username ?? ""
            cell.speakerJob.text = speakersArray[indexPath.row].job_title ?? ""
            
            if speakersArray[indexPath.row].is_favorited == 1{
                cell.favouriteButton.setBackgroundImage(#imageLiteral(resourceName: "star"), for: .normal)
            }else{
                cell.favouriteButton.setBackgroundImage(#imageLiteral(resourceName: "star-2"), for: .normal)
            }
            cell.favouriteButton.tag = indexPath.row
            cell.favouriteButton.addTarget(self, action: #selector(favourite), for: .touchUpInside)
            
            cell.speakerButton.tag = indexPath.row
            cell.speakerButton.addTarget(self, action: #selector(showProfileSpeaker), for: .touchUpInside)
            return cell
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if pageType == "My Agenda" {
            let cardController = UserCardViewController.init(nibName: "UserCardViewController", bundle: nil)
            cardController.agenda = agendaArray[indexPath.row]
            cardController.pageTyep = "MyAgeda"
            //            cardController.modalPresentationStyle = .custom
            self.navigationController?.pushViewController(cardController, animated: true)
            //            self.present(cardController, animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func myAgendaAction(_ sender: UIButton) {
        //        self.collectionView.backgroundColor = UIColor.white
        pageType = "My Agenda"
        agenda.tintColor = Color.greenColor
        agenda.setTitleColor(Color.greenColor, for: .normal)
        speaker.setTitleColor(.gray, for: .normal)
        collectionView.reloadData()
    }
    
    @IBAction func mySpeakerAction(_ sender: UIButton) {
        pageType = "Speaker"
        speaker.tintColor = Color.greenColor
        speaker.setTitleColor(Color.greenColor, for: .normal)
        agenda.setTitleColor(.gray, for: .normal)
        getRequestSpeakers()
        collectionView.reloadData()
    }
    
    @objc func notificationAction(_ sender:UIButton) {
        // notify user in time for session
        let sessionId = agendaArray[sender.tag].id ?? 0
        print("sessionId" , sessionId)
        StartLoading()
        Requests.instance.request(link: Config.apiUrl + "user/session/\(sessionId)/remind", method: .post ,parameters: ["api_token":UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    if self.agendaArray[sender.tag].is_reminded ?? true {
                        self.agendaArray[sender.tag].is_reminded  = false
                        self.agendaArray.remove(at: sender.tag)
                        self.collectionView?.deleteItems(at: [IndexPath(row:sender.tag,section:0)])
                        SessionsUtil.saveSessions(self.agendaArray)

                   
                    }else{
                        self.agendaArray[sender.tag].is_reminded  = true
                        
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                        self.StopLoading()
                        self.collectionView?.reloadData()
                    })
//                    self.collectionView.reloadData()
                }
            }
        }
    }
    
    func getRequest()  {
        if SessionsUtil.loadSessions() != nil {
            self.agendaArray =  SessionsUtil.loadSessions() ?? []
            self.view.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Sessions".route, method: .get,parameters: ["api_token": UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let sessions = response.data!["data"]["sessions"].map{Agenda($0.1)}
                    SessionsUtil.saveSessions(sessions)
                    self.agendaArray = sessions
                    self.view.stopLoading()
                    self.collectionView?.reloadData()
                    
                }
            }
        }
        
    }
    
    func getRequestSpeakers()  {
        if SpeakersFavouriteUtil.load() != nil {
            self.speakersArray =  SpeakersFavouriteUtil.load() ?? []
            self.view.stopLoading()
            loadDataSpeakers(false)
        }else{
            loadDataSpeakers()
        }
        
    }
    
    func loadDataSpeakers(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Speakers Favourite".route, method: .get,parameters: ["api_token": UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let speakers = response.data!["data"]["speakers"].map{Speakers($0.1)}
                    self.speakersArray =  SpeakersFavouriteUtil.load() ?? []
                    SpeakersFavouriteUtil.save(speakers)
                    self.speakersArray = speakers
                    self.view.stopLoading()
                    self.collectionView?.reloadData()
                    
                }
            }
        }
        
    }
    
    
    // MARK: - Favourite Function
    func favouriteSpeakerServices(speakerID: Int ,  closure: @escaping(Bool) -> Void){
        let parameter = ["api_token": UserUtil.loadUser()?.api_token ?? ""] as [String: Any]
        Requests.instance.request(link: Config.apiUrl + "user/speaker/\(speakerID)/favorite", method: .post, parameters: parameter) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print("Done")
                    closure(true)
                }
            }
        }
        
    }
    
    @objc func favourite(sender: UIButton){
        
        if UserUtil.loadUser()?.api_token == nil{
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            print( "sender.tag" , sender.tag)
            if self.speakersArray[sender.tag].is_favorited == 0{
//                AddFav(sender.tag)
            }else{
                RemoveFav(sender.tag)
            }
        }
    }
    
//    func AddFav(_ index: Int){
//        self.speakersArray[index].is_favorited = 1
//        SpeakersFavouriteUtil.save(self.speakersArray)
//        self.collectionView?.reloadData()
//        favouriteSpeakerServices(speakerID: self.speakersArray[index].id ?? 0) { (done) in
//            if done{
//                print("Done")
//            }
//        }
//    }
    
    func RemoveFav(_ index: Int){
        self.speakersArray[index].is_favorited = 0
        SpeakersFavouriteUtil.save(self.speakersArray)
        print(index)
        StartLoading()
        favouriteSpeakerServices(speakerID: self.speakersArray[index].id ?? 0) { (done) in
            if done{
                self.speakersArray.remove(at: index)
                self.collectionView?.deleteItems(at: [IndexPath(row:index,section:0)])
                SpeakersFavouriteUtil.save(self.speakersArray)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                    self.collectionView?.reloadData()
                    self.StopLoading()
                })
                
                print("Done")
                
            }
        }
    }
    
    @objc func showProfileSpeaker(sender: UIButton){
        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SpeakerProfileViewController") as! SpeakerProfileViewController
        viewController.speakerInfo = speakersArray[sender.tag]
        viewController.arrayData = speakersArray
        viewController.index = sender.tag
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    
}

extension MyScheduleViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        if pageType == "My Agenda"{
            return CGSize(width: self.collectionView.frame.width, height: 120)
        }
        
        return CGSize(width: widthPerItem, height: 200)
        
    }
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

