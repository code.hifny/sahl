//
//  MaterialTableViewController.swift
//  events
//
//  Created by Intcore on 3/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class MaterialTableViewController: UITableViewController , UISearchBarDelegate , UIDocumentInteractionControllerDelegate {

    var materialsArray: [Material] = []
    var materialsArrayArrayFilter: [Material] = []
    @objc var refresh: UIRefreshControl!
    
    // MARK:- Search Bar
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .system)
    let searcClosehButton = UIButton(type: .system)
    
//    var downloadTask: URLSessionDownloadTask!
//    var backgroundSession: URLSession!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Event Material".locale
//        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
//        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        tableViewConfig()
        getRequest()
        addButtonSearch()
        refresh = UIRefreshControl()
        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        refresh.tintColor = UIColor.black
        tableView?.addSubview(refresh)
        self.tableView?.reloadData()
    }
    
    func tableViewConfig()  {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 180
    }

    
    @objc func refreshData() {
        getRequest()
        self.setup()
    }
    
    func setup() {
        refresh.endRefreshing()
        tableView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.tabBarController?.tabBar.isHidden = false
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //&& self.view.viewWithTag(112233) == nil
        if materialsArray.count == 0 {
            BackNotFounTableView(tableView: self.tableView!, text: "No data Available".locale)
        }else {
            BackNotFounTableViewEvents(tableView: self.tableView!, text: "")
        }
        return materialsArray.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "materilaItem") as! EventMaterialInfoTableViewCell
        cell.downloadButton.tag = indexPath.row
        
        cell.config(materialsArray[indexPath.row])
//        cell.materialName.text = materialsArray[indexPath.row].title_en
//        cell.descriptionOFMaterial.text = materialsArray[indexPath.row].description_en
//        cell.downloadButton.tag = indexPath.row
//        cell.downloadButton.addTarget(self, action: #selector(downloadURL), for: .touchUpInside)
        return cell
    }
    
    @objc func downloadURL(sender: UIButton){
        
//        let url = URL(string: "\( Config.baseUrl + (materialsArray[sender.tag].file ?? "") )")
//        print(url!)
//        downloadTask = backgroundSession.downloadTask(with: url!)
//        downloadTask.resume()
//        let url = "\( Config.baseUrl + (materialsArray[sender.tag].file)! )"
//        print(url)
//        Alamofire.download(url).downloadProgress { progress in
//            print("\(Int(progress.fractionCompleted * 100) )% ")
//            }
//            .responseData { response in
//                if let data = response.result.value {
//                    print(data)
//                }
//        }
    }
  
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func getRequest()  {
        if MaterialUtil.load() != nil {
            self.materialsArray =  MaterialUtil.load() ?? []
            self.materialsArrayArrayFilter = self.materialsArray
            self.view.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Materials".route, method: .get,parameters: ["api_token": UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let materials = response.data!["data"]["materials"].map{Material($0.1)}
                    MaterialUtil.save(materials)
                    self.materialsArray = materials
                    self.view.stopLoading()
                    self.tableView?.reloadData()
                    
                }
            }
        }
    }
    
    func showFileWithPath(path: String){
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true{
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            StopLoading()
            viewer.presentPreview(animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.03 , execute: {
                self.materialsArray = self.materialsArrayArrayFilter
                self.tableView.reloadData()
                self.closeSearch()
            })
            
        }
    }
    
    //MARK: URLSessionDownloadDelegate
    // 1
//    func urlSession(_ session: URLSession,
//                    downloadTask: URLSessionDownloadTask,
//                    didFinishDownloadingTo location: URL){
//
//        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
//        let documentDirectoryPath:String = path[0]
//        let fileManager = FileManager()
//
//        let type = "/file.xlsx"
//        print(type)
//        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat(type))
//
//        if fileManager.fileExists(atPath: destinationURLForFile.path){
//            showFileWithPath(path: destinationURLForFile.path)
//        }
//        else{
//            do {
//                try fileManager.moveItem(at: location, to: destinationURLForFile)
//                // show file
//                showFileWithPath(path: destinationURLForFile.path)
//            }catch{
//                print("An error occurred while moving file to destination url")
//            }
//        }
//    }
//    // 2
//    func urlSession(_ session: URLSession,
//                    downloadTask: URLSessionDownloadTask,
//                    didWriteData bytesWritten: Int64,
//                    totalBytesWritten: Int64,
//                    totalBytesExpectedToWrite: Int64){
////        upload.progress = CGFloat(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite))
//        //            .setProgress(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite), animated: true)
//    }
//
//    //MARK: URLSessionTaskDelegate
//    func urlSession(_ session: URLSession,
//                    task: URLSessionTask,
//                    didCompleteWithError error: Error?){
//        downloadTask = nil
////        upload.progress = CGFloat(0)
//        if (error != nil) {
//            print(error!.localizedDescription)
//        }else{
//            print("The task finished transferring data successfully")
//        }
//    }
    
    //MARK: UIDocumentInteractionControllerDelegate
    
    //
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }

    

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MaterialTableViewController {
    // MARK:- Search Bar
    func addButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchButton.setImage(#imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searchButton.addTarget(self, action: #selector(searchController), for: .touchUpInside)
        navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: searchButton)
    }
    
    func addCloseButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search"
        searcClosehButton.setImage(#imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysOriginal), for: .normal)
        searcClosehButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searcClosehButton.addTarget(self, action: #selector(closeSearch), for: .touchUpInside)
        navigationItem.leftBarButtonItem =  UIBarButtonItem(customView: searcClosehButton)
    }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        let textFieldInsideSearchBarLabel = searchBar.textField.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        return true
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        let stringText = searchBar.text!
        print(stringText)
      
        materialsArray = materialsArrayArrayFilter.filter({ $0.title_en?.range(of: stringText , options:.caseInsensitive) != nil})
        if(stringText.count < 1){
            materialsArray = materialsArrayArrayFilter
        }
        self.tableView?.reloadData()
        
    }
    
    
    @objc func searchController(){
        searchBar.alpha = 1
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            // Fallback on earlier versions
        }
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.8, animations: {
            if let textfield = self.searchBar.value(forKey: "searchField") as? UITextField {
                if textfield.subviews.first != nil {
                    textfield.textColor = UIColor.white
                }
            }
            self.addCloseButtonSearch()
            self.searcClosehButton.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
        
        definesPresentationContext = true
    }
    
    @objc func closeSearch(){
        navigationItem.setRightBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.3) {
            self.searchBar.endEditing(false)
            self.searchBar.text = ""
            self.searchBar.alpha = 0
            self.addButtonSearch()
            self.searchButton.alpha = 1
            self.searcClosehButton.alpha = 0
            self.navigationItem.titleView = nil
            
//            if self.materialsArray[indexDownloadObject].elementDownload{
//                print("Done")
//                self.tableView.reloadData()
//            }else{
//                self.materialsArray = self.materialsArrayArrayFilter
//                self.tableView.reloadData()
//            }
//
            self.materialsArray = self.materialsArrayArrayFilter
            self.tableView.reloadData()

            self.settingNavigationController()
            
        }
    }
    @objc func BackToHome(){
        self.navigationController?.popViewController(animated: true)
    }
    func settingNavigationController(){
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(BackToHome))
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: false)
    }
    
}
