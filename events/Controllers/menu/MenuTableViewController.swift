//
//  MenuTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import UserNotifications

class MenuTableViewController: UITableViewController {
    
    // MARK:- Outlets
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var buttonType: UIButton!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var logoutLabel: UILabel!
    @IBOutlet weak var ticketType: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //        clearNav()
        //        LeftNavBarItems(SearchBtn())
        title = "Menu".locale
        viewButton.isHidden = UserUtil.loadUser() == nil
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        addDateUser()
//        setupLogoutCell()
        
        if UserUtil.loadUser() == nil {
            logoutLabel.localizeKey = "Login".locale
            buttonType.isUserInteractionEnabled = false
        }
        
//        else if UserUtil.loadUser() != nil {
//            logoutLabel.text = "Logout"
//            buttonType.isUserInteractionEnabled = true
//            let img = "\(Config.imageUrl + (UserUtil.loadUser()?.image)! ?? "" )"
//            //            if let img = UserUtil.loadUser()?.image {
//            ////                imageUser.af_setImage(withURL: URL(string: )
//            //                imageUser.af_setImage(withURL: URL(string: (Config.imageUrl + img))!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.0), runImageTransitionIfCached: false, completion: nil)
//            //            }
//            //            imageUser.af_setImage(withURL: URL(string: img)!)
//            self.imageUser.sd_setImage(with: URL(string: img))
//
//            nameUser.text = UserUtil.loadUser()?.username
//            tableView.reloadData()
//        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addDateUser()
//        setupLogoutCell()
        if UserUtil.loadUser() == nil {
            buttonType.localizeKey = "".locale
            buttonType.isEnabled = false
            logoutLabel.localizeKey = "Login".locale
            buttonType.isUserInteractionEnabled = false
        }
        else {
            logoutLabel.localizeKey = "Logout".locale
            buttonType.isUserInteractionEnabled = true
            let img = "\(Config.imageUrl + (UserUtil.loadUser()?.image)! ?? "" )"
            //            if let img = UserUtil.loadUser()?.image {
            ////                imageUser.af_setImage(withURL: URL(string: )
            //                imageUser.af_setImage(withURL: URL(string: (Config.imageUrl + img))!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.0), runImageTransitionIfCached: false, completion: nil)
            //            }
            //            imageUser.af_setImage(withURL: URL(string: img)!)
            self.imageUser.sd_setImage(with: URL(string: img))

            nameUser.text = UserUtil.loadUser()?.username
            tableView.reloadData()

        }
        let imgView = UIImageView(frame:CGRect(x:0,y:0,width:self.view.frame.width * 1.7,height:20))
        imgView.image = UIImage(named:"header")
        imgView.contentMode = .scaleToFill
        imgView.layer.zPosition = -1

        imgView.tag = 0099
//        let img = UIImage(named:"header")
        UIApplication.shared.statusBarView?.addSubview(imgView)
//        UIApplication.shared.statusBarView?.backgroundColor =  UIColor(patternImage: img!) // UIColor.fromGradientWithDirection(.leftToRight, frame: self.tableView.frame, colors: [ Color.blueColor,Color.greenForGradiantColor], cornerRadius: 0)

        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarHideConfig(isHidden: true)
        UIApplication.shared.statusBarView?.viewWithTag(0099)?.removeFromSuperview()
        UIApplication.shared.statusBarView?.backgroundColor = .clear
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarHideConfig(isHidden: false)
    }
    
    
    //    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        if indexPath.row == 0 {
    //          cell.backgroundColor = UIColor(hex: "F8F8F8")
    //        }
    //
    //    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if indexPath.row == 1 {
            if UserUtil.loadUser()?.api_token == nil {
                self.DangerAlert(message: "Pleace Login".locale)
            }else{
                
            }
            
        }else if indexPath.row == 2 {
            if UserUtil.loadUser()?.api_token == nil {
                self.DangerAlert(message: "Pleace Login".locale)
            }else{
                if UserUtil.loadUser()?.ticket_info?.addressEn == nil{
                    let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "BuyTicketTableViewController") as! BuyTicketTableViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                }else{
                    let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "ShowTicketInfoTableViewController") as! ShowTicketInfoTableViewController
                    let nav = EventsNavigationController(rootViewController: viewController)
                    self.present(nav, animated: true, completion: nil)

                }
            }
        }else if indexPath.row == 3 {
            if UserUtil.loadUser()?.api_token == nil{
                self.DangerAlert(message: "Pleace Login".locale)
            }else{
                let viewController = UIStoryboard(name: "My Schedule", bundle: nil).instantiateViewController(withIdentifier: "MyScheduleViewController") as! MyScheduleViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }else if indexPath.row == 4{
            if UserUtil.loadUser()?.api_token == nil {
                self.DangerAlert(message: "Pleace Login".locale)
            }else{
                let viewController = UIStoryboard(name: "People", bundle: nil).instantiateViewController(withIdentifier: "MainTableViewController") as! MainTableViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }else if indexPath.row == 5{
            if UserUtil.loadUser()?.api_token == nil {
                self.DangerAlert(message: "Pleace Login".locale)
            }else{
                let viewController = UIStoryboard(name: "EventMaterial", bundle: nil).instantiateViewController(withIdentifier: "MaterialTableViewController") as! MaterialTableViewController
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }else if indexPath.row == 6{
            let viewController = UIStoryboard(name: "Sponser", bundle: nil).instantiateViewController(withIdentifier: "SponserCollectionViewController") as! SponserCollectionViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }else if indexPath.row == 8{
            
        }else if indexPath.row == 9{

            let viewController = UIStoryboard(name: "About", bundle: nil).instantiateViewController(withIdentifier: "AbnoutTableViewController") as! AbnoutTableViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }else if indexPath.row == 10{
            
            let viewController = UIStoryboard(name: "Complain", bundle: nil).instantiateViewController(withIdentifier: "CompalintTableViewController") as! CompalintTableViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
        }else if indexPath.row == 11 {
            if UserUtil.loadUser()?.api_token == nil {
                
                self.goToLogin()
            }else{
                actionSheet(title: "Are you sure want to LOGOUT ?", actionTitle: "Yes",cancelTitle:"No") { (action) in
                    UserUtil.removeUser()
                    NotificationsUtil.removeAll()
                    self.goToLogin()
                }
            }
        }
    }
    
    
    @IBAction func gotoProfileUser(_ sender: UIButton) {
        if UserUtil.loadUser()?.api_token == nil {
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            let viewController = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ViewProfileTableViewController") as! ViewProfileTableViewController
            viewController.type = "Profile"
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func addDateUser(){
        tableView.reloadData()
        if UserUtil.loadUser()?.ticket_info?.addressEn == nil{
            ticketType.text = "Buy Ticket".locale
             tableView.reloadData()
        }else{
            ticketType.text = "My Ticket".locale
             tableView.reloadData()
        }
       
        
    }
    
    
    func setupLogoutCell() {
//        tableView.reloadData()
        
        
    }
    
    @IBAction func gotoEditProfile(_ sender: UIButton) {
        let viewController = UIStoryboard.init(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "UpdateProfileTableViewController") as! UpdateProfileTableViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

