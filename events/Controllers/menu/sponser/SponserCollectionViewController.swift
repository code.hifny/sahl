//
//  SponserCollectionViewController.swift
//  events
//
//  Created by Macbook Pro on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift
import Lightbox

class SponserCollectionViewController: UICollectionViewController {
    
    let itemsPerRow: CGFloat = 3
    let sectionInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    
    
    //    var sponsersArray: [Sponsers] = []
    var sponsersItems:SponsersItem?
    var mainSponser:Sponsers?
    @objc var refresh: UIRefreshControl!
    
    
    var sponsersArray:[String : [Sponsers]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPage()
        
        refresh = UIRefreshControl()
        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        refresh.tintColor = UIColor.black
        collectionView?.addSubview(refresh)
        self.collectionView?.reloadData()
        
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        self.tabBarController?.tabBar.isHidden = true
    //    }
    //
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(animated)
    //        self.tabBarController?.tabBar.isHidden = false
    //
    //    }
    
    @objc func refreshData() {
        getRequest()
        self.setup()
    }
    func setup() {
        refresh.endRefreshing()
        collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1 + sponsersArray.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if section == 0{
            return 1
        }else{
            return Array(sponsersArray)[section - 1].value.count + 1
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerCell", for: indexPath) as! HeaderCollectionViewCell
            cell.viewLayer.alpha = 0
            if let image = sponsersItems?.mainSponser?.image {
                cell.imageMainSponser.af_setImage(withURL: URL(string: Config.imageUrl +  image)!)
            }
            cell.nameOfMainSponser.text = sponsersItems?.mainSponser?.categoryName
            
            cell.viewLayer.alpha = 0.7
            cell.viewLayer.backgroundColor = UIColor.fromGradientWithDirection(.leftToRight, frame: collectionView.frame, colors: [ Color.blueColor,Color.greenForGradiantColor], cornerRadius: 0)
            
            return cell
            
        }else{
            if indexPath.row == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "titleCell", for: indexPath) as! TitleCollectionViewCell
                cell.name.text = Array(sponsersArray)[indexPath.section - 1].key
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "otherSponserCell", for: indexPath) as! OtherSponserCollectionViewCell
                if let image = Array(sponsersArray)[indexPath.section - 1].value[indexPath.row - 1].image {
                    cell.otherImageSponsers.af_setImage(withURL: URL(string: Config.imageUrl + image)!)
                }
                return cell
            }
            
        }
    }
    
    
    func setupPage() {
        title = "Sponsers".locale
        navigationController?.navigationBar.tintColor = .white
        getRequest()
    }
    
    
    func getRequest()  {
        if SponsersUtil.load() != nil {
            self.sponsersItems = SponsersUtil.load()
            if let allSponsers = SponsersUtil.load()?.sponsers {
                print(allSponsers)
                self.sponsersArray = Dictionary(grouping: allSponsers , by:  { $0.categoryName ?? "" })
            }
            self.collectionView?.reloadData()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Sponsors".route, method: .get) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    self.sponsersItems = SponsersItem(response.data?["data"] ?? JSON.null)
                    if let allSponsers = self.sponsersItems?.sponsers {
                        self.sponsersArray = Dictionary(grouping: allSponsers , by:  { $0.categoryName! })
                    }
                    SponsersUtil.save(self.sponsersItems)
                    self.view.stopLoading()
                    self.collectionView?.reloadData()
                }
            }
        }
        
    }
}

extension SponserCollectionViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        if indexPath.section == 0 {
            return CGSize(width: self.view.frame.width, height: 222)
        }else{
            if indexPath.row == 0 {
                return CGSize(width: self.view.frame.width, height: 20)
            }
            return CGSize(width: widthPerItem, height: widthPerItem)
        }
        
    }
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return  UIEdgeInsets(top: 0, left: 0, bottom: 0, right:  0)
        }
        return sectionInsets
    }
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return sectionInsets.left
    }
}




