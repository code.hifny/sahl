//
//  ShowTicketInfoTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/25/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON


class ShowTicketInfoTableViewController: UITableViewController {

    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var imageTicket: UIImageView!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var ticketNumber: UILabel!
    @IBOutlet weak var ticketType: UILabel!

    var pageType: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navWhite()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navConfig()
        tabBarHideConfig(isHidden: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarHideConfig(isHidden: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    func setup(){
        title = "Your ticket".locale
        exitButtonAddNav()
        appendData()
    }
    func exitButtonAddNav(){
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(BackToLogin))
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: true)
    }
    @objc func BackToLogin(){
        print("buyTicket" , pageType )
        if pageType == "buyTicket"{
            self.goToHome()
        }else{
            self.dismiss(animated: true, completion: nil)
        }

    }

    func appendData(){
        username.text = UserUtil.loadUser()?.username
        let image = "\(Config.imageUrl + (UserUtil.loadUser()?.ticket_info?.ticket_image ?? ""))"
        print(image)
        imageTicket.af_setImage(withURL: URL(string: image)!)
                
        
        let ticketTypeString = UserUtil.loadUser()?.ticket_info?.package?.name_en ?? ""
        if ticketTypeString == "" {
            ticketType.text = "Free Invitation".locale
        }else{
            ticketType.text = ticketTypeString
        }
        startDate.text = UserUtil.loadUser()?.ticket_info?.date ?? ""
        ticketNumber.text = String(describing: UserUtil.loadUser()?.ticket_info?.id ?? 0)
        location.text = UserUtil.loadUser()?.ticket_info?.addressEn ?? ""

    }
//    func getObjects(type: Event.Type) -> Results<Event>? {
//        return realm.objects(type)
//    }
    
    @IBAction func openInMap(_ sender: Any) {
        let lat = UserUtil.loadUser()?.ticket_info?.latitude ?? "0"
        let lang = UserUtil.loadUser()?.ticket_info?.longitude ?? "0"

        MapsHelper.instance.showActionSheet(fromLatitude: nil, fromLongitude: nil, toLatitude: Float(lat)!, toLongitude: Float(lang)!)
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
