//
//  BuyTicketTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/24/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON

class BuyTicketTableViewController: UITableViewController , UICollectionViewDelegate , UICollectionViewDataSource , UITextFieldDelegate {

    // MARK:- Outlets
    @IBOutlet weak var promoCodeTextField: EventsTextField!
//    @IBOutlet weak var progress: SVProgressHUD!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var imageConfirm: UIImageView!
    @IBOutlet weak var chooseTicketType: UILabel!
    
    // MARK:- Varaibles
    var packageArray: [Packages] = []
    var selectedID: Int!
    var packageID:Int = 0
    var price: Int!
    var selectedIndex:Int = -1
    var promoCode: String = ""
    var checkPromoCode: Bool = false
    var disableCollectionView: Bool = false
    var percent: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRequest()
        title = "Buy Ticket".locale
        collectionView.delegate = self
        collectionView.dataSource = self
        totalLabel.alpha = 0
        totalPrice.alpha = 0
        imageConfirm.alpha = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.promoCodeTextField.setIcon(self.promoCodeTextField.rightIcon)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return packageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "packageCell", for: indexPath) as! PackageTicketCollectionViewCell
        if selectedIndex == indexPath.row {
            cell.backgroundColor = UIColor.fromGradientWithDirection(.leftToRight, frame: cell.frame, colors: [ Color.blueColor,Color.greenForGradiantColor], cornerRadius: 15)
            totalLabel.alpha = 1
            totalPrice.alpha = 1
            cell.typeOFTicket.textColor = UIColor.white
            cell.priceOfTicket.textColor = UIColor.white
//            totalPrice.text = String(describing: packageArray[indexPath.row].price ?? 0)
//            packageID = packageArray[indexPath.row].id
//            price = packageArray[indexPath.row].price
        }else{
            cell.backgroundColor = UIColor.white
            cell.typeOFTicket.textColor = UIColor.black
            cell.priceOfTicket.textColor = UIColor.black
        }
        cell.typeOFTicket.text = packageArray[indexPath.row].packageName
        cell.priceOfTicket.text = String(describing: packageArray[indexPath.row].price ?? 0)
        print("disableCollectionView" , disableCollectionView)
        if disableCollectionView == true{
            UIView.animate(withDuration: 0.3, animations: {
                cell.alpha = 0.4
                cell.backgroundColor = UIColor(hex: "BFBFBF")
                self.chooseTicketType.textColor = UIColor(hex: "BFBFBF")
            })
           
        }else{
            self.chooseTicketType.textColor = UIColor.black
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        packageID = packageArray[indexPath.row].id ?? 0
        price = packageArray[indexPath.row].price
        // MARK: - Issues
        
        print("percent" , percent)
        if percent == 0  {
            totalPrice.text = String(describing: price ?? 0)
            promoCodeTextField.placeholder = "Promo Code (Optional)"
        }else{
            if promoCodeTextField.text == ""{
                totalPrice.text = String(describing: price ?? 0)
            }else{
                print("price" , price , "  " ,  "percent" , percent)
                let total = ( (price * percent)/100)
                print("total" , total)
                totalPrice.text = String(describing: total)
            }
        }
        collectionView.reloadData()
    }
    // MARK: - Text Field
 
    @IBAction func promoCode(_ sender: Any) {
        maxLenght(promo: promoCodeTextField, max: 5)
    }
    
    func maxLenght(promo: UITextField , max: Int){
        let lenght = promo.text?.count ?? 0
        let code = promo.text
        
        if (lenght >= max){
            let index = code?.index((code?.startIndex)!, offsetBy: max)
            promo.text = promo.text?.substring(to: index!)
            promoCode = promo.text ?? ""
            promoCodeRequest(code: promoCode)
        }else{
            
            if packageID != 0 {
                self.totalLabel.alpha = 1
                self.totalPrice.text = "\(price ?? 0)"
                disableCollectionView = false
                self.collectionView.reloadData()
            }else{
                self.imageConfirm.alpha = 0
                self.totalLabel.alpha = 0
                self.totalPrice.alpha = 0
                self.collectionView.isUserInteractionEnabled = true
                disableCollectionView = false
                self.collectionView.reloadData()
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        imageConfirm.alpha = 0
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        imageConfirm.alpha = 0
    }
    
    func promoCodeRequest(code: String) {
        print(code)
        let param:[String: Any] = ["api_token":UserUtil.loadUser()?.api_token ?? "" ,
                                   "code":code]
        Requests.instance.request(link: "Check Promo Code".route, method: .post, parameters: param) { (response) in
            self.StartLoading()

            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    let message = response.data!["data"] //["message"]
                    let messages = message["code"]
//                    let arrayData = messages?.first
                    self.imageConfirm.image = #imageLiteral(resourceName: "success")
                    self.imageConfirm.alpha = 1
                    let percentServer = messages["percent"].intValue
                    if percentServer == 0 {
                        self.totalLabel.alpha = 0
                        self.collectionView.isUserInteractionEnabled = false
                        self.disableCollectionView = true
                        self.collectionView.reloadData()
                    }else{
                        self.collectionView.isUserInteractionEnabled = true
                        self.disableCollectionView = false
                        if self.packageID != 0 {
                            self.totalLabel.alpha = 1
                            self.totalPrice.alpha = 1
                            let total = ( (self.price * percentServer)/100)
                            print("total" , total)
                            self.totalPrice.text = String(describing: total)
                        }else{
                            self.totalLabel.alpha = 0
                            self.totalPrice.alpha = 0
                        }
                       
                        print("percentServer" , percentServer)
                        self.percent = percentServer
                        self.collectionView.reloadData()
                    }
                    self.checkPromoCode = true
                }else{
                    self.collectionView.isUserInteractionEnabled = true
                    self.disableCollectionView = false
                    self.imageConfirm.image = #imageLiteral(resourceName: "attention")
                    self.imageConfirm.alpha = 1
                    self.totalLabel.alpha = 0
                    self.checkPromoCode = false
                    self.collectionView.reloadData()
                }
                self.StopLoading()

            }
        }
        
    }
    
    @IBAction func sendRequst(_ sender: Any) {
        if packageID == 0 && promoCode == ""  {
            let param:[String: Any] = [ "api_token":UserUtil.loadUser()?.api_token ?? "" ]
            StartLoading()
            Requests.instance.request(link: "Buy Ticket".route, method: .post, parameters: param) { (response) in
                print(response)
                if response.haveError{
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data?["status"].bool == true {
                        print(response)
                        let user = User(response.data!["data"])
                        UserUtil.saveUser(user)
                        let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "ShowTicketInfoTableViewController") as! ShowTicketInfoTableViewController
                        viewController.pageType = "buyTicket"

                        let nav = EventsNavigationController(rootViewController: viewController)
                        self.present(nav, animated: true, completion: nil)
                    }else{
                        self.validationShowMessage(response.data!, "message")
                    }
                }
                self.StopLoading()
            }
        }else{
            if checkPromoCode || packageID != 0 {

                let param:[String: Any] = ["package_id":packageID,
                                           "api_token":UserUtil.loadUser()?.api_token ?? "" ,
                                           "promo-code":promoCode]
                print( "packageID" , packageID)
                StartLoading()
                Requests.instance.request(link: "Buy Ticket".route, method: .post, parameters: param) { (response) in
                    print(response)
                    if response.haveError{
                        self.DangerAlert(message: response.error )
                    }else{
                        if response.data?["status"].bool == true {
                            print(response)
                            let user = User(response.data!["data"])
                            UserUtil.saveUser(user)
                            let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "ShowTicketInfoTableViewController") as! ShowTicketInfoTableViewController
                            viewController.pageType = "buyTicket"
                            let nav = EventsNavigationController(rootViewController: viewController)
                            self.present(nav, animated: true, completion: nil)
                        }else{
                            self.validationShowMessage(response.data!, "message")
                            
                        }
                    }
                    self.StopLoading()
                }
            }else{
                self.DangerAlert(message: "Enter Vailed Code".locale )

            }
        }
        
    }
    
    func getRequest()  {
        if PackageUtil.load() != nil {
            self.packageArray =  PackageUtil.load() ?? []
            self.collectionView.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.collectionView.startLoading()
        }
        Requests.instance.request(link: "Packages".route, method: .get) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let package = response.data!["data"]["packages"].map{Packages($0.1)}
                    print("Packages ===>" , package )
                    PackageUtil.save(package)
                    self.packageArray = package
                    self.collectionView.stopLoading()
                    self.collectionView.reloadData()

                }
            }
        }
        
    }
  
}
