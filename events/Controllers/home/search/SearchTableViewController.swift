//
//  SearchTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/14/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
class SearchTableViewController: UITableViewController,UISearchBarDelegate{
   
//    var event:Event?
    var eventNews:[News] = []
    var eventNewsFilter:[News] = []
    // MARK: - Search
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .system)
    let searcClosehButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            searchController()
        }
        else
        {
            // Fallback on earlier versions
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    func loadData(_ completion: @escaping (Event) -> ()) {
//        Requests.instance.request(link: "event".route, method: .get) { (response) in
//            if response.haveError{
//                self.DangerAlert(message: response.error )
//            }else{
//                if response.data?["status"].bool == true {
//                    print(response)
//                    completion(Event(response.data!["data"]))
//                }
//            }
//        }
//    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if eventNews.count == 0 {
            BackNotFounTableView(tableView: self.tableView, text: "No Matched Results")
        }else {
            BackNotFounTableViewEvents(tableView: self.tableView, text: "")
        }
        return eventNews.count

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsHome", for: indexPath) as! NewsTableViewCell
        
        // Configure the cell...
        cell.config(eventNews[indexPath.row])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NewsDetailsTableViewController") as! NewsDetailsTableViewController
        viewController.newsDetails = eventNews[indexPath.row]
        self.navigationController?.pushViewController(viewController, animated: true)
    }

}
extension SearchTableViewController {
    // MARK:- Search Bar

    func addCloseButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search"
        searcClosehButton.setImage(#imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysOriginal), for: .normal)
        searcClosehButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searcClosehButton.addTarget(self, action: #selector(closeSearch), for: .touchUpInside)
        navigationItem.leftBarButtonItem =  UIBarButtonItem(customView: searcClosehButton)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        let stringText = searchBar.text!
        print(stringText)
        eventNews = eventNewsFilter.filter({ $0.title?.range(of: stringText , options:.caseInsensitive) != nil})
        
        if(stringText.count < 1){
            eventNews = eventNewsFilter
        }
     self.tableView.reloadData()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        let textFieldInsideSearchBarLabel = searchBar.textField.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        return true
        
    }
    
    @available(iOS 11.0, *)
    @objc func searchController(){
        eventNewsFilter = eventNews
//        searchBar.alpha = 0
//        searchBar.setTextFieldClearButtonColor(color: .red)
        searchBar.barTintColor = UIColor.white

        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButton(nil, animated: true)
        searchBar.textField.textColor = .white
        
            self.addCloseButtonSearch()
            self.searcClosehButton.alpha = 1
//        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
//        })
    }
    
    @objc func closeSearch(){
        DispatchQueue.main.async {
            if self.searchBar.endEditing(false) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    

}

extension UISearchBar{
    var textField : UITextField{
        return self.value(forKey: "searchField") as! UITextField
    }
}

