//
//  AlertsInfoTableViewCell.swift
//  events
//
//  Created by Intcore on 3/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON

class AlertsInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var imageAlert: UIImageView!
    @IBOutlet weak var alertName: UILabel!
    @IBOutlet weak var timeofNotification: UILabel!
    @IBOutlet weak var body: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(_ data: Notifications){
        
        body.text = data.sessionDescrip
        print("showTimeFromString" , showTimeFromString( data.created_at ?? "") )
        timeofNotification.text = showTimeFromString( data.created_at ?? "")
        if data.type == "2" || data.type == "1"{
            imageAlert.image = #imageLiteral(resourceName: "request")
            alertName.alpha = 0
        }else{
            imageAlert.image = #imageLiteral(resourceName: "general")
            alertName.alpha = 1
            alertName.text = data.sessionTitle
        }
        
        if data.status == true {
            viewStatus.alpha = 0
        }else{
            if data.type == "3"{
                viewStatus.alpha = 0
            }else{
                viewStatus.alpha = 1
                viewStatus.backgroundColor = Color.greenColor
            }
        }
    }
    
     func showTimeFromString(_ time: String) -> String {
        let dateFormatter = DateFormatter()
     
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"

        let date = dateFormatter.date(from: time )
        let nowDate = Date()
        let ageComponents = Calendar.current.dateComponents([.minute, .hour, .day], from: date!, to: nowDate)
        let minute = ageComponents.minute
        let hour = ageComponents.hour
        let day = ageComponents.day

        if minute == 0 && hour == 0 && day == 0 {
            return "now"
        }
        else if (hour == 0 && day == 0 && minute != 0) {
            if minute! > 1 {
                return "\(String(describing: minute!)) minute ago"
            }
            else {
                return "minute ago"
            }
        }
        else if (hour! > 0 && day == 0) {
            if (hour! > 1) {
                return "\(String(describing: hour!)) hour ago"
            }
            else {
                return "hour ago"
            }
        }
        else {
            dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
            let postDate1 = dateFormatter.string(from:date!)
            return postDate1
        }
    }

}
