//
//  AlertsTableViewController.swift
//  events
//
//  Created by Intcore on 3/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import UserNotifications
import SwiftyJSON
class AlertsTableViewController: UITableViewController , UNUserNotificationCenterDelegate {
    
    var alertsArray: [Notifications] = []
    var agendaArray: [Agenda] = []
    var rooms = [Room]()

    @objc var refresh: UIRefreshControl!


    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewConfig()
        title = "Alert".locale
        getRequest()
        refresh = UIRefreshControl()
        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        refresh.tintColor = UIColor.black
        tableView.addSubview(refresh)
        tableView.tableFooterView = UIView()
        self.tableView.reloadData()
     
    }
    
    @objc func refreshData() {
        getRequest()
        self.setup()
    }
    func setup() {
        refresh.endRefreshing()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarHideConfig(isHidden: false)
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        tabBarHideConfig(isHidden: true)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
            return alertsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as!
        AlertsInfoTableViewCell
        cell.config(alertsArray[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        if alertsArray[indexPath.row].type == "local"{
//            let id = alertsArray[indexPath.row].sessionId ?? ""
//            
//            self.openSession(id)
//            alertsArray[indexPath.row].status = false
//            if alertsArray[indexPath.row].minutesType == "5" {
//                NotificationsUtil.removeOne(id + "-5")
//            }else{
//                NotificationsUtil.removeOne(id + "-15")
//            }
//            updateUserInfo(indexPath.row)
//        }else
            if alertsArray[indexPath.row].type == "2" || alertsArray[indexPath.row].type == "1" {

            let viewController = UIStoryboard(name: "People", bundle: nil).instantiateViewController(withIdentifier: "MainTableViewController") as! MainTableViewController
                
                viewController.pageType = "alert"
//            viewController.userFriendID = self.alertsArray[indexPath.row].user_id
//            viewController.type = "Alert"
//            print("Notifications ID",alertsArray[indexPath.row].notifications_id ?? "")
            navigationController?.pushViewController(viewController, animated: true)
            seenPushNotification(notificationID: alertsArray[indexPath.row].notifications_id ?? "", closure: { (done) in
                if done{
                    self.alertsArray[indexPath.row].status = true
                    tableView.reloadData()
                }
            })
        }else{
            return
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func updateUserInfo(_ indexPath: Int){
        let index = indexPath
        let sessionId = "\(alertsArray[index].sessionId ?? "")"
        print("\(alertsArray[index].sessionId ?? "")")
        let sessionDateString = "\(alertsArray[index].created_at ?? "")"
        let sessionTitle = "\(alertsArray[index].sessionTitle ?? "")"
        print("sessionDateString" , sessionDateString)
        let sessionDescrip = ""
        let createdDate = sessionDateString.timestamp


        print("createdDate" , createdDate)
        if alertsArray[index].minutesType == "5" {
            NotificationsUtil.set(sessionId+"-5", title: sessionTitle, body: sessionDescrip, date: createdDate, userInfo: ["sessionId" : sessionId+"-5" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 5 minutes" ,"minutesType" : "5", "created_at" : createdDate , "status": false , "type" : "local" , "startSession" : sessionDateString])
        }else{
            NotificationsUtil.set(sessionId+"-15", title: sessionTitle, body: sessionDescrip, date: createdDate, userInfo: [  "sessionId" : sessionId+"-15" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 15 minutes","minutesType" : "15" , "created_at" : createdDate , "status": false , "type" : "local" , "startSession":sessionDateString ])
        }
        
        tableView.reloadData()
    }
    
    func openSession(_ id:String ,_ sessions:[Agenda]? = nil) {
        let allSessions = sessions != nil ? sessions : AgendaUtil.allSessions()
        if let index = allSessions?.index(where: { $0.id == Int(id) }) {
            let agenda =  allSessions![index]
            agendaArray = [agenda]
            let cardController = UserCardViewController.init(nibName: "UserCardViewController", bundle: nil)
            cardController.agenda = agenda
            cardController.pageTyep = "MyAgeda"
            self.navigationController?.pushViewController(cardController, animated: true)
        }else{
            loadSessions({ (sessions) in
                self.openSession(id ,sessions)
            })
        }
    }
    
    func tableViewConfig()  {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 120
    }
    
    func loadSessions(_ completion: @escaping ( [Agenda]) -> ()) {
        StartLoading()
        Requests.instance.request(link: "agenda".route, method: .get,parameters: ["api_token":UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            if response.haveError{
//                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    DispatchQueue.main.async {
                        let notifications = response.data!["data"].map{Notifications($0.1 , type: 1)}
                        print("notifications" , notifications)
                        NotificationsUtil.saveNotify(notifications)
                        self.getLocalNotification(notifications)
                        self.view.stopLoading()
//                        let allData = response.data!["data"].dictionaryObject ?? [:]
//                        let agendaItems = allData.map({ (arg: (key: String, value: Any)) ->  AgendaItem in
//                            let (key, value) = arg
//                            return AgendaItem(key , JSON(value as? Array ?? []))
//                        })
//
//                        AgendaUtil.save(agendaItems)
//                        completion( AgendaUtil.allSessions(agendaItems))
//                        self.StopLoading()
                    }
                }
            }
        }
    }
    
    func getLocalNotification(_ remotNotifications:[Notifications]) {
        self.alertsArray.removeAll()
//        NotificationsUtil.removeAll()
        self.alertsArray += remotNotifications
        UNUserNotificationCenter.current().getPendingNotificationRequests { (notifications) in
            print("notifications Local Pending" , notifications)
            for notification in notifications {
                let sessionId = notification.content.userInfo["sessionId"] as? String
                let sessionTitle = notification.content.userInfo["sessionTitle"] as? String
                let sessionDescrip = notification.content.userInfo["sessionDescrip"] as? String
                let created_at = notification.content.userInfo["created_at"] as? Date
                let status = notification.content.userInfo["status"] as? Bool
                let type = notification.content.userInfo["type"] as? String
                let minutesType = notification.content.userInfo["minutesType"] as? String
                let sessionDateString = notification.content.userInfo["startSession"] as? String
                let sessionIdM15 = sessionId?.replacingOccurrences(of: "-15", with: "")
                let sessionIdFinal = sessionIdM15?.replacingOccurrences(of: "-5", with: "")
                let created = self.showTimeFromString(created_at!)
                print("created_at" , created)
                
                let data:[String:Any] = [
                    "sessionId" : sessionIdFinal ?? "" ,
                    "sessionTitle": sessionTitle ?? "" ,
                    "sessionDescrip": sessionDescrip ?? "" ,
                    "created_at" : created,
                    "status": status ?? 0 ,
                    "minutesType": minutesType ?? "" ,
                    "type": type ?? "" ,
                    "startSession" : sessionDateString ?? "" ]
                
                let notify = Notifications(JSON(data))
                
                var date15Minute: Date {
                    return (Calendar.current as NSCalendar).date(byAdding: .minute , value: -15, to: Date() , options: [])!
                }
                var date5Minute: Date {
                    return (Calendar.current as NSCalendar).date(byAdding: .minute , value: -5, to: Date() , options: [])!
                }
                
//                if (date15Minute >= Date() && Date() <= created_at! && minutesType == "15") || (date5Minute >= Date() && Date() <= created_at! && minutesType == "5") {
//                    self.alertsArray.append(notify)
//                }
                self.alertsArray = self.alertsArray.sorted{ $0.created_at ?? "" > $1.created_at ?? "" }
                
            }
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
     
    }
    
    func seenPushNotification(notificationID : String , closure: @escaping(Bool) -> Void){
        Requests.instance.request(link: Config.apiUrl + "user/notifications/\(notificationID)", method: .patch, parameters: ["api_token": UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    closure(true)
                }
            }
        }
    }
    
    func getRequest()  {
        if NotificationsUtil.loadNotify() != nil {
            self.alertsArray =  NotificationsUtil.loadNotify() ?? []
            self.view.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Notifications".route, method: .get,parameters: ["api_token": UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    self.view.stopLoading()
//                    DispatchQueue.main.async {
                        let notifications = response.data!["data"].map{Notifications($0.1 , type: 1)}
                        print("notifications" , notifications)
                        NotificationsUtil.saveNotify(notifications)
                        self.getLocalNotification(notifications)
//                    }
                }
            }
        }
    }
    

    func showTimeFromString(_ time: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +0000"
        let data = dateFormatter.string(from: time)
        return data
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


