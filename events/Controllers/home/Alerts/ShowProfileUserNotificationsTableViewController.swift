//
//  ShowProfileUserNotificationsTableViewController.swift
//  events
//
//  Created by Intcore on 3/26/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import MessageUI

class ShowProfileUserNotificationsTableViewController:BaseStaticTableViewController , MFMailComposeViewControllerDelegate {
    
    static var canSendMessageOrRequest: Int? = nil
    
    // MARK: - refrenaces
    @IBOutlet weak var userBioTitleLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userJobNameLabel: UILabel!
    @IBOutlet weak var userCompanyNameLabel: UILabel!
    @IBOutlet weak var userBioLabel: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var genderStuts: UILabel!
    @IBOutlet weak var ageStutas: UILabel!
    @IBOutlet weak var snedRequsetMessage: EventsButton! {
        didSet {
            snedRequsetMessage.backgroundColor = Color.greenForGradiantColor
            snedRequsetMessage.setTitle("Send Request Message", for: .normal)
            
        }
    }
    
    @IBOutlet weak var objdectiveTableView: UITableView!
    
    
    @IBOutlet weak var cellOneView: UIView!
    @IBOutlet weak var cellTwoView: UIView!
    @IBOutlet weak var cellThreeView: UIView!
    @IBOutlet weak var cellFourView: UIView!
    @IBOutlet weak var cellFifeView: UIView!
    @IBOutlet weak var cellSixView: UIView!
    
    @IBOutlet weak var linkedInButtonOutlet: UIButton!
    @IBOutlet weak var twitterButtonOutlet: UIButton!
    @IBOutlet weak var emailButtonOutlet: UIButton!
    

    var messages = [Message]()
    
    var userData: User?
    var type: String!
    var info: [String: Any]? // for
    var userFriendID: Int!
    var dataSource = ObjectiveTempTableView()
    
    var urlLinkedIn: String = ""
    var urlTwitter: String = ""
    var email: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        objdectiveTableView.dataSource = dataSource
        objdectiveTableView.delegate = dataSource
        helper.addNetworkObserver(on: self)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        helper.removeNetworkObserver(from: self)
    }
    
    private func setupButtonState(messagable: Messageable?, user: User) {
        if messagable == nil {
            snedRequsetMessage.setTitle("Send Request Message", for: .normal)
            ViewProfileTableViewController.canSendMessageOrRequest = nil
            self.snedRequsetMessage.isHidden = false
            self.view.stopLoading()
        }else if let messagable = messagable {
            if let status = messagable.status, let authorized = user.autherized_to_accept_chat {
                if status == 0 && authorized {
                    ViewProfileTableViewController.canSendMessageOrRequest = 0
                    snedRequsetMessage.setTitle("Pendeing", for: .normal)
                    snedRequsetMessage.isEnabled = false
                    snedRequsetMessage.backgroundColor = .white
                    snedRequsetMessage.titleColor = .black
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                }
                else if status == 1  {
                    let myToken = UserUtil.loadUser()?.api_token ?? ""
                    let user_id = userData?.id ?? 0
                    ChatAPI.getRoomWithMessages(room: nil, user_id: user_id, my_token: myToken, completion: { (messages,room, success, error) in
                        if let error = error {
                            self.DangerAlert("Error", message: error)
                            return
                        }
                        if let messages = messages {
                            self.messages = messages
                            ViewProfileTableViewController.canSendMessageOrRequest = 1
                            self.snedRequsetMessage.setTitle("Send Message", for: .normal)
                            self.snedRequsetMessage.isEnabled = true // reject me for ever
                            self.snedRequsetMessage.backgroundColor = Color.greenForGradiantColor
                            self.snedRequsetMessage.titleLabel?.textColor = .white
                            self.snedRequsetMessage.isHidden = false
                            self.view.stopLoading()
                        }
                    })
                    
                    
                }
                else if status == 2 && authorized {
                    ViewProfileTableViewController.canSendMessageOrRequest = 2
                    snedRequsetMessage.setTitle("You have a request", for: .normal) //  sent and not accept yet.....
                    snedRequsetMessage.isEnabled = false
                    snedRequsetMessage.backgroundColor = UIColor.white
                    snedRequsetMessage.layer.borderColor = UIColor.white.cgColor
                    snedRequsetMessage.layer.borderWidth = 0
                    snedRequsetMessage.shadowColor = .clear
                    snedRequsetMessage.titleColor = .black
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                }
                else if status == 2 && authorized == false {
                    ViewProfileTableViewController.canSendMessageOrRequest = 2
                    snedRequsetMessage.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
                    snedRequsetMessage.setTitle("Pending", for: .normal) //  me has a pending and accept or reject.....
                    snedRequsetMessage.shadowColor = .lightGray
                    snedRequsetMessage.layer.shadowOffset.height = 3
                    snedRequsetMessage.layer.shadowOpacity = 0.5
                    snedRequsetMessage.layer.shadowRadius = 6
                    snedRequsetMessage.isEnabled = false
                    snedRequsetMessage.titleColor = .black
                    self.snedRequsetMessage.isHidden = false
                    self.view.stopLoading()
                    
                }
            }else{
                snedRequsetMessage.setTitle("Send Request Message", for: .normal)
                ViewProfileTableViewController.canSendMessageOrRequest = nil
                self.snedRequsetMessage.isHidden = false
                self.view.stopLoading()
            }
        }
        
    }
    func loadOfflineUser() {
        self.setupButtonState(messagable: userData?.messageable, user: userData!)
    }
    
    // MARK: - update profile acction
    override func editNavBtn() {
        self.performSegue(withIdentifier: "updateProfile", sender: nil)
    }
    
    // MARK: - Setup
    func setup() {
        title = "Profile".locale
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        let my_token = UserUtil.loadUser()?.api_token ?? ""
        print("userFriendID" , userFriendID)
        self.view.startLoading()

        ChatAPI.getProfileOfUserForDisplay(id: userFriendID, token: my_token) { (user, success, error) in
            if let error = error {
                self.DangerAlert("Error", message: error)
                self.snedRequsetMessage.isHidden = false
                self.view.stopLoading()
                return
            }
            if let user = user {
                self.addUserData(user)
                self.userData = user
                self.setupButtonState(messagable: user.messageable, user: user)
                self.cellOneView.alpha = 0
                self.cellTwoView.alpha = 0
                self.cellThreeView.alpha = 0
                self.cellFourView.alpha = 0
                self.cellFifeView.alpha = 0
                self.cellSixView.alpha = 0

                self.view.stopLoading()
            }
        }
        tableView.reloadDataAnimatedKeepingOffset()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.tableView.reloadData()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 4 {
            objdectiveTableView.layoutIfNeeded()
            print("objdectiveTableView.contentSize.height" , objdectiveTableView.contentSize.height)
            return objdectiveTableView.contentSize.height + 40
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    @IBAction func sendRequestOrMessageButton(_ sender: EventsButton)
    {
        if ViewProfileTableViewController.canSendMessageOrRequest == 1 {
            performSegue(withIdentifier: "GoToChat", sender: messages)
            return
        }
        else if ViewProfileTableViewController.canSendMessageOrRequest == nil{
            self.snedRequsetMessage.isEnabled = false
            if let id = userData?.id {
                ChatAPI.sendMessageRequest(id: id, completion: { (room,user, granted, error) in
                    if let error = error {
                        self.DangerAlert("Error", message: error)
                        self.snedRequsetMessage.isEnabled = true
                        return
                    }
                    if let granted = granted
                    {
                        if granted
                        {
                            self.snedRequsetMessage.setTitle("Pending", for: .normal)
                            self.snedRequsetMessage.isEnabled = false
                            self.snedRequsetMessage.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                            self.snedRequsetMessage.titleColor = .black
                        }
                    }
                })
            }
        }
        else if ViewProfileTableViewController.canSendMessageOrRequest == 0 || ViewProfileTableViewController.canSendMessageOrRequest == 2 {
            self.snedRequsetMessage.isEnabled = false
            
            return
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToChat" {
            if let vc = segue.destination as? ChatViewController {
                vc.messages = (sender as? [Message])!
                vc.come_from = "Profile"
                vc.user = self.userData
            }
            
        }
    }
    
    @IBAction func linkedInShatActio(_ sender: UIButton) {
        let url = "https://www.linkedin.com/in/\(urlLinkedIn))"
        print(url)
        openLinkWithSafari(link: url )
    }
    @IBAction func twitterAction(_ sender: UIButton) {
        let url = "https://www.twitter.com/\(urlTwitter)"
        print(url)
        openLinkWithSafari(link: url )
        
    }
    
    @IBAction func googleMailAction(_ sender: UIButton) {
        sendEmail()
    }
    
    func sendEmail() {
        
        let googleUrlString = "googlegmail:///co?to=\(email)&subject=Hello&body=Hi" //
        if let googleUrl = NSURL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl as URL)
                }
            }else{
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["\(email)"])
                    
                    self.present(mail, animated: true, completion: nil)
                } else {
                    // show failure alert
                }
                
            }
        }
        
    }
    
    
    func addUserData(_ user: User){
   
        urlLinkedIn = user.linkedin ?? ""
        urlTwitter = user.twitter ?? ""
        email = user.email ?? ""
        let img = "\(Config.imageUrl + (user.image)!)"
        print(img)
        if UserUtil.loadUser()?.id == user.id{
            snedRequsetMessage.alpha = 0
            linkedInButtonOutlet.alpha = 0
            twitterButtonOutlet.alpha = 0
            emailButtonOutlet.alpha = 0
        }else{
            linkedInButtonOutlet.alpha = 1
            twitterButtonOutlet.alpha = 1
            emailButtonOutlet.alpha = 1
            snedRequsetMessage.alpha = 1
            if urlLinkedIn == "" {
                linkedInButtonOutlet.isEnabled = false
                if urlTwitter == ""{
                    twitterButtonOutlet.isEnabled = false
                }else{
                    twitterButtonOutlet.isEnabled = true
                    
                }
            }else{
                linkedInButtonOutlet.isEnabled = true
                if urlTwitter == ""{
                    twitterButtonOutlet.isEnabled = false
                }else{
                    twitterButtonOutlet.isEnabled = true
                    
                }
            }
            
            if urlTwitter == ""{
                twitterButtonOutlet.isEnabled = false
                if urlLinkedIn == "" {
                    linkedInButtonOutlet.isEnabled = false
                }else{
                    linkedInButtonOutlet.isEnabled = true
                }
                
            }else{
                twitterButtonOutlet.isEnabled = true
                if urlLinkedIn == "" {
                    linkedInButtonOutlet.isEnabled = false
                }else{
                    linkedInButtonOutlet.isEnabled = true
                }
                
            }
            emailButtonOutlet.isEnabled = true
        }
        self.userImageView.af_setImage(withURL: URL(string: img)!)
        self.usernameLabel.text = user.username
        self.userJobNameLabel.text = user.job_title
        self.userCompanyNameLabel.text = user.company
        self.userBioLabel.text = user.bio
        if user.gender == nil{
            self.gender.text = "___"
        }else{
            self.genderStuts.alpha = 1
            if user.gender == "1" {
                self.gender.text = "Male"
            }else{
                self.gender.text = "Female"
            }
        }
        
        if user.age == 0{
//            self.ageStutas.alpha = 0
//            self.age.alpha = 0
            self.age.text = "___"
        }else{
            self.age.text = "\(user.age ?? 0)"
        }
        if user.bio == nil{
            userBioLabel.text = "____"
        }else{
            userBioLabel.text = user.bio
            
        }
//        self.userBioTitleLabel.isHidden = user.bio == nil || user.bio == ""
        
    }
}


