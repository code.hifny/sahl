//
//  SpeakersDataCollectionViewCell.swift
//  events
//
//  Created by Macbook Pro on 2/20/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class SpeakersDataCollectionViewCell: UICollectionViewCell , favouriteProtocol {
   
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var speakerJob: UILabel!
    
    var speaker:Speakers?
    var indexSpeaker: Int?
    var favouriteAction: Int?

    func config(_ speaker: Speakers , index: Int) {
        self.speaker = speaker
        self.indexSpeaker = index
        let imageSpeaker = "\(Config.imageUrl + (speaker.image ?? ""))"
        
        speakerImage.af_setImage(withURL: URL(string: imageSpeaker)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.4), runImageTransitionIfCached: false, completion: nil)
        
        speakerName.text = speaker.username
        speakerJob.text = speaker.job_title
        
        if speaker.is_favorited == 1{
           favouriteButton.setBackgroundImage(#imageLiteral(resourceName: "star"), for: .normal)
        }else{
            favouriteButton.setBackgroundImage(#imageLiteral(resourceName: "star-2"), for: .normal)
        }
            
    }
    
    @IBAction func favAction(_ sender: UIButton) {
        sender.startLoading()
        sender.isEnabled = false
        if UserUtil.loadUser()?.api_token == nil{
            sender.stopLoading()
            UIApplication.topViewController()?.DangerAlert(message: "Pleace Login".locale)
        }else{
            print( "sender.tag" , sender.tag)
            if self.speaker?.is_favorited == 0{
                AddFav(sender.tag , sender:sender)
            }else{
                RemoveFav(sender.tag , sender:sender)
            }
        }
    }
    func actionFavourite(done: Int , index: Int) {
        print("done" , done)
        self.favouriteAction = done
        print("done" , favouriteAction)
        self.speaker?.is_favorited = done
//        if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
//            top.collectionView?.reloadData()
//            top.speakersArray[index].is_favorited = done
//            top.speakersArrayFilter[index].is_favorited = done
//
//        }
    }
    
    
    @IBAction func speakerProfile(_ sender: UIButton) {
        if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
            top.searchBar.text = ""
            top.closeSearch()
            top.navigationController?.navigationBar.shadowImage = UIImage()
            let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SpeakerProfileViewController") as! SpeakerProfileViewController
            viewController.speakerInfo = speaker
            viewController.index = indexSpeaker
            viewController.done = self
            top.speakersArray = top.speakersArrayFilter
            let navigationController = EventsNavigationController(rootViewController: viewController)
            top.present(navigationController, animated: true, completion: nil)
//            top.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    // MARK: - Favourite Function
    func favouriteSpeakerServices(speakerID: Int ,  closure: @escaping(Bool) -> Void){
        let parameter = ["api_token": UserUtil.loadUser()?.api_token ?? ""] as [String: Any]
        Requests.instance.request(link: Config.apiUrl + "user/speaker/\(speakerID)/favorite", method: .post, parameters: parameter) { (response) in
            if response.haveError{
                UIApplication.topViewController()?.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print("Done")
                    closure(true)
                }
            }
        }
        
    }
    

    var collectionView: UICollectionView?
    func AddFav(_ index: Int , sender: UIButton){
        self.speaker?.is_favorited = 1
        favouriteSpeakerServices(speakerID: self.speaker?.id ?? 0) { (done) in
            if done{
                sender.isEnabled = true
                sender.stopLoading()
                if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
                    top.speakersArray.first(where: { $0.id == self.speaker?.id} )?.is_favorited
                    SpeakersUtil.save(top.speakersArray)
                    top.collectionView?.reloadData()
                    self.collectionView?.reloadData()
                }
                print("Done")
            }
        }
    }
    
    func RemoveFav(_ index: Int , sender: UIButton){
        self.speaker?.is_favorited = 0
        favouriteSpeakerServices(speakerID: self.speaker?.id ?? 0) { (done) in
            if done{
                sender.isEnabled = true
                sender.stopLoading()
                print("Done")
                if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
                    top.speakersArray.first(where: { $0.id == self.speaker?.id} )?.is_favorited
                    SpeakersUtil.save(top.speakersArray)
                    top.collectionView?.reloadData()
                    self.collectionView?.reloadData()
                }
            }
        }
    }
    
    
//    var speaker:Speakers?
//    var indexSelected = 0
//    func config(_ data:Speakers) {
//        speaker = data
//        let imageSpeaker = "\(Config.imageUrl + (data.image ?? ""))"
//
//        speakerImage.af_setImage(withURL: URL(string: imageSpeaker)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.4), runImageTransitionIfCached: false, completion: nil)
//
//        speakerName.text = data.username ?? ""
//        speakerJob.text = data.job_title ?? ""
//        if data.is_favorited == 1{
//            favouriteButton.setBackgroundImage(#imageLiteral(resourceName: "star"), for: .normal)
//        }else{
//           favouriteButton.setBackgroundImage(#imageLiteral(resourceName: "star-2"), for: .normal)
//        }
//    }
//
//    @IBAction func favAction(_ sender: Any) {
//        if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
//            if UserUtil.loadUser()?.api_token == nil{
//                top.DangerAlert(message: "Pleace Login".locale)
//            }else{
////                self.favouriteButton.startLoading()
//                if self.speaker?.is_favorited == 0{
//                    AddFav(closure: { (done) in
//                        if done{
////                            self.favouriteButton.stopLoading()
//                        }
//                    })
//
//                }else{
//                    RemoveFav(closure: { (done) in
//                        if done{
////                            self.favouriteButton.stopLoading()
//                        }
//
//                    })
//
//                }
//            }
//
//        }
//
//    }
//    // MARK: - Favourite Function
//    func favouriteSpeakerServices(speakerID: Int ,  closure: @escaping(Bool) -> Void){
//        print("speakerID" ,speakerID)
//        let parameter = ["api_token": UserUtil.loadUser()?.api_token ?? ""] as [String: Any]
//        Requests.instance.request(link: Config.apiUrl + "user/speaker/\(speakerID)/favorite", method: .post, parameters: parameter) { (response) in
//            if response.haveError{
//                if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
//                    top.DangerAlert(message: "Pleace Login".locale)
//                }
//            }else{
//                if response.data?["status"].bool == true {
//                    print("Done")
//                    closure(true)
//                }
//            }
//        }
//
//    }
//
//    func AddFav(closure: @escaping(Bool) -> Void){
//        if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
//            if UserUtil.loadUser()?.api_token == nil{
//                top.DangerAlert(message: "Pleace Login".locale)
//            }else{
//                self.speaker?.is_favorited = 1
//                top.collectionView?.reloadData()
//                favouriteSpeakerServices(speakerID: self.speaker?.id ?? 0) { (done) in
//                    if done{
//                        print("Done")
//                        closure(done)
//                    }
//                }
//            }
//        }
//    }
//
//    func RemoveFav(closure: @escaping(Bool) -> Void){
//        if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
//            if UserUtil.loadUser()?.api_token == nil{
//                top.DangerAlert(message: "Pleace Login".locale)
//            }else{
//                self.speaker?.is_favorited = 0
//                top.collectionView?.reloadData()
//                favouriteSpeakerServices(speakerID: self.speaker?.id ?? 0) { (done) in
//                    if done{
//                        print("Done")
//                        closure(done)
//                    }
//                }
//            }
//        }
//    }
//
//    @IBAction func speakerAction(_ sender: Any) {
//        if let top = UIApplication.topViewController() as? SpeakersCollectionViewController {
//            let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SpeakerProfileViewController") as! SpeakerProfileViewController
//            viewController.speakerInfo = speaker
//            top.navigationController?.pushViewController(viewController, animated: true)
//        }
//
//    }
    
}
