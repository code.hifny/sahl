//
//  SpeakerProfileViewController.swift
//  events
//
//  Created by Macbook Pro on 2/21/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI
import Material

class SpeakerProfileViewController: UIViewController , MFMailComposeViewControllerDelegate    {
    // MAEK: - Outlets
    @IBOutlet weak var speakerImage: UIImageView!
    @IBOutlet weak var linkedInButtonOutlet: UIButton!
    @IBOutlet weak var twitterButtonOutlet: UIButton!
    @IBOutlet weak var emailButtonOutlet: UIButton!
    // MAEK: - Varibale
    var speakerInfo: Speakers?
    var arrayData: [Speakers] = []
    var index: Int?
    var done: favouriteProtocol!
    var pageType: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearNav()
        exitButtonAddNav()
        addData()
        addFavouriteButton()
        print("index" , index ?? 0)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navConfigBlak()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        navConfig()
        print("speakerInfo?.is_favorited ?? 0" , speakerInfo?.is_favorited ?? 0)
        print("index" , index ?? 0)
        if pageType != "Agenda"{
            done.actionFavourite(done: speakerInfo?.is_favorited ?? 0, index: index ?? 0)
        }

    }
    
    func addData(){
        self.childViewControllers.forEach({ (child) in
            if let vc = child as? SpeakerProfileInfoViewController {
                vc.speakerInfo = self.speakerInfo
                vc.addData()
            }
        })
        let image = "\(Config.imageUrl + (speakerInfo?.image!)!)"
        speakerImage.af_setImage(withURL: URL(string: image)!)
        let urlLinkedIn = speakerInfo?.linkedin ?? ""
        let urlTwitter = speakerInfo?.twitter ?? ""
        
        if UserUtil.loadUser()?.api_token == nil{
            linkedInButtonOutlet.isEnabled = false
            twitterButtonOutlet.isEnabled = false
            emailButtonOutlet.isEnabled = false
        }else{
            print(urlLinkedIn)
            if urlLinkedIn == "" {
                linkedInButtonOutlet.isEnabled = false
                if urlTwitter == ""{
                    twitterButtonOutlet.isEnabled = false
                }else{
                    twitterButtonOutlet.isEnabled = true
                    
                }
            }else{
                linkedInButtonOutlet.isEnabled = true
                if urlTwitter == ""{
                    twitterButtonOutlet.isEnabled = false
                }else{
                    twitterButtonOutlet.isEnabled = true
                    
                }
            }
            
            if urlTwitter == ""{
                twitterButtonOutlet.isEnabled = false
                if urlLinkedIn == "" {
                    linkedInButtonOutlet.isEnabled = false
                }else{
                    linkedInButtonOutlet.isEnabled = true
                }
                
            }else{
                twitterButtonOutlet.isEnabled = true
                if urlLinkedIn == "" {
                    linkedInButtonOutlet.isEnabled = false
                }else{
                    linkedInButtonOutlet.isEnabled = true
                }            }
            emailButtonOutlet.isEnabled = true
        }
    }
    
    func addFavouriteButton(){
        let button: UIButton = UIButton (type:.custom)
        button.tintColor = Color.greenForGradiantColor
        button.frame = CGRect(x:0, y:0, width:24, height:24)
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(favourite), for: .touchUpInside)
        if speakerInfo?.is_favorited == 1{
            button.setBackgroundImage(UIImage(named:"star"), for: .normal)
//            button =  IconButton(image:  UIImage(named:"star") , tintColor: Color.greenColor)
        }else{
            button.setBackgroundImage(UIImage(named:"star-white"), for: .normal)
//            button =  IconButton(image:  UIImage(named:"star-white") , tintColor: Color.greenColor)
        }
        let BarButtonItem =  UIBarButtonItem(customView: button)
        BarButtonItem.tintColor = Color.greenColor
        self.navigationItem.rightBarButtonItem = BarButtonItem//setRightBarButton(BarButtonItem, animated: true)

    }
    
    // MARK: - Favourite Function
    
    @objc func favourite(){
        
        if UserUtil.loadUser()?.api_token == nil{
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            if self.speakerInfo?.is_favorited == 0{
                AddFav()
            }else{
                RemoveFav()
            }
        }
    }
    
    func favouriteSpeakerServices(closure: @escaping(Bool) -> Void){
        let parameter = ["api_token": UserUtil.loadUser()?.api_token ?? ""] as [String: Any]
        let id = String(describing: speakerInfo?.id ?? 0)
        print(id)
        Requests.instance.request(link: Config.apiUrl + "user/speaker/\(id)/favorite", method: .post, parameters: parameter) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print("Done")
                    closure(true)
                }
            }
        }
        
    }
    
    func AddFav(){
        if UserUtil.loadUser()?.api_token == nil{
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            self.speakerInfo?.is_favorited = 1
            addFavouriteButton()
            if pageType != "Agenda"{
                self.done.actionFavourite(done: 1 , index: index ?? 0)
//                done.actionFavourite(done: speakerInfo?.is_favorited ?? 0, index: index ?? 0)
            }
            favouriteSpeakerServices(closure: { (done) in
                if done{
                    //  self.getBackRequest(true)
                    print("Done")
                }
            })
        }
    }
    
    func RemoveFav(){
        if UserUtil.loadUser()?.api_token == nil{
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            
            self.speakerInfo?.is_favorited = 0
            addFavouriteButton()
            if pageType != "Agenda"{
                self.done.actionFavourite(done: 0 , index: index ?? 0)
            }
//            self.done.actionFavourite(done: 0, index: index ?? 0)
            favouriteSpeakerServices(closure: { (done) in
                if done{
                    print("Done")
                }
            })
        }
    }
    
    
    @IBAction func linkedInShatActio(_ sender: UIButton) {
        let url = "https://www.linkedin.com/in/\(speakerInfo?.linkedin ?? ""))"
        print(url)
        openLinkWithSafari(link: url )
    }
    @IBAction func twitterAction(_ sender: UIButton) {
        let url = "https://www.twitter.com/\(speakerInfo?.twitter ?? "")"
        print(url)
        openLinkWithSafari(link: url )
        
    }
    
    @IBAction func googleMailAction(_ sender: UIButton) {
        sendEmail()
    }
    
    func sendEmail() {
        
        let googleUrlString = "googlegmail:///co?to=\(speakerInfo?.email ?? "")&subject=Hello&body=Hi" //
        if let googleUrl = NSURL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl as URL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl as URL)
                }
            }else{
                if MFMailComposeViewController.canSendMail() {
                    let mail = MFMailComposeViewController()
                    mail.mailComposeDelegate = self
                    mail.setToRecipients(["\(speakerInfo?.email ?? "")"])
                    
                    self.present(mail, animated: true, completion: nil)
                } else {
                    // show failure alert
                }
                
            }
        }
        
    }
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        dismiss(animated: true, completion: nil)
    }
    
    func exitButtonAddNav(){
        //        self.navigationController?.navigationBar.tintColor = UIColor.black
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(BackToLogin))
        leftAddBarButtonItemImage.tintColor = Color.blueForGradiantColor
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: true)
    }
    @objc func BackToLogin(){
        if pageType == "Agenda"{
            self.dismiss(animated: true, completion: nil)
        }else{
            done.actionFavourite(done: speakerInfo?.is_favorited ?? 0, index: index ?? 0)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
}

