//
//  SpeakerProfileInfoViewController.swift
//  events
//
//  Created by Macbook Pro on 2/27/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class SpeakerProfileInfoViewController: UIViewController {
   
    // MARK: - Outlets
    @IBOutlet weak var speakerName: UILabel!
    @IBOutlet weak var speakerTitle: UILabel!
    @IBOutlet weak var bio: UITextView!
    
    // MARK: - Variables
    var speakerInfo: Speakers?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addData(){
        speakerName.text = speakerInfo?.username ?? ""
        let jobTitle = speakerInfo?.job_title ?? ""
        let comapny = speakerInfo?.company ?? ""
        if speakerInfo?.job_title == nil{
            speakerTitle.text = "\(comapny)"
        }else if speakerInfo?.company == nil{
            speakerTitle.text = "\(jobTitle)"
        }else{
            speakerTitle.text = "\(jobTitle + "," + comapny)"
        }
        bio.text = speakerInfo?.bio ?? ""
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
