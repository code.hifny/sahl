//
//  SpeakersCollectionViewController.swift
//  events
//
//  Created by Macbook Pro on 2/20/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift

//private let reuseIdentifier = "Cell"
protocol favouriteProtocol {
    func actionFavourite(done: Int , index: Int)
}

class SpeakersCollectionViewController: UICollectionViewController , UISearchBarDelegate {
    
    // MARK:- Variables
    let itemsPerRow: CGFloat = 2
    let sectionInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
    var speakersArray: [Speakers] = []
    var speakersArrayFilter: [Speakers] = []
    var refresh: UIRefreshControl!
    var selectedIndex = -1
    var chaeckSearchOpen: Bool = false
    var favouriteAction: Int?
    
    
    // MARK:- Search Bar
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .system)
    let searcClosehButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Speakers".locale

        navigationController?.navigationBar.tintColor = UIColor.black
        addButtonSearch()
        getRequest()
        
        refresh = UIRefreshControl()
        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        refresh.tintColor = UIColor.black
        collectionView?.addSubview(refresh)
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func refreshData() {
        getRequest()
        self.setup()
    }
    
    func setup() {
        refresh.endRefreshing()
        collectionView?.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarHideConfig(isHidden: false)
        collectionView?.reloadData()
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        tabBarHideConfig(isHidden: true)
    }
    
    func actionFavourite(done: Int , index: Int) {
        self.favouriteAction = done
        self.speakersArray[index].is_favorited = done
        self.collectionView?.reloadData()
    }
    
    
    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        if speakersArray.count == 0 {
            BackNotFounCollection(collectionView: self.collectionView!, text: "No Speakers Available")
        }else {
            BackNotFounCollection(collectionView: self.collectionView!, text: "")
        }
        return speakersArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "itemCell", for: indexPath) as! SpeakersDataCollectionViewCell
        cell.config(speakersArray[indexPath.row] , index: indexPath.row)
        cell.collectionView = collectionView
        return cell
    }
    
    func getRequest()  {
        if SpeakersUtil.load() != nil {
            self.speakersArray =  SpeakersUtil.load() ?? []
            self.speakersArrayFilter = SpeakersUtil.load() ?? []
            self.view.stopLoading()
            loadData(false)
        }else{
            loadData()
        }
        
    }
    
    
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Speakers".route, method: .get,parameters: ["api_token": UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let speakers = response.data!["data"]["speakers"].map{Speakers($0.1)}
                    SpeakersUtil.save(speakers)
                    self.speakersArray = speakers
                    self.speakersArrayFilter = self.speakersArray
                    self.view.stopLoading()
                    self.collectionView?.reloadData()
                    
                }
            }
        }
        
    }
 
}
extension SpeakersCollectionViewController : UICollectionViewDelegateFlowLayout {
    //1
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        //2
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: 200)
        
    }
    //3
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    // 4
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}
extension SpeakersCollectionViewController {
    // MARK:- Search Bar
    func addButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchButton.setImage(#imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searchButton.addTarget(self, action: #selector(searchController), for: .touchUpInside)
        navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: searchButton)
    }
    
    func addCloseButtonSearch(){
        self.chaeckSearchOpen = true
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search"
        searcClosehButton.setImage(#imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysOriginal), for: .normal)
        searcClosehButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searcClosehButton.addTarget(self, action: #selector(closeSearch), for: .touchUpInside)
        navigationItem.leftBarButtonItem =  UIBarButtonItem(customView: searcClosehButton)
    }
    
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        let textFieldInsideSearchBarLabel = searchBar.textField.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        return true
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        let stringText = searchBar.text!
        print(stringText)
        speakersArray = speakersArrayFilter.filter({ $0.username?.range(of: stringText , options:.caseInsensitive) != nil})
        if(stringText.count < 1){
            speakersArray = speakersArrayFilter
        }
        self.collectionView?.reloadData()
        
    }
    
    
    @objc func searchController(){
        searchBar.alpha = 1
        if #available(iOS 11.0, *) {
            navigationItem.hidesSearchBarWhenScrolling = false
        } else {
            // Fallback on earlier versions
        }
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.8, animations: {
            if let textfield = self.searchBar.value(forKey: "searchField") as? UITextField {
                if textfield.subviews.first != nil {
                    textfield.textColor = UIColor.white
                }
            }
            self.addCloseButtonSearch()
            self.searcClosehButton.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
        
        definesPresentationContext = true
    }
    
    @objc func closeSearch(){
        navigationItem.setRightBarButton(nil, animated: true)
        UIView.animate(withDuration: 0.3) {
            self.chaeckSearchOpen = false
            self.searchBar.endEditing(false)
            self.searchBar.text = ""
            self.searchBar.alpha = 0
            self.addButtonSearch()
            self.searchButton.alpha = 1
            self.searcClosehButton.alpha = 0
            self.navigationItem.titleView = nil
        }
        self.speakersArray = self.speakersArrayFilter
        self.collectionView?.reloadData()
    }
    
}


