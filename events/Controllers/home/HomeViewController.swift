//
//  HomeViewController.swift
//  Events
//
//  Created by abdelrahman on 2/1/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON
import Realm
import RealmSwift
import Material
import UserNotifications
//UISearchResultsUpdating
class HomeViewController: UIViewController, UISearchBarDelegate{
    
    //    @IBOutlet weak var floatingButtonOutlet: FABButton!
    var pageType: String?
    @IBOutlet weak var floatingButtonOutlet: FABButton!
    
    static var instance: HomeViewController {
        return HomeViewController()
        
    }
    
    let realm = try! Realm()
    lazy var eventRealm: Results<Event> = { self.realm.objects(Event.self) }()
    
    // MARK: - Search
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .system)
    let searcClosehButton = UIButton(type: .system)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        tabBarHideConfig(isHidden: false)
        if UserUtil.loadUser()?.ticket_info?.addressEn == nil {
            floatingButtonOutlet.alpha = 0
        }else{
            floatingButtonOutlet.alpha = 1
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tabBarHideConfig(isHidden: false)
//        if pageType == "appDelegete"{
//            let mainVC =  UIStoryboard(name: "People", bundle: nil).instantiateViewController(withIdentifier: "MainTableViewController") as! MainTableViewController
//            //            let navCon = UINavigationController(rootViewController: mainVC)
//            //            tabBarController?.navigationController?.topViewController
//            navigationController?.pushViewController(mainVC, animated: true)
//        }
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        tabBarHideConfig(isHidden: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarHideConfig(isHidden: true)
    }
    
    @IBAction func floatingButtonAction(_ sender: Any) {
        let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "ShowTicketInfoTableViewController") as! ShowTicketInfoTableViewController
        let nav = EventsNavigationController(rootViewController: viewController)
        self.present(nav, animated: true, completion: nil)
    }
    
    func setup() {
        addButtonSearch()
        title = "Home".locale
        if eventRealm.count > 0 {
            self.appendRealm()
            getBackRequest(false)
        }else{
            getBackRequest(true)
        }
        
    }
    
    func appendRealm() {
        self.childViewControllers.forEach({ (child) in
            if let vc = child as? HomeTableViewController {
                vc.event = self.eventRealm[0]
                vc.setup()
            }
        })
    }
    
    func getBackRequest(_ isFirst:Bool)  {
        if isFirst{
            self.view.startLoading()
        }
        loadData { (status , event)  in
            if isFirst{
                self.view.stopLoading()
            }
            self.appendRealm()
        }
    }
    
    func loadData(_ completion: @escaping (Bool ,Results<Event>? ) -> ()) {
        
        Requests.instance.request(link: "event".route, method: .get) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
                completion(false, nil)
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let event = Event(response.data!["data"])
                    
                    // Persist your data easily
                    try! self.realm.write {
                        self.realm.delete(self.realm.objects(Event.self))
                        self.realm.add(event)
                    }
                    
                    self.eventRealm = self.realm.objects(Event.self)
                    completion(true ,self.eventRealm)
                }
            }
        }
    }
}


extension HomeViewController {
    // MARK:- Search Bar
    func addButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchButton.setImage(#imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searchButton.addTarget(self, action: #selector(searchController), for: .touchUpInside)
        navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: searchButton)
    }
    
    //    func addCloseButtonSearch(){
    //        searchBar.delegate = self
    //        searchBar.searchBarStyle = UISearchBarStyle.minimal
    //        searchBar.tintColor = UIColor.white
    //        searchBar.placeholder = "Search"
    //        searcClosehButton.setImage(#imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysOriginal), for: .normal)
    //        searcClosehButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
    //        searcClosehButton.addTarget(self, action: #selector(closeSearch), for: .touchUpInside)
    //        navigationItem.leftBarButtonItem =  UIBarButtonItem(customView: searcClosehButton)
    //    }
    //    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    //        let stringText = searchBar.text!
    //        print(stringText)
    //        eventNews = eventNewsFilter.filter({ $0.title?.range(of: stringText , options:.caseInsensitive) != nil})
    //        if(stringText.count < 1){
    //            eventNews = eventNewsFilter
    //        }
    //
    //        self.childViewControllers.forEach({ (child) in
    //            if let vc = child as? HomeTableViewController {
    //                vc.setup()
    //            }
    //        })
    //    }
    
    @objc func searchController(){
        performSegue(withIdentifier: "showSearch", sender: self)
        //        searchBar.alpha = 0
        ////        searchBar.searchResultsUpdater = self
        //        navigationItem.hidesSearchBarWhenScrolling = false
        //        navigationItem.titleView = searchBar
        //        navigationItem.setLeftBarButton(nil, animated: true)
        //        UIView.animate(withDuration: 0.8, animations: {
        //            if let textfield = self.searchBar.value(forKey: "searchField") as? UITextField {
        //                if textfield.subviews.first != nil {
        //                    textfield.textColor = UIColor.white
        //                }
        //            }
        ////            self.addCloseButtonSearch()
        ////            self.searcClosehButton.alpha = 1
        //        }, completion: { finished in
        //            self.searchBar.becomeFirstResponder()
        //        })
        
        //        definesPresentationContext = true
        
    }
    //
    //    @objc func closeSearch(){
    //        navigationItem.setRightBarButton(nil, animated: true)
    //        UIView.animate(withDuration: 0.8) {
    //            self.searchBar.alpha = 0
    //            self.addButtonSearch()
    //            self.searchButton.alpha = 1
    //            self.searcClosehButton.alpha = 0
    //        }
    //    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        if let nav = segue.destination as? EventsNavigationController {
            nav.viewControllers.forEach({ (controller) in
                if let vc =  controller as? SearchTableViewController {
                    vc.eventNews = Array(eventRealm[0].news)
                    vc.eventNewsFilter = Array(eventRealm[0].news)
                }
            })
        }
        // Pass the selected object to the new view controller.
    }
}



