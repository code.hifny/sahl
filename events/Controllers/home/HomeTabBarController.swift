//
//  HomeTabBarController.swift
//  events
//
//  Created by abdelrahman on 2/11/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import EasyLocalization

class HomeTabBarController: UITabBarController {
    var agendaButton = UIButton(frame: CGRect(x: 0, y:0, width: 64, height: 64))
    //    var selected: Int = 0
    var pageType: String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.items?[0].title = "Home".locale
        self.tabBar.items?[1].title = "Agenda".locale
        self.tabBar.items?[2].title = "Speakers".locale
        
        if UserUtil.loadUser()?.api_token == nil {
            self.tabBar.items?[3].title = "Alert".locale
            self.tabBar.items?[3].selectedImage = UIImage(named: "alert")
            self.tabBar.items?[3].image = UIImage(named: "alert")
        }else{
            self.tabBar.items?[3].title = "Alert".locale
//            self.tabBar[3].selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
//            self.tabBar[3].image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        
//        self.tabBar.items?[3].title = "Alert".locale
        self.tabBar.items?[4].title = "Menu".locale
        for item in self.tabBar.items!{
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAgendaButton()
    }
    

    func setupAgendaButton() {
        view.viewWithTag(220)?.removeFromSuperview()
        var agendaButtonFrame:CGRect = CGRect(x: 0, y:0, width: view.frame.width / 4 , height: 64)
        if UIDevice.isIphoneX {
            // is iPhoneX
            agendaButtonFrame = CGRect(x: 0, y:0, width: view.frame.width / 4 , height: 100)
            agendaButtonFrame.origin.y = view.frame.height - agendaButtonFrame.height + 20
        } else {
            // is not iPhoneX
            agendaButtonFrame = CGRect(x: 0, y:0, width: view.frame.width / 4 , height: 64)
            agendaButtonFrame.origin.y = view.frame.height - agendaButtonFrame.height + 2
        }
        let x1 = ((view.frame.width / 5) * 2) - agendaButtonFrame.size.width + 10
        let x2 =  ((view.frame.width / 5) * 4) - agendaButtonFrame.size.width  + 10
        agendaButtonFrame.origin.x = EasyLocalization.getLanguage() == .en ?  x1 : x2
        agendaButton.frame = agendaButtonFrame
        agendaButton.backgroundColor = .clear
        agendaButton.tag = 220
        view.addSubview(agendaButton)
        
        agendaButton.addTarget(self, action: #selector(AgendaButtonAction(sender:)), for: .touchUpInside)
        view.layoutIfNeeded()
    }
    
    @objc private func AgendaButtonAction(sender: UIButton) {
        print("Enter to Agenda")
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "AgendaViewController") as! AgendaViewController
        controller.modalPresentationStyle = .custom
        UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
        //        let transition = CATransition()
        //        transition.duration = 0.2
        //        transition.type = kCATransitionFade
        //        self.view.window!.layer.add(transition, forKey: kCATransition)
        //        selectedIndex = 2
        //        menuButton.backgroundColor = selectedIndex != 2 ? .gray : UIColor(hex:"20bec8")
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}

extension UIDevice {
    static var isIphoneX: Bool {
        var modelIdentifier = ""
        if isSimulator {
            modelIdentifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] ?? ""
        } else {
            var size = 0
            sysctlbyname("hw.machine", nil, &size, nil, 0)
            var machine = [CChar](repeating: 0, count: size)
            sysctlbyname("hw.machine", &machine, &size, nil, 0)
            modelIdentifier = String(cString: machine)
        }
        
        return modelIdentifier == "iPhone10,3" || modelIdentifier == "iPhone10,6"
    }
    
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0
    }
}

