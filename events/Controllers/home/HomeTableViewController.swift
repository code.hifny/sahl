//
//  HomeTableViewController.swift
//  events
//
//  Created by abdelrahman on 2/12/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

class HomeTableViewController: UITableViewController , UISearchBarDelegate{

    var stretchyHeader: GSKStretchyHeaderView!
    var event:Event?
    @objc var refresh: UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        title = "Home".locale
        let headerSize = CGSize(width: self.tableView.frame.size.width,
                                height: 2) // 200 will be the default height
        self.stretchyHeader = GSKStretchyHeaderView(frame: CGRect(x: 0, y: 0,
                                                                          width: headerSize.width,
                                                                          height: headerSize.height))
        stretchyHeader.backgroundColor =  UIColor.fromGradientWithDirection(.leftToRight, frame: self.stretchyHeader.frame, colors:[ Color.blueForGradiantColor,Color.greenForGradiantColor])
//        self.stretchyHeader.delegate = self // this is completely optional
        self.tableView.addSubview(self.stretchyHeader)
        

        refresh = UIRefreshControl()
        self.refresh.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
        refresh.tintColor = .white
        tableView.addSubview(refresh)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    @objc func refreshData() {
        HomeViewController.instance.loadData { (status , event) in
            if status {
                self.event = event?[0]
            }
            self.setup()
        }
    }
    
    func setup() {
        refresh.endRefreshing()
        tableView.reloadData()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return event != nil ? 2 : 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return section == 0 ? 1 : event?.news.count ?? 0
    }
//122
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "headHome", for: indexPath) as! HomeHeadTableViewCell
            // Configure the cell...
            cell.config(event!)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsHome", for: indexPath) as! NewsTableViewCell
            
            // Configure the cell...
            cell.config(event!.news[indexPath.row])
            return cell
        }

    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "NewsDetailsTableViewController") as! NewsDetailsTableViewController
            viewController.newsDetails = event!.news[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    // PeopleGoingTableViewController
    @IBAction func gotoGoingUser(_ sender: UIButton) {
        if UserUtil.loadUser()?.api_token == nil{
            self.DangerAlert(message: "Pleace Login".locale)
        }else{
            let viewController = UIStoryboard(name: "People", bundle: nil).instantiateViewController(withIdentifier: "MainTableViewController") as! MainTableViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        
    }
    
    @IBAction func gotoBuyTicket(_ sender: Any) {
        if UserUtil.loadUser()?.api_token == nil
        {
            self.DangerAlert(message: "Pleace Login".locale)
        }
        else
        {
            if UserUtil.loadUser()?.ticket_info?.addressEn == nil
            {
                let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "BuyTicketTableViewController") as! BuyTicketTableViewController
                self.navigationController?.pushViewController(viewController, animated: true)
                
            }
            else
            {
                let viewController = UIStoryboard.init(name: "BuyTicket", bundle: nil).instantiateViewController(withIdentifier: "ShowTicketInfoTableViewController") as! ShowTicketInfoTableViewController
                let nav = EventsNavigationController(rootViewController: viewController)
                self.present(nav, animated: true, completion: nil)
            }
        }
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
