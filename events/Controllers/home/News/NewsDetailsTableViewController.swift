//
//  NewsDetailsTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/21/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class NewsDetailsTableViewController: UITableViewController {
    
    // MARK:- Outlets
    
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDescribtion: UITextView!
    // MARK:- Variable
    var newsDetails: News?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        clearNav()
        addData()
        //        tableViewConfig()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navConfigBlak()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navConfig()
    }
    
    //    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return UITableViewAutomaticDimension
    //    }
    
    func addData(){
        let image = "\(Config.imageUrl + (newsDetails?.image)!)"
        imageNews.af_setImage(withURL: URL(string: image)!)
        newsTitle.text = newsDetails?.title
        newsDescribtion.text = newsDetails?.descrip
    }
    //    func tableViewConfig()  {
    //        tableView.rowHeight = UITableViewAutomaticDimension
    //        tableView.estimatedRowHeight = 200
    //    }
    //
    
}

