//
//  GuideViewController.swift
//  Sahl
//
//  Created by code on 11/8/18.
//  Copyright © 2018 MR code. All rights reserved.
//

import UIKit

class GuideViewController: UIViewController {
    // MARK :- outlet
    @IBOutlet var GuidePageController: UIPageControl!
    @IBOutlet weak var GuideCollectionView: UICollectionView!
    var Stepimages = ["findplace","offer","offer"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
    }
    // UIPageControl COUNT
    @IBAction func page_action(_ sender: UIPageControl) {
        GuidePageController.currentPage = Stepimages.count
    }
    @IBAction func GoToSelectSession(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Auth", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SessionTypeViewController") as! SessionTypeViewController
        self.present(nextViewController, animated:true, completion:nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.setup()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup()  {
        RightNavBarItems(skipBtn())
        UserUtil.removeUser()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
extension GuideViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Stepimages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let Step = GuideCollectionView.dequeueReusableCell(withReuseIdentifier: "Step", for: indexPath)as? StepCollectionViewCell

        Step?.StepText.localizeKey = "GuideStep\(indexPath[1])"
        Step?.StepDescription.localizeKey = "GuideDescStep\(indexPath[1])"
        Step?.StepImage.image = UIImage(named: Stepimages[indexPath[1]])
        Step?.SkipBtn.isHidden = String(indexPath[1])=="2" ? false:true
        return Step!
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        GuidePageController.currentPage = Int(pageNumber)
    }
    
}
