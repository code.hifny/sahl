//
//  EventsNavigationController.swift
//  events
//
//  Created by abdelrahman on 2/8/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class EventsNavigationController:UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar.isTranslucent = false
        self.navigationBar.barTintColor = Color.grayColor
        self.navigationBar.tintColor = .white//Color.greenColor
        // Background and seperator
//        let image = UIImage.fromGradientWithDirection(.rightToLeft,frame: self.navigationBar.frame)
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        if #available(iOS 11.0, *) {
            self.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
        }
//        let paragraph = NSMutableParagraphStyle()
//        paragraph.alignment = .right
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
        
//        self.navigationBar.largeTitleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:27)!]
        self.view.backgroundColor = Color.grayColor
        self.navigationBar.backgroundColor = Color.grayColor
        let backImage = UIImage(named: "back")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-2000, 0), for: .default)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

