//
//  ActivationViewController.swift
//  Sahl
//
//  Created by code on 11/25/18.
//  Copyright © 2018 MR code. All rights reserved.
//

import UIKit

class ActivationViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()

        // Do any additional setup after loading the view.
    }
    func setup()  {
        navGray()
        LeftNavBarItems(BackBtn())
        //        RightNavBarItems(BackBtn())
        UserUtil.removeUser()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.faceBookButton.isUserInteractionEnabled = true
        //        self.googleButton.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            self.setup()
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
