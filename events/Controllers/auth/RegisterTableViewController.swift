//
//  RegisterTableViewController.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Google
import GoogleSignIn
import FacebookLogin
import FBSDKLoginKit


class RegisterTableViewController: UITableViewController , GIDSignInDelegate  {
    
    @IBOutlet weak var fullName: EventsTextField!
    @IBOutlet weak var email: EventsTextField!
    @IBOutlet weak var password: EventsTextField!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    
    // MARK: - Variables
    var dict: [String: AnyObject]!

    override func viewDidLoad() {
        super.viewDidLoad()
        Google()
        UserUtil.removeUser()
        navWhite()
        RightNavBarItems(skipBtn())
//        self.tableView.backgroundView = UIView()

//        password.setIcon(password.icon ?? UIImage() ,CGRect(x:13,y:10,width:16,height:20))

////        view.gradiantView()
//        self.tableView.backgroundView?.gradient()
//        [Color.greenColor.cgColor, Color.blueColor.cgColor]
//        appendData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GIDSignIn.sharedInstance().delegate = self

        self.googleButton.isUserInteractionEnabled = true
        self.facebookButton.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            self.fullName.setIcon(self.fullName.rightIcon)
            self.password.setIcon(self.password.rightIcon)
            self.email.setIcon(self.email.icon ?? UIImage() ,CGRect(x:10,y:10,width:22,height:18))
        }

    }

  
    func appendData() {
        fullName.text = "testfirst"
        email.text = "test@Events.com"
        password.text = "Aa123123"
    }
    
    @IBAction func loginAction(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func loginWithFacebook(_ sender: UIButton) {
        self.view.isUserInteractionEnabled = false
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions:  ["public_profile", "email"], from: self) { (result, error) -> Void in
            
            if error != nil {
                NSLog("Process error")
                self.view.isUserInteractionEnabled = true
                self.googleButton.isUserInteractionEnabled = true
                self.facebookButton.isUserInteractionEnabled = true
            }
            else if (result?.isCancelled)! {
                NSLog("Cancelled")
                self.view.isUserInteractionEnabled = true
                self.googleButton.isUserInteractionEnabled = true
                self.facebookButton.isUserInteractionEnabled = true
            }
            else {
                NSLog("Logged in")
                if (result?.grantedPermissions.contains("email"))!{
                    self.getFBUserData()
                    self.view.isUserInteractionEnabled = true
                    self.googleButton.isUserInteractionEnabled = true
                    self.facebookButton.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    
    @IBAction func loginWithGoogle(_ sender: UIButton) {
//        self.googleButton.isUserInteractionEnabled = false
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    //MARK:- register request action
    
    @IBAction func registerAction(_ sender: Any) {
        
        //TODO:- register validation
        if validation(){
            self.dismissKeyboard()
            StartLoading()
            print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
            print("signup".route)

            let param:[String: Any] = [
                "username":fullName.text!,
                "email":email.text!,
                "password":password.text!,
                "mobile_token" : UserDefaults.standard.string(forKey: "mobile_token") ?? "",
                "os": "ios"
            ]
            Requests.instance.request(
                link: "signup".route,
                method: .post,
                parameters: param,
                completion: { (response) in
                    if response.haveError {
                        self.DangerAlert(message: response.error )
                    }else{
                        self.view.endEditing(true)
                        if response.data!["status"].bool == true {

                            NotificationsUtil.removeAll()
                            print(response)
                            self.dismissKeyboard()
                            let user = User(response.data!["data"])
                            UserUtil.saveUser(user)
                            print("success")
                            self.performSegue(withIdentifier: "ActiveViewController", sender: nil)
                        }else{
                            self.validationShowMessage(response.data!, "username")
                            self.validationShowMessage(response.data!, "email")
                            self.validationShowMessage(response.data!, "password")
                        }
                    }
                    self.StopLoading()
            })
        }
    }
    
    //MARK:- register validation
    func validation() -> Bool {
        
        var fields:[ValidationData] = []
        
        fields.append(
            ValidationData(
                value:fullName.text! ,
                name : "Full Name".locale ,
                type:"required|notOnlyNumber|notOnlyNumberOrSpacialChr|min:3"))
        fields.append(
            ValidationData(
                value:email.text! ,
                name : "Email".locale ,
                type:"required|email"))
        fields.append(
            ValidationData(
                value:password.text! ,
                name : "Password".locale ,
                type:"required|min:6|password"))
        
        return  Validation.instance.SetValidation(ValidationData: fields)
    }
    
    
}

extension RegisterTableViewController{
    // MARK:- Signin With Google
    func Google(){
        var error: NSError?
        GGLContext.sharedInstance().configureWithError(&error)
        if error != nil{
            return
        }
//        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error)
            return
        }else{
            print(user.userID)
            print( "User Name Google" , user.profile.name)
            sendRequestSocial(socialID: user.userID, email: user.profile.email, userName: user.profile.name , type: 2)
            print( "User Image" , user.profile.imageURL(withDimension: 400))
        }
        
    }
    
    // MARK:- Send Request Google
    func sendRequestSocial(socialID: String , email: String , userName: String , type: Int){
        StartLoading()
        print("Name of Google" , userName)
        let param:[String: Any] = ["email":email,
                                   "username": userName , "social_id": socialID , "social_type": type , "mobile_token" : UserDefaults.standard.string(forKey: "mobile_token") ?? "" , "os": "ios"]
        self.googleButton.isUserInteractionEnabled = false
        self.facebookButton.isUserInteractionEnabled = false
        Requests.instance.request(link: "socialMedia".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
                    print(response)
                    NotificationsUtil.removeAll()
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "ObjectivesViewController") as! ObjectivesViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
//                        self.goToHome()
//                    }
                }else{
                    self.googleButton.isUserInteractionEnabled = true
                    self.facebookButton.isUserInteractionEnabled = true
                    self.validationShowMessage(response.data!, "email")
                    self.validationShowMessage(response.data!, "message")
                    
                }
            }
            self.StopLoading()
        }
    }
    
    
    // MARK: - Facebook
    func getFBUserData(){
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields":
            "email, name, id"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as? [String: AnyObject]
                    print("Facebook" , self.dict)
                    self.sendRequestSocial(socialID: self.dict["id"] as! String, email: self.dict["email"] as! String, userName: self.dict["name"] as! String , type: 1)
                }
            })
    }
    
}
