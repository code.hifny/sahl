//
//  ObjectivesViewController.swift
//  events
//
//  Created by Intcore on 3/25/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class ObjectivesViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{

    var objectivessArray: [Objectives] = []
    var selectedCell: Bool = false
    var selectedIndex: Int?
    var checkedArr:[Int] = []

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        navConfig()
        navigationItem.hidesBackButton = true
        exitButtonAddNav()
        tableView.delegate = self
        tableView.dataSource = self
        title = "Chooce Event Objectives".locale
        tableView.allowsMultipleSelection = true

        loadData()
    }
    
    // MARK: - Table view data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return objectivessArray.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ObjectivesTableViewCell
        cell.titleLabel.text = objectivessArray[indexPath.row].title_en
        if objectivessArray[indexPath.row].isSelected {
           cell.config(true)
        }else{
            cell.config(false)
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         objectivessArray[indexPath.row].isSelected = !objectivessArray[indexPath.row].isSelected
//        if checkedArr.contains(indexPath.row) {
//            checkedArr = checkedArr.filter { $0 != indexPath.row}
//        }else{
//            checkedArr.append(indexPath.row)
//        }
        tableView.reloadData()
    }
    
    func getSelectedObjectivesIds() -> [Int] {
        var ids:[Int] = []
        objectivessArray.forEach { (obj) in
            if obj.isSelected {
                ids.append(obj.id!)
            }
        }
        return ids
    }
    
    func loadData(_ isFirst:Bool = true) {
        if isFirst{
            self.view.startLoading()
        }
        Requests.instance.request(link: "Objectives".route, method: .get,parameters: [:]) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let objectives = response.data!["data"].map{Objectives($0.1)}
                    self.objectivessArray = objectives
                    self.view.stopLoading()
                    self.tableView.reloadData()
                    
                }
            }
        }
        
    }
    
    @IBAction func sendRequest(_ sender: Any) {
        
        let ids = getSelectedObjectivesIds()

        StartLoading()
        if ids.count == 0 {
            self.DangerAlert(message: "Please Fill in your Objectives".locale )
            self.StopLoading()
        }else{
            print(UserUtil.loadUser()?.api_token ?? "")
            var objas = "["
            var num = 0
            ids.forEach({ (id) in
                if num != 0 {
                    objas += ","
                }
                objas += "\(id)"
                num += 1
            })
             objas += "]"
            let param:[String: Any] = ["api_token":UserUtil.loadUser()?.api_token ?? "",
                                       "objectives":objas]
            print(param)
            Requests.instance.request(link: "updateObjectives".route, method: .post, parameters: param) { (response) in
                print(response.error)
                if response.haveError{
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["status"].bool == true {
                        print(response)
                        let user = User(response.data!["data"])
                        UserUtil.saveUser(user)
                        self.goToHome()
                    }
                }
                self.StopLoading()
            }
        }
        
       
    }
    
    func exitButtonAddNav(){
//        self.navigationController?.navigationBar.tintColor = UIColor.black
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(BackToLogin))
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: true)
    }
    @objc func BackToLogin(){
        self.goToLogin()
    }
    

}
