//
//  regionViewController.swift
//  Sahl
//
//  Created by code on 11/25/18.
//  Copyright © 2018 MR code. All rights reserved.
//

import UIKit

class regionViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var country: UIPickerView!
    @IBOutlet weak var city: UIPickerView!
    @IBOutlet weak var region: UIPickerView!
    var countryData: [String] = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        countriesData()
        // Connect data:
        self.country.delegate = self as UIPickerViewDelegate
        self.country.dataSource = self as UIPickerViewDataSource
        countryData = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6"]

        // Do any additional setup after loading the view.
    }
    func countriesData() {
        StartLoading()
        UserUtil.removeUser()
        
        //        print( "mobile_token" , UserDefaults.standard.string(forKey: "api_key") ?? "")
        
        Requests.instance.request(link: "countries".route, method: .post) { (response) in
            print(response)
            print(response.data!["countries"])

            //            return response.data!['data']
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
//                    countryData=response.data!["countries"]
//                    self.view.endEditing(true)
//                    let user = User(response.data!["countries"])
//                    UserUtil.saveUser(user)
                    //                        if UserUtil.loadUser()?. {
//                    self.performSegue(withIdentifier: "ActivationViewController", sender: nil)
                    //                    }else{
                    //                        if UserUtil.loadUser()?.objectives?.count == 0 {
//                    let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "ActivationViewController") as! ActivationViewController
//                    self.navigationController?.pushViewController(viewController, animated: true)
                    //                        }else{
                    //                            self.goToHome()
                    //                        }
                    self.view.endEditing(true)
                }else{
                    self.view.endEditing(true)
                }
            }
            self.StopLoading()
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfComponents(in counry: UIPickerView) -> Int {
    return 1
    }
    
    // The number of rows of data
    func pickerView(_ counry: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return countryData.count
    }
    
    // The data to return fopr the row and component (column) that's being passed in
    func pickerView(_ counry: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return countryData[row]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
