//
//  ForgetPasswordViewController.swift
//  Events
//
//  Created by Macbook Pro on 2/1/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var emailTextField: EventsTextField!
    
    
    static var instance: ForgetPasswordViewController {
        return ForgetPasswordViewController()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        exitButtonAddNav()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.emailTextField.setIcon(self.emailTextField.icon ?? UIImage() ,CGRect(x:10,y:10,width:22,height:18))
        }
    }
    
    // MARK:- Actions

    @IBAction func sendCode(_ sender: Any) {
        if validation(){
            self.dismissKeyboard()
            sendRequest(closure: { (done,code ,str  ) in
                if done{
                    let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "ActiveViewController") as! ActiveViewController
                    viewController.activeType = .reset_password
                    viewController.tempCode = code
                    viewController.emailActivation = self.emailTextField.text!
                    self.navigationController?.pushViewController(viewController, animated: true)
                }else{
                    self.DangerAlert(message: str)
                }
            })
        }
    }
    
    // MARK:- Send Request
    func sendRequest(closure: @escaping(Bool , String , String) -> Void){
        StartLoading()
        let param:[String: Any] = ["email" : emailTextField.text!]
        
        Requests.instance.request(link: "Forget".route, method: .post, parameters: param) { (response) in
            print(response)
//            self.view.endEditing(true)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
                    print(response)
                    let code = response.data!["data"]["reset_password_code"].stringValue
                    closure(true , code , "" )
                }else{
                    self.validationShowMessage(response.data!, "email")
                    self.validationShowMessage(response.data!, "message")

//                    print(response.data!)
//                    let errorMesage = response.data!["error"]
//                    print(errorMesage)
//                    let message = errorMesage["message"].stringValue
//                    print("message" , message)
//                    closure(false , "" ,message)
                }
            }
            self.StopLoading()
        }
    }
    
    //MARK:- Reset validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:emailTextField.text! ,name : "Email" , type:"required|email"))
        return Validation.instance.SetValidation(ValidationData: fields)
    }
    
    
    // MARK: - Extra Function
    func setup()  {
        emailTextField.setIcon(emailTextField.icon ?? UIImage() ,CGRect(x:10,y:10,width:22,height:18))
        navWhite()
    }
    func exitButtonAddNav(){
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(BackToLogin))
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: true)
    }
    @objc func BackToLogin(){
        self.dismiss(animated: true, completion: nil)
    }
    

}
