//
//  LoginViewController.swift
//  Sahl
//
//  Created by code on 11/14/18.
//  Copyright © 2018 MR code. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var phoneTextField: UnderlinedTextField!
    
    
    // MARK: - Variables
    var dict: [String: AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.faceBookButton.isUserInteractionEnabled = true
        //        self.googleButton.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            self.setup()
        }
        
    }
    
    
    // MARK: - Table view data source
    
    func setup()  {
        navGray()
        LeftNavBarItems(BackBtn())
//        RightNavBarItems(BackBtn())
        UserUtil.removeUser()
    }
    
    
    // MARK: - Actions
    @IBAction func loginAction(_ sender: Any) {
        
        if validation(){
            self.dismissKeyboard()
            sendRequest()
        }
    }
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        UserUtil.removeUser()

//        print( "mobile_token" , UserDefaults.standard.string(forKey: "api_key") ?? "")
        
        let param:[String: Any] = ["phone":phoneTextField.text!,
                                   "device_id" : "3345345545343312345209"]
print("login".route)
print(param)
        Requests.instance.request(link: "login".route, method: .post, parameters: param) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
//                    if response.status.bool == true {
                    self.view.endEditing(true)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
//                        if UserUtil.loadUser()?. {
                    self.performSegue(withIdentifier: "ActivationViewController", sender: nil)
                    //                    }else{
                    //                        if UserUtil.loadUser()?.objectives?.count == 0 {
                    let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "ActivationViewController") as! ActivationViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
//                        }else{
//                            self.goToHome()
//                        }
                    self.view.endEditing(true)
                    self.validationShowMessage(response.data!, "phone_error")
                    self.validationShowMessage(response.data!, "message")
                }else{
                    self.view.endEditing(true)
                    self.validationShowMessage(response.data!, "phone_error")
                    print(response.data!["phone_error"])
                }
            }
            self.StopLoading()
        }
    }
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:phoneTextField.text! ,name : "Phone" , type:"required|min:10|max:10"))
        //|password|min:6
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
