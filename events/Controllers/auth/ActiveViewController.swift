//
//  ActiveViewController.swift
//  Events
//
//  Created by abdelrahman on 1/31/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Material
import IQKeyboardManagerSwift

enum activeType {
    case register
    case update_phone
    case reset_password
}
class ActiveViewController: UIViewController ,PinTexFieldDelegate {
    
    @IBOutlet weak var Code1: EventsActiveTextField!
    @IBOutlet weak var Code2: EventsActiveTextField!
    @IBOutlet weak var Code3: EventsActiveTextField!
    @IBOutlet weak var Code4: EventsActiveTextField!
    @IBOutlet weak var Resend: UIButton!
    @IBOutlet weak var Description: UILabel!

    
    var pageType: String = ""
    var tempCode: String = ""
    var count = 30
    var phoneTempCode = 0
    var phone = ""
    var activeType: activeType = .register
    var emailActivation: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if #available(iOS 11.0, *) {
            Setup()
        } else {
            // Fallback on earlier versions
        }
        print("tempCode" , tempCode)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navWhite()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enable = true
//        navConfig()
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
    }
    
    // MARK: - Setup
    @available(iOS 11.0, *)
    @available(iOS 11.0, *)
    func Setup()  {
        IQKeyboardManager.shared.enable = false

        configFields(Code1,Code2,Code3,Code4)
//        navWhite()
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationController?.navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        navigationController?.navigationBar.tintColor = .black
        
        if activeType == .reset_password
        {
            Description.text = "Activiation Description".locale
            title = "Forget Password"
        }
        else if activeType == .update_phone
        {
            Description.text = "Activiation Description".locale
            title = "Phone Activation".locale
        }
        else
        {
            Description.text = "Activiation Description".locale
            title = "Activation".locale
        }

        
        if Config.locale == .en {
            Code1.isEnabled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.Code1.becomeFirstResponder()
            })
            Code4.tag = 500
        }else{
            Code4.isEnabled = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.Code4.becomeFirstResponder()
            })
            Code1.tag = 500
        }
 
        Resend.isEnabled = false
        Resend.setTitleColor(Color.grayColor, for: .normal)
        Resend.layer.borderWidth = 2
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
     
    }
    // MARK: - config Fields
    func configFields(_ fields : EventsActiveTextField...) {
        fields.forEach { (field) in
            field.font = UIFont.systemFont(ofSize: 35)
            field.delegate = self
            field.isEnabled = false
        }
    }
    
    func configFieldsColor(_ fields : EventsActiveTextField... , color:UIColor = .red) {
        fields.forEach { (field) in
            field.dividerColor = color
            field.dividerNormalColor = color
        }
    }
    
    // MARK: - Counter
    @objc func updateCounter() {
        if(count > 0) {
            print(count - 1)
            Resend.setTitle("\("Resend".locale) (\((count - 1)))", for: UIControlState.normal)
            Resend.setTitleColor(Color.grayColor, for: .normal)
            Resend.layer.borderColor = Color.grayColor.cgColor
            count = count - 1
        }else{
        }
        
        if(count == 0) {
            Resend.setTitle("\("Resend".locale)", for: UIControlState.normal)
            Resend.isEnabled = true
            Resend.setTitleColor(Color.greenColor, for: .normal)
            Resend.layer.borderColor = Color.greenColor.cgColor
        }
    }
    
    // MARK: - Text delegation
    func checkMaxLength(textField: UITextField!,nextField :UITextField? ,PreviousField :UITextField? , maxLength: Int) {
        if (textField.text!.count > maxLength) {
            textField.deleteBackward()
        }else{
            if (textField.text!.count == maxLength){
                if nextField != nil {
                    PreviousField?.isEnabled = false
                    textField.isEnabled = false
                    nextField?.isEnabled = true
                    nextField?.becomeFirstResponder()
                }
            }else{
                if textField.tag != 500 {
                    if PreviousField != nil {
                        PreviousField?.isEnabled = true
                        nextField?.isEnabled = false
                        textField.isEnabled = false
                        PreviousField?.becomeFirstResponder()
                    }
                }
            }
        }
    }
    
    func didPressBackspace(textField: EventsActiveTextField) {
        
        if Config.locale == .en {
            if textField == Code2 {
                textField.isEnabled = false
                becomeFirstResponder(Code1)
            }else if textField == Code3 {
                textField.isEnabled = false
                becomeFirstResponder(Code2)
            }else if textField == Code4 {
                textField.isEnabled = false
                becomeFirstResponder(Code3)
            }
        }else{
            if textField == Code3 {
                textField.isEnabled = false
                becomeFirstResponder(Code4)
            }else if textField == Code2 {
                textField.isEnabled = false
                becomeFirstResponder(Code3)
            }else if textField == Code1 {
                textField.isEnabled = false
                becomeFirstResponder(Code2)
            }
        }
        
    }
    
    func becomeFirstResponder(_ code :EventsActiveTextField) {
        code.isEnabled = true
        code.text = ""
        code.becomeFirstResponder()
    }
    

    // MARK: - Actions
    @IBAction func Code1(_ sender: Any) {
        if Config.locale != .en {
            checkCode()
        }
        checkMaxLength(textField: Code1!, nextField: Config.locale == .en ? Code2 : nil, PreviousField: Config.locale == .en ? nil : Code2, maxLength: 1)
    }
    
    @IBAction func Code2(_ sender: Any) {
        checkMaxLength(textField: Code2!, nextField:Config.locale == .en ? Code3 : Code1, PreviousField: Config.locale == .en ? Code1 : Code3, maxLength: 1)
    }
    
    @IBAction func Code3(_ sender: Any) {
        checkMaxLength(textField: Code3!, nextField:Config.locale == .en ? Code4 : Code2, PreviousField:  Config.locale == .en ? Code2 : Code4 , maxLength: 1)
    }
    
    @IBAction func Code4(_ sender: Any) {
        if Config.locale == .en {
            checkCode()
        }
        checkMaxLength(textField: Code4!, nextField: Config.locale == .en ? nil : Code3, PreviousField: Config.locale == .en ? Code3 : nil , maxLength:  1)
    }

    func sendRequest() 
    {
        if activeType == .register || activeType == .update_phone{
            Request()
        }else if activeType == .reset_password{
            let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "SendNewPasswordViewController") as! SendNewPasswordViewController
            viewController.tempCode = tempCode
            viewController.email = emailActivation
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func updatePhone() {
        let param:[String:Any] = ["api_token":UserUtil.loadUser()?.api_token ?? "" , "phone":UserUtil.loadUser()?.temp_phone ?? "" ,"reset_phone_code":UserUtil.loadUser()?.reset_phone_code ?? 0 ]
        Requests.instance.request(link:"updatePhone".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
                    print(response)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    self.SuccessAlert(message: "تم تحديث رقم الهاتف بنجاح")
                   self.dismiss(animated: true, completion: nil)
                    
                }
            }
     
        }
    }
    
    @IBAction func Resend(_ sender: Any) {
//        if timeLabel.text == "00:00" {
            self.count = 30
            self.updateCounter()
            Resend.isEnabled = false
            Resend.setTitleColor(Color.grayColor, for: .normal)
            let token = UserUtil.loadUser()?.api_token ?? ""
            print(emailActivation , " " , activeType )
        var param:[String:Any] = [:]
                
        if activeType == .reset_password  {
          param = ["email" : emailActivation]
        }
        else if  activeType == .update_phone{
            param = [
                "api_token": UserUtil.loadUser()?.api_token ?? "",
                "phone" : phone,
                ]
        }
        else {
              param = ["api_token":token]
            }
        
            let route =  activeType == .update_phone ? "resendPhone".route : activeType == .reset_password ?  "Forget".route :  "resend".route
            Requests.instance.request(link:route, method: .post, parameters: param) { (response) in
                print(response)
                if response.haveError{
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["status"].bool == true {
                        print(response)
                        let code = response.data!["data"]["reset_password_code"].stringValue
                        self.tempCode = code
                        self.SuccessAlert(message: response.data?["data"]["message"].string ?? "Send Successfully".locale)
                        
                    }
                }
            }
//        }
    }
    
    func checkCode() {
        print("Type" , activeType)
        let code = activeType == .update_phone ? Int(tempCode)  : activeType == .reset_password ? Int(tempCode) : UserUtil.loadUser()?.activation ?? 0
        print("code===>" , code ?? 0)

        if UserUtil.loadUser()?.activation != nil {
            let PassToCheck = Config.locale == .en ? "\(Code1.text!)\(Code2.text!)\(Code3.text!)\(Code4.text!)" : "\(Code4.text!)\(Code3.text!)\(Code2.text!)\(Code1.text!)"
            print(tempCode , " " , PassToCheck )
            if  "\(code  ?? 0)" == PassToCheck {
                configFieldsColor(Code1,Code2,Code3,Code4 , color : UIColor(hex: "14cc9d"))
                view.endEditing(false)
                print("\(code ?? 0)")
                sendRequest()
                Resend.isUserInteractionEnabled = false
            }else{
                configFieldsColor(Code1,Code2,Code3,Code4)
            }
        }else{
            let PassToCheck =  "\(Code1.text!)\(Code2.text!)\(Code3.text!)\(Code4.text!)"
            print(tempCode , " " , PassToCheck )
            if  "\(code  ?? 0)" == PassToCheck {
                configFieldsColor(Code1,Code2,Code3,Code4 , color : Color.greenColor)
                view.endEditing(false)
                print("\(code ?? 0)")
                sendRequest()
//                successBtn.setImage(#imageLiteral(resourceName: "log_in_true_icon"), for: .normal)
//                successBtn.isEnabled = true
                Resend.isUserInteractionEnabled = false
            }else{
                configFieldsColor(Code1,Code2,Code3,Code4)
            }
            
        }
    }
    func Request()  {
            let code = activeType == .update_phone ? Int(tempCode) : UserUtil.loadUser()?.activation ?? 0
        
        let link = activeType == .update_phone ? "updatePhone".route : "activate".route
        //activeType == .update_phone ? Routes.get("UpdatePhone") : Routes.get("ActiveUser")
            let token = UserUtil.loadUser()?.api_token ?? ""
        var parameters:[String: Any] = [:]
        parameters["api_token"] = token

            if activeType == .update_phone {
                parameters["phone"] = phone
                parameters["reset_phone_code"] = "\( code ?? 0)"
            }else{
                parameters["code"] = "\(code ?? 0)"
            }
        print(parameters)

            Requests.instance.request(link: link, method: .post,parameters: parameters) { (response) in
                print(response)
                
                
                if response.haveError {
                    self.DangerAlert(message: response.error )
                }else{
                    if response.data!["status"].bool == true {
                        let user = User(response.data!["data"])
                        UserUtil.saveUser(user)
                        if self.activeType == .update_phone {
                            self.SuccessAlert(message: "Update successfully".locale)
//                            for vc in (self.navigationController?.viewControllers ?? []) {
//                                if vc is SettingsMenuViewController {
                            self.navigationController?.popViewController(animated: true)
                            self.navigationController?.popViewController(animated: true)//popToViewController(vc, animated: true)
//                                }
//                            }
                        }else if self.activeType == .register{
                            let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "ObjectivesViewController") as! ObjectivesViewController
                            self.navigationController?.pushViewController(viewController, animated: true)
//                            self.goToHome()
                        }
                    }else{
                        self.validationShowMessage(response.data!, "phone")
                        self.DangerAlert(message: response.data?["data"]["message"].string ?? "Connection Error".locale)
                    }
                }
            }
    }
    
}

protocol PinTexFieldDelegate : UITextFieldDelegate {
    func didPressBackspace(textField : EventsActiveTextField)
}


