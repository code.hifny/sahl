import UIKit

class SendNewPasswordViewController: UIViewController {
    // MAEK:- Outlets
    
    @IBOutlet weak var passwordTextField: EventsTextField!
    @IBOutlet weak var confirmPassordTextField: EventsTextField!
    
    // MAEK:- Variables
    var tempCode: String!
    var email: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        passwordTextField.setIcon(passwordTextField.icon ?? UIImage() ,CGRect(x:10,y:10,width:22,height:18))
//        confirmPassordTextField.setIcon(confirmPassordTextField.icon ?? UIImage() ,CGRect(x:10,y:10,width:22,height:18))

        exitButtonAddNav()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.passwordTextField.setIcon(self.passwordTextField.rightIcon)
            self.confirmPassordTextField.setIcon(self.confirmPassordTextField.rightIcon)
        }


    }
    
    // MARK: - Actiona
    
    @IBAction func sendRequest(_ sender: Any) {
        if validation(){
            sendRequest()
        }
    }
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        let param:[String: Any] = ["email":email ?? "",
                                   "password":passwordTextField.text! ,
                                   "reset_password_code": tempCode ?? "" ]
        
        Requests.instance.request(link: "ResetPassword".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
                    self.dismissKeyboard()
                    print(response)
                    let user = User(response.data!["data"])
                    UserUtil.saveUser(user)
                    self.goToHome()
                }
            }
            self.StopLoading()
        }
    }
    
    //MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:passwordTextField.text! ,name : "passowrd", type:"required|min:6|password"))
        fields.append(ValidationData(value:confirmPassordTextField.text! ,name : "Confirm Password" , type:"required"))
        
        if !Validation.instance.SetValidation(ValidationData: fields){
            if passwordTextField.text != confirmPassordTextField.text {
                DangerAlert(message: "Passwords not matched")
                return true
            }
            return Validation.instance.SetValidation(ValidationData: fields)
        }
        return true
    }
    
    func exitButtonAddNav(){
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "cancel"), style: .plain, target: self, action: #selector(BackToLogin))
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: true)
    }
    @objc func BackToLogin(){
        self.goToLogin()
    }
    
  

}
