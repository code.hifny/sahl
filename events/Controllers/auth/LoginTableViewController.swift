//
//  LoginTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/8/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class LoginTableViewController: UITableViewController  {
    
    // MARK: - Outlets
   
    
   
    // MARK: - Variables
    var dict: [String: AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.faceBookButton.isUserInteractionEnabled = true
//        self.googleButton.isUserInteractionEnabled = true
        DispatchQueue.main.async {
            self.setup()
        }

    }

    
    // MARK: - Table view data source

    func setup()  {
        navWhite()
//        RightNavBarItems(skipBtn())
//        UserUtil.removeUser()
    }
    
    
    // MARK: - Actions
    @IBAction func loginAction(_ sender: Any) {
        
        if validation(){
            self.dismissKeyboard()
            sendRequest()
        }
    }
    
    
    // MARK:- Send Request
    func sendRequest(){
        StartLoading()
        print( "mobile_token" , UserDefaults.standard.string(forKey: "mobile_token") ?? "")
      
        let param:[String: Any] = ["phone":phoneTextField.text!,
                                    "device_id" : UserDefaults.standard.string(forKey: "mobile_token") ?? "" , "os": "ios"]
        Requests.instance.request(link: "login".route, method: .post, parameters: param) { (response) in
            print(response)
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data!["status"].bool == true {
                    print(response)
//                    self.view.endEditing(true)
//                    let user = User(response.data!["data"])
//                    UserUtil.saveUser(user)
//                    if UserUtil.loadUser()?.activation != 1 {
//                        self.performSegue(withIdentifier: "ActiveViewController", sender: nil)
//                    }else{
//                        if UserUtil.loadUser()?.objectives?.count == 0 {
////                            let viewController = UIStoryboard.init(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "ObjectivesViewController") as! ObjectivesViewController
////                            self.navigationController?.pushViewController(viewController, animated: true)
//                        }else{
//                            self.goToHome()
//                        }
////                        NotificationsUtil.appendAllWithLogin()
//                    }
//                }else{
//                    self.view.endEditing(true)
//                    self.validationShowMessage(response.data!, "phone")
//                    self.validationShowMessage(response.data!, "message")
//
                }
            }
            self.StopLoading()
        }
    }
    
    //    MARK:- login validation
    func validation() -> Bool {
        var fields = [ValidationData]()
        fields.append(ValidationData(value:phoneTextField.text! ,name : "Phone" , type:"required"))
        //|password|min:6
        return Validation.instance.SetValidation(ValidationData: fields)
        
    }
    
}



    


