//
//  homeViewController.swift
//  Sahl
//
//  Created by code on 11/26/18.
//  Copyright © 2018 MR code. All rights reserved.
//

import UIKit
import SwipeViewController


class HomeViewController: SwipeViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let stb = UIStoryboard(name: "Home", bundle: nil)
        let latest = stb.instantiateViewController(withIdentifier:"latestViewController") as UIViewController
        let favorite = stb.instantiateViewController(withIdentifier:"favoriteViewController") as UIViewController
        let myads = stb.instantiateViewController(withIdentifier:"myadsViewController") as UIViewController
        latest.title = "latest".localized
        setViewControllerArray([latest, favorite, myads])
        setFirstViewController(1)
        setNavigationColor(Color.darkBlueColor)
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: nil)
        setNavigationWithItem(UIColor.white, leftItem: barButtonItem, rightItem: nil)
        equalSpaces = true
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
