//
//  PeopleGoingTableViewController.swift
//  events
//
//  Created by Macbook Pro on 2/18/18.
//  Copyright © 2018 Mina Gad. All rights reserved.
//

import UIKit
import SwiftyJSON
import PusherSwift

class MainTableViewController: UIViewController , UISearchBarDelegate {

    @IBOutlet weak var redView:UIView!
    
    @IBOutlet weak var noChatAvailableView: UIView!
    var pusher: Pusher!
    var counter: Int = 0
    
    @IBOutlet weak var circleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var peopleButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var bottomSpinner: UIActivityIndicatorView!
    
    var peoples = [User]()
    var rooms = [Room]()
    var pageType: String?
    // MARK: - Search
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .system)
    let searcClosehButton = UIButton(type: .system)

    lazy var refresher:UIRefreshControl = {
        let refresher = UIRefreshControl()
        refresher.tintColor = UIColor.blue
        refresher.addTarget(self, action: #selector(getPeopleDataFromNetwork), for: .valueChanged)
        refresher.addTarget(self, action: #selector(getAllRequest), for: .valueChanged)
        return refresher
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        noChatAvailableView.isHidden = true
        addButtonSearch()
        peopleButton.isEnabled = false
        title = "Meet New People".locale
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refresher
        chatButton.setTitleColor(.gray, for: .normal)
        chatButton.tintColor = UIColor.gray
//        setupUnseenCounter()
        if pageType == "appDelegete" || pageType == "alert" {
            exitButtonAddNav()
            chatNotifications()
            helper.chat_tap = "chat"
            
        }else{
            helper.chat_tap = "people"

        }
    }


    private func setupPusherServer() {
        // Only use your secret here for testing or if you're sure that there's
        // no security risk
        //        app_id = "477621"
        //        key = "45c150bdf6031cea789f"
        //        secret = "6447c09f251711405f5a"
        //        cluster = "eu"
        //        channel : inbox_user_id_{autherize_id}
        //        broadcast : new_message
        
        let options = PusherClientOptions(
            host: .cluster("eu")
        )
        
        pusher = Pusher(key: "45c150bdf6031cea789f", options: options)
        
        let channel = pusher.subscribe("inbox_user_id_\(UserUtil.loadUser()?.id ?? 0)")
        
        channel.bind(eventName: "new_message", callback: { (data: Any?) -> Void in
            
            if let dict = data as? [String: Any] {
                print(dict) // ["counter": 8, "receiver_id": 313]
                self.counter = dict["counter"] as! Int
                self.getAllRequest()
//                let data = data as? JSON
//                let counter = data!["counter"].intValue
//                self.chatButton.setTitle("Chat \(counter ?? 0)", for: .normal)
//                if let counter = dict["counter"] as? Int {
//                self.chatButton.setTitle("\(self.counter) Chats", for: .normal)
//                self.counter += 1
                self.circleLabel.text = "\(self.counter)"
            }
        })
        pusher.connect()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupPusherServer()
        helper.addNetworkObserver(on: self)
        helper.connected ? getPeopleDataFromNetwork() : loadOfflinePeople()
        // get requests from server
        hideRedView()
        helper.connected ? getAllRequest() : loadOfflineChat()
        
    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        tabBarHideConfig(isHidden: false)
//    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        tabBarHideConfig(isHidden: true)
//    }

    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        //        tabBarHideConfig(isHidden: true)
        helper.removeNetworkObserver(from: self)
        //        pusher.disconnect()
        
    }
    func hideRedView() {
        if helper.connected == false {
            redView.isHidden = true
        }
    }
    
    @objc func getAllRequest() {
        
        refresher.endRefreshing()
//        StartLoading()
        let token = UserUtil.loadUser()?.api_token ?? ""
        ChatAPI.getAllRequests(my_token: token) { [weak self](rooms, success, error) in
            if let error = error {
                self?.DangerAlert("Error", message: error)
//                self?.StopLoading()
                return
            }
            if let rooms  = rooms {
//                self?.StopLoading()
                self?.rooms = rooms
//                getRoomData()
                self?.tableView.reloadData()
            }
        }
    }
    
    func loadOfflineChat() {
        rooms = ChatStore.shared.savedRooms
        tableView.reloadData()
    }

    private func setupUnseenCounter() {
        if rooms.count > 0 {
            rooms.forEach { (room) in
                if room.seen == 0 {
                    counter += 1
                }
            }
        }
        circleLabel.text = "\(counter)"
//        chatButton.setTitle("\(counter) Chats", for: .normal)
    }
    
    
   
    
    var isLoading = false
    var current_page = 1
    var last_page = 1
    
    @objc private func getPeopleDataFromNetwork() {
        StartLoading()
        refresher.endRefreshing()
        guard !isLoading else { return }
        isLoading = true
        
        ChatAPI.getAllPeople { [weak self] (peoples, granted, error, last_page) in
            self?.isLoading = false
            if let error = error {
                self?.StopLoading()
                self?.DangerAlert("Error", message: error)
            }
            if let p = peoples
            {
                self?.StopLoading()
                self?.peoples = p
                self?.tableView.reloadData()
                self?.current_page = 1
                self?.last_page = last_page
            }
        }
    }
    
    
    private func loadOfflinePeople() {
        peoples = PeopleStore.shared.savedPeople
        tableView.reloadData()
    }
    
    fileprivate func load_more(){
        bottomSpinner.startAnimating()
        guard !isLoading else { return }
        
        guard current_page < last_page else {
            self.bottomSpinner.stopAnimating()
            return
        }
        
        isLoading = true
        ChatAPI.getAllPeople(page: current_page + 1) { (peoples, granted, error, last_page) in
            self.isLoading = false
            self.bottomSpinner.stopAnimating()

            if let error = error {
                self.DangerAlert("Error", message: error)
            }
            if let peoples = peoples {
                self.peoples.append(contentsOf: peoples)
                self.tableView.reloadData()
                self.current_page += 1
                self.last_page = last_page
            }
        }
    }

    @IBAction func peopleButtonPressed(_ sender: UIButton) {
        chatButton.isEnabled = true
        peopleButton.isEnabled = false
        chatButton.tintColor = UIColor.gray
        chatButton.setTitleColor(.gray, for: .normal)
        peopleButton.setTitleColor(Color.greenColor, for: .normal)
        helper.chat_tap = "people"
        tableView.reloadData()
//        getPeopleDataFromNetwork()
        noChatAvailableView.isHidden = true
    }
    
    @IBAction func chatsButtonPressed(_ sender: UIButton) {
        chatNotifications()
//        peopleButton.isEnabled = true
//        chatButton.isEnabled = false
//        chatButton.tintColor = Color.greenColor
//        chatButton.setTitleColor(Color.greenColor, for: .normal)
//        peopleButton.setTitleColor(.gray, for: .normal)
//        // TODO:- .........
//        helper.chat_tap = "chat"
//
//        tableView.reloadData()
////        getAllRequest()
//        if rooms.count == 0 {
//            noChatAvailableView.isHidden = false
//        }
    }
    
    
    func chatNotifications(){
        if rooms.count == 0 {
            noChatAvailableView.isHidden = false
        }
        peopleButton.isEnabled = true
        chatButton.isEnabled = false
        chatButton.tintColor = Color.greenColor
        chatButton.setTitleColor(Color.greenColor, for: .normal)
        peopleButton.setTitleColor(.gray, for: .normal)
        // TODO:- .........
        helper.chat_tap = "chat"
        
        if pageType == "appDelegete" || pageType == "alert" {
            noChatAvailableView.isHidden = true
        }
//        getAllRequest()
        tableView.reloadData()
        //        getAllRequest()
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Config.view_profile {
            if let vp = segue.destination as? ViewProfileTableViewController
            {
                vp.info = ["MyProfile": true]
                vp.userData = sender as? User
                vp.type = "Going"
                
            }
        }
        else if segue.identifier == Config.open_message {
            if let message_vc = segue.destination as? ChatViewController
            {

                message_vc.room = sender as? Room
            }
        }else if let nav = segue.destination as? EventsNavigationController {
            nav.viewControllers.forEach({ (controller) in
                if let vc =  controller as? SearchPeopleTableViewController {
                    
                    if helper.connected {
                        vc.peopleArray = []
                        vc.peopleArrayFilter = []
                    }else{
                        vc.peopleArray = peoples
                        vc.peopleArrayFilter = peoples
                    }
                    
                   
                }
            })

        }
    }
}


extension MainTableViewController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if helper.chat_tap == "people"
        {
            let selectedUser = peoples[indexPath.row]
            performSegue(withIdentifier: Config.view_profile, sender: selectedUser)
        }
        else if helper.chat_tap == "chat"
        {
            rooms[indexPath.row].seen = 1
            let selectedRoom = rooms[indexPath.row]
            performSegue(withIdentifier: Config.open_message, sender: selectedRoom)
        }
    }
    
    func exitButtonAddNav(){
        let leftAddBarButtonItemImage:UIBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "back") , style: .plain, target: self, action: #selector(BackToLogin))
        self.navigationItem.setLeftBarButton(leftAddBarButtonItemImage, animated: true)
    }
    @objc func BackToLogin(){
//      self.navigationController?.popViewController(animated: true)
        guard let widnow = UIApplication.shared.keyWindow else { return }
        if let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabBarController") as? HomeTabBarController {
            widnow.rootViewController = vc
            UIView.transition(with: widnow, duration: 0.4, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        }
    }
    
}

extension MainTableViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if helper.chat_tap == "chat"
        {
            return rooms.count
        }
        return peoples.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if helper.chat_tap == "people"
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Config.PeopleCell, for: indexPath) as? PeopleCell {
                let people = peoples[indexPath.row]
                cell.people = people
                return cell
            }
        }else if helper.chat_tap == "chat"
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: Config.ChatCell, for: indexPath) as? ChatCell {
                let room = rooms[indexPath.row]
                cell.room = room
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let people = peoples.count
        if indexPath.row == (people - 1)
        {
            load_more()
        }
    }
}

extension MainTableViewController {
    // MARK:- Search Bar
    func addButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchButton.setImage(#imageLiteral(resourceName: "search").withRenderingMode(.alwaysOriginal), for: .normal)
        searchButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searchButton.addTarget(self, action: #selector(searchController), for: .touchUpInside)
        navigationItem.rightBarButtonItem =  UIBarButtonItem(customView: searchButton)
    }
    
    @objc func searchController(){
        performSegue(withIdentifier: "showPeopleSearch", sender: self)
    }

}

