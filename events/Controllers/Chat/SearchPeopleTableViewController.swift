//
//  SearchPeopleTableViewController.swift
//  events
//
//  Created by Intcore on 3/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class SearchPeopleTableViewController: UITableViewController,UISearchBarDelegate{
    
    var peopleArray:[User] = []
    var peopleArrayFilter:[User] = []
    var pageSearch: Bool = false
    // MARK: - Search
    var searchBar = UISearchBar()
    let searchButton = UIButton(type: .system)
    let searcClosehButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.showsCancelButton = false

        if helper.connected{
            tableView.tableFooterView = UIView()
        }

        if #available(iOS 11.0, *) {
            searchController()
        }
        else
        {
            // Fallback on earlier versions
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        helper.addNetworkObserver(on: self)
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        print(  "self.pageSearch" , self.pageSearch)
        if helper.connected {
            if searchBar.text == ""{
                self.pageSearch = false
            }
            print(  "self.pageSearch" , self.pageSearch)
            if peopleArray.count == 0 {
                if pageSearch && peopleArray.count == 0 {
                    BackNotFounTableView(tableView: self.tableView, text: "No Matched Results")
                }else{
                    BackNotFounTableViewEvents(tableView: self.tableView, text: "")
                }
            }
        }else{
            if peopleArray.count == 0 {
                BackNotFounTableView(tableView: self.tableView, text: "No Matched Results")
            }else{
                BackNotFounTableViewEvents(tableView: self.tableView, text: "")
            }
        }
        
        return peopleArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PeopleCell", for: indexPath) as! PeopleCell
        
        // Configure the cell...
        cell.people = peopleArray[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "ViewProfileTableViewController") as! ViewProfileTableViewController
        viewController.userData = peopleArray[indexPath.row]
        viewController.type = "Going"
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func loadData(searchText: String, _ completion: @escaping (Bool , [User]) -> ()) {
        print("searchBar.text" , searchBar.text ?? "")
        let param:[String: Any] = ["search": searchBar.text ?? ""]
        Requests.instance.request(link: "Search".route , method: .get, parameters: param) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
                completion(false , [])
            }else{
                if response.data?["status"].bool == true {
                    print(response)
                    let people = response.data!["data"]["going"].map{User($0.1)}
                    completion(true , people )
                }else{
                    self.pageSearch = false
                }
            }
        }
    }
    
}
extension SearchPeopleTableViewController {
    // MARK:- Search Bar
    
    func addCloseButtonSearch(){
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBarStyle.minimal
        searchBar.tintColor = UIColor.white
        searchBar.placeholder = "Search"
        searcClosehButton.setImage(#imageLiteral(resourceName: "cancel").withRenderingMode(.alwaysOriginal), for: .normal)
        searcClosehButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        searcClosehButton.addTarget(self, action: #selector(closeSearch), for: .touchUpInside)
        navigationItem.leftBarButtonItem =  UIBarButtonItem(customView: searcClosehButton)
    }
    
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
      
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.startLoading()
        loadData(searchText: searchBar.text ?? "") { (done, people) in
            if done{
                self.searchBar.endEditing(false)
                self.peopleArray = people
                self.view.stopLoading()
                self.pageSearch = true
                self.tableView?.reloadData()
            }
        }
    }
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.text = ""
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
//        clearButton.addTarget(self, action: #selector(cleatSearch), for: .touchUpInside)
        let stringText = searchBar.text!
       
        print(stringText)
        peopleArray = peopleArrayFilter.filter({ $0.username?.range(of: stringText , options:.caseInsensitive) != nil})
        
        if(stringText.count < 1){
            peopleArray = peopleArrayFilter
        }
        self.tableView.reloadData()
    }
//    @objc func cleatSearch(){
//        self.pageSearch = false
//        self.peopleArray = []
//        print(self.pageSearch)
//    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.showsCancelButton = false
        let textFieldInsideSearchBarLabel = searchBar.textField.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        
        if let glassIconView = searchBar.textField.leftView as? UIImageView {
            //Magnifying glass
            glassIconView.image = glassIconView.image?.withRenderingMode(.alwaysTemplate)
            glassIconView.tintColor = .white//UIColor(hex:"20bec8")
        }
        let clearButton = searchBar.textField.value(forKey: "_clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .highlighted)
        clearButton.imageView?.tintColor = .white//UIColor(hex:"20bec8")
        
        return true
        
    }
  
    @available(iOS 11.0, *)
    @objc func searchController(){
        //        searchBar.alpha = 0
        //        searchBar.setTextFieldClearButtonColor(color: .red)
        searchBar.barTintColor = UIColor.white
        
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationItem.titleView = searchBar
        navigationItem.setLeftBarButton(nil, animated: true)
        searchBar.textField.textColor = .white
        
        self.addCloseButtonSearch()
        self.searcClosehButton.alpha = 1
        //        }, completion: { finished in
        self.searchBar.becomeFirstResponder()
        //        })
    }
    
    @objc func closeSearch(){
        DispatchQueue.main.async {
            if self.searchBar.endEditing(false) {
                self.pageSearch = false
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

