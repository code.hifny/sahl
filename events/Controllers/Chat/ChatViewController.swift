//
//  ChatViewController.swift
//  events
//
//  Created by Mina Gad on 2/22/18.
//  Copyright © 2018 Mina Gad. All rights reserved.
//

import UIKit
import SwiftyJSON
import IQKeyboardManagerSwift
import PusherSwift
import Material


class ChatViewController: UIViewController {
    
    @IBOutlet weak var messageTextView: TextView! {
        didSet {
            messageTextView.layer.cornerRadius = 15
            messageTextView.clipsToBounds = true
            messageTextView.layer.borderColor = UIColor.lightGray.cgColor
            messageTextView.layer.borderWidth = 1
        }
    }

    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var send_message_Button: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    var option = 0 // Accept 1 , reject 0
    var room: Room?
    var messages = [Message]() // get room with all messages between 2 persons.......
    var come_from: String?
    var user: User?
    var pusher: Pusher!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        helper.addNetworkObserver(on: self)
        tableView.delegate = self
        tableView.dataSource = self
        tableView?.backgroundColor = .white
        
//        collectionView?.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 60, right: 0)
//        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 76
        
        tableView.register(UINib(nibName: Config.ChatMessageCell, bundle: nil), forCellReuseIdentifier: Config.ChatMessageCell)
        loadofflineNavBar()
        loadOfflineMessages()
    }
    
    
    func checkIfRequestOrConversation() {
//        print("room" , room)

        if let room = room {
            user = room.user
            let my_id = UserUtil.loadUser()?.id ?? 0
            guard let user_id = my_id == room.user_id_1 ? room.user_id_2 : room.user_id_1 else { return }
            Config.user_id = user_id
            helper.connected ? setupNavigationBar() : loadofflineNavBar()
            if let status = room.status
            {
                if status == 2 || status == 0 {
                    holderView.isHidden = false
                }
                else
                {
                    holderView.isHidden = true
                    helper.connected ? getRoomWithAllMessagesBetweenTwoUsers() : loadOfflineMessages() // if friends get room with All messages here ......................
                }
            }
        }
        
        if  come_from == "Profile" {
            holderView.isHidden = true
        }
    }
    func loadofflineNavBar() {
 
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openProfile))
        tapGesture.numberOfTapsRequired = 1
        let userNamelabel = UILabel()
        userNamelabel.text = come_from == "Profile" ? user?.username ?? "" :  room?.user?.username ?? ""
        userNamelabel.isUserInteractionEnabled = true
        userNamelabel.sizeToFit()
        userNamelabel.font = .boldSystemFont(ofSize: 15)
        userNamelabel.textColor = .white
        userNamelabel.addGestureRecognizer(tapGesture)
        
        let joblabel = UILabel()
        joblabel.text =  come_from == "Profile" ? user?.job_title ?? "" : room?.user?.job_title ?? ""
        joblabel.sizeToFit()
        joblabel.font = .systemFont(ofSize: 14)
        joblabel.textColor = .white
        joblabel.addGestureRecognizer(tapGesture)
        
        let width = max(userNamelabel.bounds.width, joblabel.bounds.width)
        let height = userNamelabel.bounds.height + joblabel.bounds.height + 1
        let stackView = UIStackView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        stackView.addArrangedSubview(userNamelabel)
        stackView.addArrangedSubview(joblabel)
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.distribution = .fillProportionally
        stackView.spacing = 1
        navigationItem.titleView = stackView
        navigationItem.titleView?.isUserInteractionEnabled = true
        navigationItem.titleView?.addGestureRecognizer(tapGesture)
    }
    private func setupNavigationBar() {
        let user_id = Config.user_id
        let token = UserUtil.loadUser()?.api_token ?? ""
        ChatAPI.getProfileOfUserForDisplay(id: user_id, token: token) { [weak self](user, success, error) in
            if let error = error {
                self?.DangerAlert("Error", message: error)
                return
            }
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self?.openProfile))
            tapGesture.numberOfTapsRequired = 1
            let userNamelabel = UILabel()
            userNamelabel.isUserInteractionEnabled = true
            userNamelabel.text = user?.username
            userNamelabel.sizeToFit()
            userNamelabel.font = .boldSystemFont(ofSize: 15)
            userNamelabel.textColor = .white
            userNamelabel.addGestureRecognizer(tapGesture)
            let joblabel = UILabel()
            joblabel.text = user?.job_title
            joblabel.sizeToFit()
            joblabel.font = .systemFont(ofSize: 13)
            joblabel.textColor = .white
            joblabel.addGestureRecognizer(tapGesture)

            let width = max(userNamelabel.bounds.width, joblabel.bounds.width)
            let height = userNamelabel.bounds.height + joblabel.bounds.height + 1
            let stackView = UIStackView(frame: CGRect(x: 0, y: 0, width: width, height: height))
            stackView.addArrangedSubview(userNamelabel)
            stackView.addArrangedSubview(joblabel)
            stackView.axis = .vertical
            stackView.alignment = .center
            stackView.distribution = .fillProportionally
            stackView.spacing = 1
            self?.navigationItem.titleView?.frame = CGRect(x: 0, y: 0, width: width, height: height)
            self?.navigationItem.titleView = stackView
            self?.navigationItem.titleView?.isUserInteractionEnabled = true
            self?.navigationItem.titleView?.addGestureRecognizer(tapGesture)
            
        }
    }
    @objc func openProfile() {
        performSegue(withIdentifier: "fromChatViewControllerToProfileViewControllerSegue", sender: ["MyProfile": false, "id":user?.id , "user":user] )
    }
    func loadOfflineMessages() {
        if let room = room {
            if let messages = ChatStore.shared.getMessages(for: room) {
                self.messages = messages
                tableView.reloadData()
            }
        }
    }
    
    @IBAction func acceptButtonPressed(_ sender: UIButton)
    {
        if let room = room , let room_id = room.id
        {
            option = 1
            let token = UserUtil.loadUser()?.api_token ?? ""
            self.holderView.isHidden = true

            ChatAPI.AcceptOrRejectRequest(status: option, room_id: room_id, token: token) { room in
                print(room?.status);
                // TODO:- response..........
                //                if let room = room {
                self.holderView.isHidden = true
            }
        }
    }
    
    @IBAction func rejectButtonPressed(_ sender: UIButton) {
        
        if let room = room , let room_id = room.id {
            option = 0
            let token = UserUtil.loadUser()?.api_token ?? ""
            self.navigationController?.popViewController(animated: true)

            ChatAPI.AcceptOrRejectRequest(status: option, room_id: room_id, token: token) { room in
                // TODO:- response..........
                //                print(room)
                //                self.SuccessAlert(message: "rejected successfully".locale)
                //                if let room = room {
//                self.navigationController?.popViewController(animated: true)

                
                if room?.status == 0 || room?.status == 2 {
                    //                        self.holderView.isHidden = false
                }
                //                }
            }
            
            
        }
    }
    
    func getRoomWithAllMessagesBetweenTwoUsers() {
        //        self.StartLoading()
        let my_token = UserUtil.loadUser()?.api_token ?? ""
        view.startLoading()
        if let room = room {
            ChatAPI.getRoomWithMessages(room: room, user_id: Config.user_id, my_token: my_token, completion: { [weak self] (messages, parentRoom ,granted, error) in
                if let error = error {
                    self?.view.stopLoading()
                    self?.DangerAlert("Error", message: error)
                }
                //TODO:- messages here.......
                if let messages = messages {
                    self?.view.stopLoading()
                    self?.messages = messages
                    self?.tableView.reloadData()
                }
            })
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        checkIfRequestOrConversation()
        setupPusherServer()  // start pusher listenr
        sendSeenState()
    }
    
    func sendSeenStateXXX() {
        // TODO:- seen here......
        print(Config.user_id)
        let token = UserUtil.loadUser()?.api_token ?? ""
        let id = UserUtil.loadUser()?.id ?? 0
        print(token)
        ChatAPI.seenMessage(my_token: token, user_id: id) { (message, success, error) in
            if let error = error {
                print(error)
            }
            if let message = message {
                print(message)
            }
        }
    }
    //*****

    func sendSeenState() {
        // TODO:- seen here......
        let token = UserUtil.loadUser()?.api_token ?? ""
        ChatAPI.seenMessage(my_token: token, user_id: Config.user_id) { (message, success, error) in
            if let error = error {
                print(error)
            }
            if let message = message {
                print(message)
            }
        }
    }
    ///****

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        pusher.disconnect()
    }
    
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        guard let sent_message = messageTextView.text?.trimmed.encode(), !sent_message.isEmpty else { return }
        view.endEditing(true)
        let token = UserUtil.loadUser()?.api_token ?? ""
        let my_id = UserUtil.loadUser()?.id ?? 0
        if helper.connected == true {
            if let room = room {
                let sendMessage  = JSON([
                    "id" :  0,
                    "chat_room_id": room.id ?? 0,
                    "sender_id": my_id ,
                    "message" :sent_message,
                    "seen" : 1,
                    "date" :  "",
                    "sender": ""
                    ])
                let message = Message(sendMessage)
                message?.sender =  UserUtil.loadUser()
                self.messages.append(message!)
                
                self.tableView.reloadData()
                let indexPath = IndexPath(item: (self.messages.count - 1), section: 0)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                messageTextView.text = ""
                let recevier_id = my_id == room.user_id_1 ? room.user_id_2 : room.user_id_1
                let id = room.id
                ChatAPI.sendMessage(id: id!, message: sent_message, my_token: token, receiver_id: recevier_id!, completion: { (message, success, error) in
                    if let error = error {
                        self.DangerAlert("Error", message: error)
                        return
                    }
                })
            }
        }
        else
        {
            self.DangerAlert("Error", message: "No Internet connection".locale)
        }
    }
    

    
    
    private func setupPusherServer() {
        // Only use your secret here for testing or if you're sure that there's
        // no security risk
        //        app_id = "477621"
        //        key = "45c150bdf6031cea789f"
        //        secret = "6447c09f251711405f5a"
        //        cluster = "eu"
//        channel : inbox_user_id_{autherize_id}
//        broadcast : new_message
        
        let options = PusherClientOptions(
            host: .cluster("eu")
        )
        
        pusher = Pusher(key: "45c150bdf6031cea789f", options: options)
        
        let channel = pusher.subscribe("chat_\(room?.id ?? 0)")
        
        channel.bind(eventName: "message_sent", callback: { (data: Any?) -> Void in
            
            if let _ = data as? [String: AnyObject] {
                print(data) // ["counter": 8, "receiver_id": 313]
                let json = JSON(data)
                let newMessage = Message(json["message"])
                if newMessage?.sender_id != UserUtil.loadUser()?.id {
                    self.messages.append(newMessage!)
                    self.tableView.reloadData()
                    let indexPath = IndexPath(item: (self.messages.count - 1), section: 0)
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
                self.sendSeenState()
//                self.sendSeenStateForMyID()

                //                Message
//                let reciever_id = data["receiver_id"]?.int64Value
//                let token = UserUtil.loadUser()?.api_token ?? ""
//                ChatAPI.getRoomWithMessages(room: self.room!, user_id: Config.user_id, my_token: token, completion: { (messages, success, error) in
//                        self.messages = messages!
//                        self.tableView.reloadData()
//                        let indexPath = IndexPath(item: (self.messages.count - 1), section: 0)
//                        self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//                })
            }
        })
        pusher.connect()
        
    }
        
        
    private func sendSeenStateForMyID() {
        if let api_token = user?.api_token {
        let id = UserUtil.loadUser()?.id ?? 0
            ChatAPI.seenMessage(my_token: api_token, user_id: id) { (message, success, error) in
                if let error = error {
                    print(error)
                }
                if let message = message {
                    print(message)
                }
            }
        }
    }
}



extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return UITableViewAutomaticDimension
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tableView.endEditing(true)
        view.endEditing(true)
    }
}

extension ChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Config.ChatMessageCell, for: indexPath) as? ChatMessageCell {
            let message = messages[indexPath.row]
            setupCell(with: cell, message: message)
            cell.message = message
            cell.messageTextView.tag = indexPath.row
            return cell
        }
        return UITableViewCell()
    }
    
    
    private func setupCell(with cell: ChatMessageCell, message: Message) {
        let detectorPhoneNumber = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let detectorLink = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        
        let my_id = UserUtil.loadUser()?.id ?? 0
        if message.sender_id != my_id {
            cell.my_ImageView.isHidden = true
            cell.my_ImageViewWidthConstraint.constant = 0
            cell.friend_ImageViewWidthConstraint.constant = 44
            cell.friend_ImageView.isHidden = false
            cell.messageTextView.backgroundColor =  UIColor.fromGradientWithDirection(.leftToRight, frame: messageTextView.frame, colors: [ Color.blueColor,Color.greenForGradiantColor]) //Color.blueForGradiantColor
            cell.messageTextView.textColor = .white
            cell.messageTextView.textAlignment = .right
            
            cell.textLeftPaddingConstraint.priority = UILayoutPriority(rawValue: 100)
            cell.textLeftPaddingConstraint2.priority = UILayoutPriority(rawValue: 999)
            cell.textRightPaddingConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.textRightPaddingConstraint2.priority = UILayoutPriority(rawValue: 100)
            
            let matches1 = detectorPhoneNumber.matches(in: message.message!, options: [], range: NSRange(location: 0, length: message.message!.utf16.count))
            
            let matches2 = detectorLink.matches(in: message.message!, options: [], range: NSRange(location: 0, length: message.message!.utf16.count))
            
            if matches1.count > 0 {
                for match in matches1 {
                    guard let range = Range(match.range, in: message.message!) else { continue }
                    let url = message.message![range]
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(url))
                    attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, attributeString.length))

                    cell.messageTextView.attributedText = attributeString
                }
            }
            if matches2.count > 0 {
                for match in matches2 {
                    guard let range = Range(match.range, in: message.message!) else { continue }
                    let url = message.message![range]
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(url))
                    attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                    cell.messageTextView.attributedText = attributeString
                }
            }
           
            
        }
        else if message.sender_id == my_id {
            cell.my_ImageView.isHidden = false
            cell.my_ImageViewWidthConstraint.constant = 44
            cell.friend_ImageViewWidthConstraint.constant = 0
            cell.friend_ImageView.isHidden = true
            cell.messageTextView.backgroundColor = #colorLiteral(red: 0.9371728301, green: 0.9373074174, blue: 0.9371433854, alpha: 1)
            cell.messageTextView.textColor = .black
            cell.messageTextView.textAlignment = .left
            cell.textLeftPaddingConstraint.priority = UILayoutPriority(rawValue: 999)
            cell.textLeftPaddingConstraint2.priority = UILayoutPriority(rawValue: 100)
            cell.textRightPaddingConstraint.priority = UILayoutPriority(rawValue: 100)
            cell.textRightPaddingConstraint2.priority = UILayoutPriority(rawValue: 999)
            
            
            let matches1 = detectorPhoneNumber.matches(in: message.message!, options: [], range: NSRange(location: 0, length: message.message!.utf16.count))
            
            let matches2 = detectorLink.matches(in: message.message!, options: [], range: NSRange(location: 0, length: message.message!.utf16.count))
            
            if matches1.count > 0 {
                for match in matches1 {
                    guard let range = Range(match.range, in: message.message!) else { continue }
                    let url = message.message![range]
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(url))
                    attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, attributeString.length))

                    cell.messageTextView.attributedText = attributeString
                }
            }
            if matches2.count > 0 {
                for match in matches2 {
                    guard let range = Range(match.range, in: message.message!) else { continue }
                    let url = message.message![range]
                    let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(url))
                    attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                    cell.messageTextView.attributedText = attributeString
                }
            }
            
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if messages.count > 0 {
            let indexPath = IndexPath(item: (messages.count - 1), section: 0)
            tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "fromChatViewControllerToProfileViewControllerSegue" {
            if let vc = segue.destination as? ViewProfileTableViewController {
                vc.info = sender as? [String: Any]
            }
        }
    }
}

extension String {
    func encode() -> String? {
        let data = self.data(using: .nonLossyASCII, allowLossyConversion: true)!
        return String(data: data, encoding: .utf8)
    }
    
    func decode() -> String? {
        let data = self.data(using: .utf8)!
        return String(data: data, encoding: .nonLossyASCII)
    }
    
}
