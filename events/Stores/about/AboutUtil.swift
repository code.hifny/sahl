//
//  AboutUtil.swift
//  events
//
//  Created by Intcore on 3/27/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation

class AboutUtil {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("about")
    
    static func save(_ items: about?) {
        if items != nil {
            NSKeyedArchiver.archiveRootObject(items!, toFile: self.ArchiveURL.path)
        }
    }
    
    static func load() -> about? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: self.ArchiveURL.path) as? about
    }
    
//    static func loadInfo() -> about? {
//        if let unarchivedObject = UserDefaults.standard.object(forKey: aboutKey) as? Data {
//            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? about
//        }
//        return nil
//    }
//    
//    static func saveInfo(_ about : about?) {
//        let archivedObject = archiveUser(about!)
//        UserDefaults.standard.set(archivedObject, forKey: aboutKey)
//        UserDefaults.standard.synchronize()
//    }
    
}


