//
//  AgendaUtil.swift
//  events
//
//  Created by abdelrahman on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation

class AgendaUtil {
    
    static func save(_ items: [AgendaItem]?) {
        if items != nil {
            NSKeyedArchiver.archiveRootObject(items!, toFile: AgendaItem.ArchiveURL.path)
        }
    }
    
    static func load() -> [AgendaItem]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: AgendaItem.ArchiveURL.path) as? [AgendaItem]
    }
    
    static func allSessions() -> [Agenda] {
        if let allItems = self.load() {
            var sessions:[Agenda] = []
            allItems.forEach({ (item) in
                item.agenda?.forEach({ (session) in
                    sessions.append(session)
                })
            })
            return sessions
        }
        return []
    }
    
    static func allSessions(_ items:[Agenda]) -> [Agenda] {
        if let allItems = self.load() {
            var sessions:[Agenda] = []
            allItems.forEach({ (item) in
                item.agenda?.forEach({ (session) in
                    sessions.append(session)
                })
            })
            return sessions
        }
        return []
    }
}




