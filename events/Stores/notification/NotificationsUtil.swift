//
//  Notifications.swift
//  events
//
//  Created by Intcore on 3/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import UserNotifications

class NotificationsUtil {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("notify")
   
    static func set(_ identifier:String , title:String , body:String , date:Date , userInfo :[AnyHashable : Any]) {
        print("Done Add")
        print("userInfo" , userInfo.count)
        let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.second,.minute,.hour,.day], from: date), repeats: false)
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = body
        content.userInfo = userInfo
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            print(error?.localizedDescription ?? "")
        }
    }
    
    static func saveNotify(_ items: [Notifications]?) {
        if let items = items {
            NSKeyedArchiver.archiveRootObject(items, toFile: ArchiveURL.path)
        }
    }
    
    static func loadNotify() -> [Notifications]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: self.ArchiveURL.path) as? [Notifications]
    }
    
    static func remove(_ identifier:String )  {
        print("Done Remove")
        let firstId = identifier+"-5"
        let secondId = identifier+"-15"
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [firstId , secondId ])
    }
    
    static func removeOne(_ identifier:String )  {
        print("Done one Remove")
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier ])
    }

    static func removeAll() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }

    
    static func appendAllWithLogin() {
        self.loadSessions { (sessions) in
            sessions.forEach({ (session) in
                let sessionId = "\(session.id ?? 0)"
                let sessionDateString = (session.date ?? "") + " " + (session.time_from ?? "")
                let sessionTitle = "\(session.title ?? "")"
                print("sessionDateString" , sessionDateString)
                let sessionDate = sessionDateString.timestamp

                print("sessionDateString" , sessionDateString)
                let dateBefore15Minute = Calendar.current.date(byAdding: .minute , value: -15, to: sessionDate)
                let dateBefore5Minute = Calendar.current.date(byAdding: .minute, value: -5, to: sessionDate)
                
                print("dateBefore15Minute" , dateBefore15Minute)

                self.set(sessionId+"-5", title: sessionTitle, body: "\(sessionTitle) will start after 5 minutes", date: dateBefore5Minute!, userInfo: ["sessionId" : sessionId+"-5" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 5 minutes" ,"minutesType" : "5", "created_at" : dateBefore5Minute ?? "", "status": false , "type" : "local" , "startSession":sessionDateString ])
                
                self.set(sessionId+"-15", title: sessionTitle, body: "\(sessionTitle) will start after 15 minutes", date: dateBefore15Minute!, userInfo: [  "sessionId" : sessionId+"-15" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 15 minutes" ,"minutesType" : "15", "created_at" : dateBefore15Minute ?? "" , "status": false , "type" : "local", "startSession":sessionDateString])
            })
        }
        
    }
    
    static func loadSessions(_ completion: @escaping ( [Agenda]) -> ()) {
        Requests.instance.request(link: "Sessions".route, method: .get,parameters: ["api_key":UserUtil.loadUser()?.api_key ?? ""]) { (response) in
            if response.haveError{
                //                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    DispatchQueue.main.async {
                         let sessions = response.data!["data"]["sessions"].map{Agenda($0.1)}
                        SessionsUtil.saveSessions(sessions)

//                        let allData = response.data!["data"].dictionaryObject ?? [:]
//                        let agendaItems = allData.map({ (arg: (key: String, value: Any)) ->  AgendaItem in
//                            let (key, value) = arg
//                            return AgendaItem(key , JSON(value as? Array ?? []))
//                        })
//
//                        AgendaUtil.save(agendaItems)
                        SessionsUtil.saveSessions(sessions)
                        completion( AgendaUtil.allSessions(sessions))
                    }
                }
            }
        }
    }
    
    
    
    static let ArchiveURLItem = DocumentsDirectory.appendingPathComponent("notificationsItems")
    
    
    static func saveNotifcation(_ items: [Agenda]?) {
        if items != nil {
            NSKeyedArchiver.archiveRootObject(items!, toFile: NotificationsUtil.ArchiveURLItem.path)
        }
    }
    
    static func loadNotifications() -> [Agenda]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: NotificationsUtil.ArchiveURLItem.path) as? [Agenda]
    }
}
