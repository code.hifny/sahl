//
//  MaterialUtil.swift
//  events
//
//  Created by Intcore on 3/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation

class MaterialUtil {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("material")
    
    static func save(_ items: [Material]?) {
        if items != nil {
            NSKeyedArchiver.archiveRootObject(items!, toFile: self.ArchiveURL.path)
        }
    }
    
    static func load() -> [Material]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: self.ArchiveURL.path) as? [Material]
    }
}
