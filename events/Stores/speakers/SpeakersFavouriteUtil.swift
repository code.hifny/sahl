//
//  SpeakersFavouriteUtil.swift
//  events
//
//  Created by Intcore on 3/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
class SpeakersFavouriteUtil {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("speakersFav")
    
    static func save(_ items: [Speakers]?) {
        if items != nil {
            NSKeyedArchiver.archiveRootObject(items!, toFile: self.ArchiveURL.path)
        }
    }
    
    static func load() -> [Speakers]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: self.ArchiveURL.path) as? [Speakers]
    }
    
}
