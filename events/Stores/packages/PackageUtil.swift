//
//  PackageUtil.swift
//  events
//
//  Created by Intcore on 3/21/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation

class PackageUtil {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("packages")
    
    static func save(_ items: [Packages]?) {
        if items != nil {
            NSKeyedArchiver.archiveRootObject(items!, toFile: self.ArchiveURL.path)
        }
    }
    
    static func load() -> [Packages]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: self.ArchiveURL.path) as? [Packages]
    }
}


