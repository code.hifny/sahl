//
//  Validation+Ext+UIViewController.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Foundation
import NVActivityIndicatorView
import Alamofire
import AlamofireImage
import Material
import SwiftyJSON

extension UIViewController
{
    
    func ShowAlertTextField(TextField:TextField , message:String) {
        TextField.detail = message
        TextField.dividerColor = .red
        TextField.detailLabel.textAlignment = Config.locale == .en ? .right : .left
        TextField.detailColor = .red
        
    }
    
    public static var successBackgroundColor : UIColor = UIColor(red: 142.0/255, green: 183.0/255, blue: 64.0/255,  alpha: 0.95)//UIColor(hex:"000000").withAlphaComponent(0.7)
    public static var infoBackgroundColor    : UIColor = UIColor(red: 142.0/255, green: 183.0/255, blue: 64.0/255,  alpha: 0.95)//UIColor(hex:"000000").withAlphaComponent(0.7)
    public static var warningBackgroundColor : UIColor = UIColor(red: 230.0/255, green: 189.0/255, blue: 1.0/255,   alpha: 0.95)//UIColor(hex:"c0392b").withAlphaComponent(1)
    public static var errorBackgroundColor   : UIColor = UIColor(hex:"c0392b").withAlphaComponent(1)
    
    func AlertConfig(_ ISMessages:ISMessages) {
        ISMessages.titleLabelFont = UIFont(name: "NeoSansArabic", size: 16)
        ISMessages.titleLabelTextColor = .white
        ISMessages.messageLabelFont = UIFont(name: "NeoSansArabic", size: 14)
        ISMessages.messageLabelTextColor = .white
//        ISMessages.image
    }
    
    func InfoAlert(_ title:String = "Info".locale,message:String){
        let cardAlert = ISMessages.cardAlert(withTitle: title, message: message, iconImage: UIImage(named:"exclamation-circle"), duration: 5, hideOnSwipe: true, hideOnTap: true, alertType: .custom, alertPosition: .top)
        AlertConfig(cardAlert!)
        cardAlert?.alertViewBackgroundColor = UIViewController.infoBackgroundColor//UIColor(white: 0.1, alpha: 0.95)
        cardAlert?.show(nil)
    }
    
    func WarningAlert(_ title:String = "Warning".locale, message:String){
        let cardAlert = ISMessages.cardAlert(withTitle: title, message: message, iconImage: UIImage(named:"exclamation-circle"), duration: 5, hideOnSwipe: true, hideOnTap: true, alertType: .custom, alertPosition: .top)
        
        cardAlert?.alertViewBackgroundColor = UIViewController.warningBackgroundColor//UIColor(white: 0.1, alpha: 0.95)
        AlertConfig(cardAlert!)
        cardAlert?.show(nil)
    }
    
    
    func DangerAlert(_ title:String = "Worng".locale ,message:String){
        let cardAlert = ISMessages.cardAlert(withTitle: title, message: message, iconImage: UIImage(named:"exclamation-circle"), duration: 5, hideOnSwipe: true, hideOnTap: true, alertType: .custom, alertPosition: .top)
        
        cardAlert?.alertViewBackgroundColor = UIViewController.errorBackgroundColor//UIColor(white: 0.1, alpha: 0.95)
        AlertConfig(cardAlert!)
        cardAlert?.show(nil)
    }
    
    func SuccessAlert(_ title:String = "Success".locale,message:String){
        let cardAlert = ISMessages.cardAlert(withTitle: title, message: message, iconImage: UIImage(named:"exclamation-circle"), duration: 5, hideOnSwipe: true, hideOnTap: true, alertType: .custom, alertPosition: .top)
        cardAlert?.alertViewBackgroundColor = UIViewController.successBackgroundColor//UIColor(white: 0.1, alpha: 0.95)
        AlertConfig(cardAlert!)
        cardAlert?.show(nil)
    }
}
