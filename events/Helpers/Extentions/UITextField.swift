//
//  UITextField.swift
//  opmakup
//
//  Created by abdelrahman on 5/11/17.
//  Copyright © 2017 abdelrahman. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0
    
    @IBInspectable var color: UIColor = UIColor.lightGray {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedStringKey.foregroundColor: color])
    }
}

extension UITextField{
    func SetFieldWithoutBorderImage(image:UIImage,frame:CGRect) {
        let View = UIView()
        let arrow = UIImageView(image: image)
        View.frame = CGRect(x: 0, y: 0, width: frame.width + 10.0, height:  frame.height + 10)
        arrow.frame = frame
        View.addSubview(arrow)
        arrow.contentMode = UIViewContentMode.scaleToFill
        self.rightView = View
        self.rightViewMode = UITextFieldViewMode.always
    }
    func SetFieldImage(image: UIImage, frame: CGRect) {
        let View = UIView()
        let arrow = UIImageView(image: image)
        View.frame = CGRect(x: 0, y: 0, width: frame.width + 10.0, height:  frame.height + 10)
        arrow.frame = frame
        View.addSubview(arrow)
        arrow.contentMode = UIViewContentMode.scaleToFill

        self.rightView = View
        self.rightViewMode = UITextFieldViewMode.always
        self.layer.borderColor = UIColor(hex:"e2dede").cgColor
        self.layer.borderWidth = 1.0
    }
    func setBottomBorder(color:UIColor , height:CGFloat) {
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: height)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
