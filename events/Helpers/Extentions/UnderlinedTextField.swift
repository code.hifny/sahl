
import Foundation
import Material
import UIKit

class UnderlinedTextField: UITextField {
    
    @IBOutlet weak var underlineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        underlineView.backgroundColor = UIColor.lightGray
            
    }
}
