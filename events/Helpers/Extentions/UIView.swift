//
//  UIView.swift
//  ihere
//
//  Created by abdelrahman on 10/21/17.
//  Copyright © 2017 abdelrahman. All rights reserved.
//

import UIKit
import DGActivityIndicatorView
import SnapKit

@IBDesignable
extension UIView {
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow(shadowColor:UIColor.black.cgColor )
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    @IBInspectable var SetShadowColor: UIColor {
        get {
            return .black
        }
        set {
            self.addShadow(shadowColor: newValue.cgColor )
        }
    }
    
    func addShadow(shadowColor: CGColor = Color.grayColor.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0, height: 0),
                   shadowOpacity: Float = 0.3,
                   shadowRadius: CGFloat = 4.0) {
        self.layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    
    class func loadNib<T: UIView>(_ viewType: T.Type) -> T {
        let className = String.className(viewType)
        return Bundle(for: viewType).loadNibNamed(className, owner: nil, options: nil)!.first as! T
    }
    
    class func loadNib() -> Self {
        return loadNib(self)
    }

    
    func addTapGesture(tapNumber : Int, target: Any , action : Selector) {
        
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
        
    }
    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
//    func fadeIn(withDuration duration: TimeInterval = 1.0) {
//        UIView.animate(withDuration: duration, animations: {
//            self.alpha = 1.0
//        })
//    }
    
    /**
     Fade out a view with a duration
     
     - parameter duration: custom animation duration
     */
//    func fadeOut(withDuration duration: TimeInterval = 1.0) {
//        UIView.animate(withDuration: duration, animations: {
//            self.alpha = 0.0
//        })
//    }
//    
    
    func addGradientWithColor(_ colors: UIColor...) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    
    func setGradientBackground() {
        let colorTop =   Color.blueColor.cgColor//UIColor(red: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0).CGColor
        let colorBottom = Color.darkBlueColor.cgColor//UIColor(red: 255.0/255.0, green: 94.0/255.0, blue: 58.0/255.0, alpha: 1.0).CGColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.bounds
        
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func gradiantView(horizontalMode:  Bool = false , diagonalMode:    Bool = false ) {
        let view = GradientView()
        view.frame = self.bounds
        view.startColor = Color.darkBlueColor
        view.endColor = Color.blueColor
        view.horizontalMode = horizontalMode
        view.diagonalMode = diagonalMode        
        self.addSubview(view)
//        layoutSubviews()

//        let gradient = CAGradientLayer()
//
//        if horizontalMode {
//            gradient.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
//            gradient.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
//        } else {
//            gradient.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
//            gradient.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
//        }
//        gradient.frame = self.bounds
//        gradient.locations = [0.05 as NSNumber, 0.95 as NSNumber]
//        gradient.colors    = [Color.greenForGradiantColor.cgColor, Color.blueColor.cgColor]
//        self.layer.insertSublayer(gradient, at: 0)

    }

}

extension UIView{
    
    func showLoadingView(at centerYOffset: CGFloat, withColor color: UIColor) {
        
        self.stopLoading()
        
        let indicatorView = DGActivityIndicatorView(type: .ballClipRotate, tintColor: color, size: 40)
        indicatorView?.tag = 112233
        
        indicatorView?.startAnimating()
        
        self.addSubview(indicatorView!)
        self.bringSubview(toFront: indicatorView!)
        
        indicatorView?.snp.makeConstraints { (make) in
            
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview().offset(centerYOffset)
        }
    }
    
    func startLoading(withColor color: UIColor) {
        self.showLoadingView(at: 0, withColor: color)
    }
    
    func startLoading() {
        self.showLoadingView(at: 0, withColor: Color.greenColor)
    }
    
    
    
    func stopLoading() {
        
        let loadingView = self.viewWithTag(112233) as? DGActivityIndicatorView
        
        loadingView?.stopAnimating()
        
        loadingView?.removeFromSuperview()
    }
}
