//
//  CALayer.swift
//  ihere
//
//  Created by abdelrahman on 10/21/17.
//  Copyright © 2017 abdelrahman. All rights reserved.
//

import Foundation
import UIKit
extension CALayer {
    
    
    func configureGradientBackground( startPoint:CGPoint , endPoint:CGPoint ,colors:CGColor... ){
        
        let gradient = CAGradientLayer()
      //  let maxWidth = max(self.bounds.size.height,self.bounds.size.width)
        let squareFrame = CGRect(origin: self.bounds.origin, size: CGSize(width:(self.superlayer?.bounds.width)!, height:self.bounds.size.height))
        gradient.frame = squareFrame
        //CGPoint(x: 0, y: 0.5)
        gradient.startPoint = startPoint
        //CGPoint(x: 1, y: 0.5)
        gradient.endPoint = endPoint
        gradient.colors = colors
        
        self.insertSublayer(gradient, at: 0)
    }
    
    func configureGradientBackgroundMain( startPoint:CGPoint , endPoint:CGPoint ,colors:CGColor... ){
        
        let gradient = CAGradientLayer()
        let squareFrame = CGRect(origin:self.bounds.origin, size: CGSize(width:UIScreen.main.bounds.size.width, height:self.bounds.size.height))
        gradient.frame = squareFrame
        //CGPoint(x: 0, y: 0.5)
        gradient.startPoint = startPoint
        //CGPoint(x: 1, y: 0.5)
        gradient.endPoint = endPoint
        gradient.colors = colors
    
        self.insertSublayer(gradient, at: 0)
    }
    
    func configureGradientBackgroundProfileEdit( startPoint:CGPoint , endPoint:CGPoint ,colors:CGColor... ){
        
        let gradient = CAGradientLayer()
        let squareFrame = CGRect(origin:self.bounds.origin, size: CGSize(width:UIScreen.main.bounds.size.width, height:150))
        gradient.frame = squareFrame
        //CGPoint(x: 0, y: 0.5)
        gradient.startPoint = startPoint
        //CGPoint(x: 1, y: 0.5)
        gradient.endPoint = endPoint
        gradient.colors = colors
        
        self.insertSublayer(gradient, at: 0)
    }
}
