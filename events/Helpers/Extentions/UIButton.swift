//
//  UIButton.swift
//  ffexa
//
//  Created by Abdelrahman on 3/13/17.
//  Copyright © 2017 Abdelrahman. All rights reserved.
//

import UIKit

extension UIButton {
    func setBottomBorder(color:UIColor , height:CGFloat) {
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: height)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
