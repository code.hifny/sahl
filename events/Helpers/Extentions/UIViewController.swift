//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON
import SideMenu
import Lightbox
//import STZPopupView
import SafariServices
import NVActivityIndicatorView
import CFAlertViewController
import Realm
import RealmSwift
var timerTest : Timer? = nil {
    willSet {
        timerTest?.invalidate()
    }
}

var progressLabel: UILabel!
var LoadingLabel: UILabel!
var progress: UInt8 = 0
var window: UIWindow?

extension UIViewController : NVActivityIndicatorViewable, SFSafariViewControllerDelegate {
    
    // for network status listner
    @objc func networkStatusChanged(_ notification: Notification) {
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            helper.connected = false
        case .online(.wwan):
            helper.connected = true
        case .online(.wiFi):
            helper.connected = true
        }
    }
    
    
    //    func NotificationAlert(message : String , AutoHide:Bool = true,cancelTitle:String = Lang.get("Cancel")){
    //        // Create Alet View Controller
    //        let alertController = CFAlertViewController(title: Lang.get("error"),
    //                                                    titleColor: .white,
    //                                                    message: message,
    //                                                    messageColor: .white,
    //                                                    textAlignment: .center,
    //                                                    preferredStyle: .notification,
    //                                                    didDismissAlertHandler: nil)
    //        let image = UIImageView(frame:CGRect(x:0,y:0,width:0,height:110))
    //        image.contentMode = .scaleAspectFit
    //        image.clipsToBounds = true
    //        image.image = UIImage(named:"user")
    //        alertController.headerView = image
    //        alertController.containerView?.backgroundColor = UIColor.red.withAlphaComponent(0.5)
    //        // Present Alert View Controller
    //        present(alertController, animated: true, completion: nil)
    //        if AutoHide == true {
    //            DispatchQueue.main.asyncAfter(deadline: .now() + 3 , execute: {
    //                self.dismiss(animated: true)
    //            })
    //        }
    //    }
    
    func openLinkWithSafari(link:String)  {
        let url = URL(string: link)!
        let safariVC = SFSafariViewController(url: url)
        UIApplication.topViewController()?.present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
 
    }
    
   
    //MARK: - Alerts
    func actionSheet(title: String, actionTitle : String ,cancelTitle:String =
        "Cancel".locale , completion: @escaping (_ action: CFAlertAction) -> ()){
        // Create Alet View Controller
        let alertController = alertViewController(title)
        // Create Upgrade Action
        let defaultAction = defaultAlertAction(actionTitle) { (action) in
            completion(action)
        }
        // Create Cancel Action
        let cancelAction = cancelAlertAction()
        // Add Action Button Into Alert
        alertController.addAction(defaultAction)
        // Add Action Button Into Alert
        alertController.addAction(cancelAction)
        // Present Alert View Controller
        present(alertController, animated: true, completion: nil)
    }
    
    func alertViewController(_ title: String) -> CFAlertViewController{
        
        // Create Alet View Controller
        let alertController = CFAlertViewController(title: title,
                                                    titleColor:  Color.greenColor,
                                                    message: nil,
                                                    messageColor: nil,
                                                    textAlignment: .center,
                                                    preferredStyle: .actionSheet,
                                                    headerView: nil,
                                                    footerView: nil,
                                                    didDismissAlertHandler: nil)
        
        return alertController
    }
    
    func defaultAlertAction(_ title:String, backgroundColor:UIColor = Color.greenColor, completion: @escaping (_ action: CFAlertAction) -> () ) -> CFAlertAction{
        // Create Action
        let action = CFAlertAction(title: title,
                                   style: .Default,
                                   alignment: .justified,
                                   backgroundColor: backgroundColor,
                                   textColor: nil,
                                   handler: { (action) in
                                    completion(action)
        })
        action.backgroundColor = UIColor.fromGradientWithDirection(.leftToRight, frame: self.view.frame)
        return action
    }
    
    func cancelAlertAction(_ title:String =
        "Cancel".locale) ->  CFAlertAction{
        // Create Action
        let action = CFAlertAction(title: title,
                                   style: .Default,
                                   alignment: .justified,
                                   backgroundColor: .lightGray,
                                   textColor: .white,
                                   handler: nil)
        return action
    }
    
    
    open class func showCardAlert(with title: String, message: String) {
        
        let cardAlert = ISMessages.cardAlert(withTitle: title, message: message, iconImage: UIImage(named:"main_close_icon"), duration: 10, hideOnSwipe: true, hideOnTap: true, alertType: .custom, alertPosition: .top)
        
        cardAlert?.alertViewBackgroundColor = UIColor(white: 0.1, alpha: 0.95)
        
        cardAlert?.titleLabelFont = UIFont(name: "NeoSansArabic", size: 16)
        cardAlert?.titleLabelTextColor = UIColor.white
        
        cardAlert?.messageLabelFont = UIFont(name: "NeoSansArabic", size: 14)
        cardAlert?.messageLabelTextColor = UIColor.white
        
        cardAlert?.show(nil)
    }
    
    
    /*
     * set Status bar Color
     * Color
     */
    func SetStatusBarColor(Color:UIColor)  {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Color
        }
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    /*
     * add keyboard hide in view
     */
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    /*
     * set Navigation bar with background clear
     */
    func clearNav()  {
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-2000, 0), for: .default)
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        if Config.locale == .en {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:  UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
            
        }else{
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
            
        }
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.backgroundColor = .clear
        //        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: .systemFont(ofSize: 20)]
    }
    
    /*
     * set Navigation bar Configration
     */
    func navConfig()  {
        
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = Color.grayColor
        
        
        
        self.navigationController?.navigationBar.tintColor = .white//Color.greenColor
        // Background and seperator
        //        let image = UIImage.fromGradientWithDirection(.rightToLeft,frame: self.navigationBar.frame)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
        }
        //        let paragraph = NSMutableParagraphStyle()
        //        paragraph.alignment = .right
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
        
        //        self.navigationBar.largeTitleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:27)!]
        self.navigationController?.view.backgroundColor = Color.grayColor
        let backImage = UIImage(named: "back")?.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-2000, 0), for: .default)
        
        if Config.locale == .en {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:  UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
            
        }else{
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
            
        }
        
//
//
//        self.navigationController?.navigationBar.isTranslucent = false
//        self.navigationController?.navigationBar.barTintColor = Color.greenColor
//        self.navigationController?.navigationBar.tintColor = Color.greenColor
//        // Background and seperator
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        self.navigationController?.navigationBar.shadowImage = UIImage()
//
//
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,NSAttributedStringKey.font: UIFont(name:"NeoSansArabic" , size:17)!]
//
//        self.navigationController?.view.backgroundColor = Color.greenColor
//        self.navigationController?.navigationBar.backgroundColor = Color.greenColor
//        let backImage = UIImage(named: "back")?.withRenderingMode(.alwaysOriginal)
//        UINavigationBar.appearance().backIndicatorImage = backImage
//        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
//        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-2000, 0), for: .default)
        
    }
    func navGray()  {
        self.navigationController?.navigationBar.barTintColor = Color.grayColor
        self.navigationController?.navigationBar.tintColor = UIColor.black
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont(name:"NeoSansArabic-bold" , size:17)!]
        
    }
    func navConfigBlak()  {
        self.clearNav()
        let frame = CGRect(x:0,y:0,width:view.frame.width , height: 65)
        let img = UIImage.fromGradientWithDirection(.topToBottom,frame: frame, colors:[ UIColor.black.withAlphaComponent(0.5),.clear])
        self.navigationController?.navigationBar.setBackgroundImage(img, for: UIBarMetrics.default)
    }
    
    func navWhite()  {
        self.navigationController?.navigationBar.barTintColor = .white
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font: UIFont(name:"NeoSansArabic-bold" , size:17)!]

    }
    /*
     * set right bar items
     * [UIButtons...]
     */
    func RightNavBarItems(_ Btns:UIBarButtonItem...) {
        navigationItem.rightBarButtonItems = Btns
    }
    /*
     * set left bar items
     * [UIButtons...]
     */
    func LeftNavBarItems(_ Btns:UIBarButtonItem...) {
        navigationItem.leftBarButtonItems = Btns
    }
    
    /*
     * return Menu button
     */
    func MenuBtn() -> UIBarButtonItem {
        let BarButtonItem =  UIBarButtonItem()
        BarButtonItem.setIcon(icon: .fontAwesome(.bars), iconSize: 24, color: .white)
//        BarButtonItem.image = UIImage(named:"menu")
//        BarButtonItem.tintColor = Color.greenColor
        BarButtonItem.action = #selector(openMenu)
        BarButtonItem.target = self
        return BarButtonItem
    }
    /*
     * return edit button
     */
    func editBtn() -> UIBarButtonItem {
        let BarButtonItem =  UIBarButtonItem()
        BarButtonItem.title = "Edit".locale
        BarButtonItem.tintColor = .white
        BarButtonItem.action = #selector(editNavBtn)
        BarButtonItem.target = self
        return BarButtonItem
    }
    
    @objc func editNavBtn() {
        
    }
    /*
     * return back button
     */
    func BackBtn() -> UIBarButtonItem {
        let button = ButtonWithImage("back")
        button.addTarget(self, action: #selector(PopVC), for: .touchUpInside)
        return BarBtnConfig(button)
    }
    
    
    /*
     * return back button
     */
    func skipBtn() -> UIBarButtonItem {
        let BarButtonItem =  UIBarButtonItem()
        BarButtonItem.title = "Skip".locale
        BarButtonItem.tintColor = .black
//        BarButtonItem.layer.cornerRadius = 15
        BarButtonItem.target = self
        BarButtonItem.action = #selector(goToSessionType)
        return BarButtonItem
    }
    
    /*
     * return Search button
     */
    //    func NotificationBtn() -> UIBarButtonItem {
    //        let button = ButtonWithImage("Ellipse_1")
    //        //        button.setIcon(icon: .fontAwesome(.search), iconSize: 20, color: .white, backgroundColor: .clear, forState: .normal)
    //        return BarBtnConfig(button)
    //    }
    func NotificationBtn() -> UIBarButtonItem {
        let BarButtonItem =  UIBarButtonItem()
        //        BarButtonItem.setIcon(icon: .fontAwesome(.bars), iconSize: 24, color: MainColor)
        BarButtonItem.image = UIImage(named:"Ellipse_1")
        BarButtonItem.tintColor = Color.greenColor
        BarButtonItem.action = #selector(openNotification)
        //        BarButtonItem.target = self
        return BarButtonItem
    }
    
    @objc func openNotification() {
        Push(storyboard: "Settings", Identifier: "NotificationsTableViewController")
    }
    /*
     * return Search button
     */
    func SearchBtn() -> UIBarButtonItem {
        let button = ButtonWithImage("search")
        //        button.setIcon(icon: .fontAwesome(.search), iconSize: 20, color: .white, backgroundColor: .clear, forState: .normal)
        
        return BarBtnConfig(button)
    }
    
    /*
     * return close item as bar button item
     */
    func NavCloseItem()-> UIBarButtonItem {
        return BarBtnConfig(ButtonWithIcon(.times))
    }
    
    /*
     * return bar button item with icon
     */
    func ButtonWithIcon(_ Icon:FAType) -> UIButton {
        var button: UIButton = UIButton (type:.custom)
        button =  IconButton(image: UIImage(named:""), tintColor: Color.greenColor)
        button.frame = CGRect(x:0, y:0, width:30, height:30)
        button.setIcon(icon: .fontAwesome(Icon), iconSize: 20, color: .white, forState: .normal)
        button.contentHorizontalAlignment = .center
        button.addTarget(self, action: #selector(ClosePresent), for: .touchUpInside)
        return button
    }
    
    func BarBtnConfig(_ button:UIButton) -> UIBarButtonItem {
        let BarButtonItem =  UIBarButtonItem(customView: button)
        BarButtonItem.tintColor = UIColor.white
        return BarButtonItem
    }
    
    /*
     * return bar button item with image
     */
    func ButtonWithImage(_ Image:String) -> UIButton {
        var button: UIButton = UIButton (type:.custom)
        button =  IconButton(image:  UIImage(named:Image) , tintColor: Color.greenColor)
        button.frame = CGRect(x:0, y:0, width:50, height:30)
        button.contentHorizontalAlignment = .center
        return button
    }
    
    @objc func PopVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func ClosePresent() {
        dismiss(animated: true)
    }
    
    @objc func openMenu() {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        SideMenuManager.default.menuRightNavigationController = storyboard.instantiateViewController(withIdentifier: "RightMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuFadeStatusBar  = true
        SideMenuManager.default.menuShadowOpacity = 0.3
        SideMenuManager.default.menuAnimationTransformScaleFactor = 1
        SideMenuManager.default.menuAnimationBackgroundColor = .black
        SideMenuManager.default.menuAnimationFadeStrength = 0.4
        SideMenuManager.default.menuParallaxStrength = 2
        
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width / 1.2
        UIApplication.topViewController()?.present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func RootPush(storyboard:String , Identifier:String, animated:Bool = true)  {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifier)
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: controller)
        UIApplication.topViewController()?.navigationController?.pushViewController(navEditorViewController, animated: animated)
    }
    
    
    func Push(storyboard:String , Identifier:String, animated:Bool = true)  {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifier)
        UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: animated)
    }
    
    func RootPresent(storyboard:String , Identifier:String, animated:Bool = true)  {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifier)
        controller.modalPresentationStyle = .custom
        let navEditorViewController: UINavigationController = UINavigationController(rootViewController: controller)
        UIApplication.topViewController()?.present(navEditorViewController, animated: animated, completion: nil)
    }
    
    func Present(storyboard:String , Identifier:String, animated:Bool = true)  {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: Identifier)
        controller.modalPresentationStyle = .custom
        UIApplication.topViewController()?.present(controller, animated: animated, completion: nil)
    }
    
    func validationShowMessage(_ data:JSON , _ name:String) {
        if let valid = data["error"][name].arrayObject{
            UIApplication.topViewController()?.DangerAlert(message: valid[0] as! String)
        }
    }
//    func validShowMessageAlert(_ Data:JSON , _ name:String, _ field:ETSTextField) {
//        if let valid = Data[name].arrayObject{
//            UIApplication.topViewController()?.DangerAlert(message: valid[0] as! String)
//            //            field.ShowAlertTextField(message: valid[0] as! String)
//        }
//    }
    
    func validShowMessageAlertWithoutField(_ Data:JSON , _ name:String) {
        if let valid = Data[name].arrayObject{
            UIApplication.topViewController()?.DangerAlert(message: valid[0] as! String)
            //            field.ShowAlertTextField(message: valid[0] as! String)
        }
    }
    @objc func goToSessionType(_ withClearData:Bool = false) {
        let vc =  UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "SessionTypeViewController") as! SessionTypeViewController
//        let navigationController = EventsNavigationController(rootViewController: vc)
        if withClearData {
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
        }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionFade
        self.view.window?.layer.add(transition, forKey: kCATransition)
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    @objc func goToHome(_ withClearData:Bool = false) {
//        let vc =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "HomeTabBarController") as! HomeTabBarController
//        let navigationController = EventsNavigationController(rootViewController: vc)
        if withClearData {
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
        }
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionFade
        self.view.window?.layer.add(transition, forKey: kCATransition)
//        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    func goToLogin() {
        let vc =  UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let navigationController = EventsNavigationController(rootViewController: vc)
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionFade
        self.view.window!.layer.add(transition, forKey: kCATransition)
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }

//    func goToSettings() {
//        let vc =  UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsMenuViewController
//        let navigationController = EventsNavigationController(rootViewController: vc)
//        let transition = CATransition()
//        transition.duration = 0.5
//        transition.type = kCATransitionFade
//        UIApplication.topViewController()?.view.window!.layer.add(transition, forKey: kCATransition)
//        UIApplication.shared.keyWindow?.rootViewController = navigationController
//    }
    func removeNavigationBarItems() {
        self.navigationItem.leftBarButtonItems = nil
        self.navigationItem.rightBarButtonItems = nil
    }
    
    func LogoutAlert(){
        self.actionSheet(title: "", actionTitle: "Logout Title".locale) { (action) in
            self.goToLogin()
        }
        let alert = UIAlertController(title: "Logout Title".locale, message: "Logout Message".locale, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title:"Yes".locale, style: UIAlertActionStyle.default,  handler: { action in
            print("yes")
            UIApplication.shared.unregisterForRemoteNotifications()
            self.goToLogin()
            //self.sideMenuViewController?.hideMenuViewController()
            //            Firebase registration token: d3j7mmIGL50:APA91bGZJew0KTX5Fs_7yp1NVE5eR7ZYAdT5YZqENuVKoA3F1-FqXnLWTVBZsLlonICZ5d-uRSF7qQNHUAORrUCTW4rX6h4A9ZD-2higpespz3Dvzj-7FRjMvcMaN7cS2u4LRC6Frgwc
            //            APNs device token: ab9931d7ead41eb123439e9f7568e81bc29e7b34539dd5371d93779a89cfd55e
        }))
        alert.addAction(UIAlertAction(title:"No".locale, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    func StartLoading()  {
        let size = CGSize(width: 50, height: 50)
        NVActivityIndicatorView.DEFAULT_COLOR = Color.greenColor
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        self.startAnimating(size, message: "", type: NVActivityIndicatorType.ballScaleMultiple )
    }
    
    func StopLoading()  {
        self.stopAnimating()
    }
    
    
    func backButtonPressed(btn : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func BackNotFounTableView(tableView:UITableView , text:String)  {
        let view = UIView(frame:CGRect(x:0,y:0,width:tableView.frame.width,height:tableView.frame.height))
        let bgImage = UIImageView(frame:CGRect(x:view.frame.width / 2 - 40 ,y:140 - 60 ,width:80,height:80));
        bgImage.image = UIImage(named: "noSearch");
        bgImage.contentMode = .scaleToFill
        let label = UILabel(frame:CGRect(x:0 ,y:120 + 60 ,width:view.frame.width,height:20));
        label.text = text
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 17)
        label.textAlignment = .center
        view.addSubview(bgImage)
        view.addSubview(label)
        tableView.backgroundView = view
    }
    func BackNotFounTableViewEvents(tableView:UITableView , text:String)  {
        let view = UIView(frame:CGRect(x:0,y:0,width:tableView.frame.width,height:tableView.frame.height))
        let label = UILabel(frame:CGRect(x:0 ,y:120 + 60 ,width:view.frame.width,height:20));
        label.text = text
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 17)
        label.textAlignment = .center
        view.addSubview(label)
        tableView.backgroundView = view
    }

    
    func BackNotFounCollection(collectionView:UICollectionView , text:String)  {
        let view = UIView(frame:CGRect(x:0,y:0,width:collectionView.frame.width,height:collectionView.frame.height))
        let bgImage = UIImageView(frame:CGRect(x:view.frame.width / 2 - 50 ,y:view.frame.height / 2 - 60 ,width:100,height:80));
        bgImage.image = UIImage(named: "");
        bgImage.contentMode = .scaleToFill
        let label = UILabel(frame:CGRect(x:0 ,y: 120 ,width:view.frame.width,height:20));
        label.text = text
        label.textColor = .lightGray
        label.font = .systemFont(ofSize: 17)
        label.textAlignment = .center
        view.addSubview(bgImage)
        view.addSubview(label)
        collectionView.backgroundView = view
    }
    
    //config tab bar
    func tabBarHideConfig(isHidden:Bool)  {
//        self.tabBarController?.view.viewWithTag(200)?.isHidden = isHidden
//        self.tabBarController?.view.viewWithTag(210)?.isHidden = isHidden
        self.tabBarController?.view.viewWithTag(220)?.isHidden = isHidden
        self.tabBarController?.tabBar.isHidden = isHidden
//        self.tabBarController?.view.viewWithTag(200)?.layer.zPosition = 4
//        self.tabBarController?.view.viewWithTag(210)?.layer.zPosition = 3
        self.tabBarController?.view.viewWithTag(220)?.layer.zPosition = 1

        self.tabBarController?.tabBar.layer.zPosition = 0
    }
    
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
}


extension UIViewController : LightboxControllerDismissalDelegate ,LightboxControllerPageDelegate{
    public func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    
    public func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
    func openImages(_ images:[String]) {
        
        let imgs:[LightboxImage] = images.map{LightboxImage(imageURL:URL(string:$0)!)}
        LightboxConfig.DeleteButton.enabled = false
        LightboxConfig.CloseButton.text = "Cancel".locale//"X"
        LightboxConfig.InfoLabel.enabled = false
        LightboxConfig.PageIndicator.enabled = false
        let lightbox = LightboxController(images: imgs , startIndex : 0)
        lightbox.dismissalDelegate = self
        // Set delegates.
        lightbox.pageDelegate = self
        // Use dynamic background.
        lightbox.dynamicBackground = true
        UIApplication.topViewController()?.present(lightbox, animated: true, completion: nil)
    }
    
}

