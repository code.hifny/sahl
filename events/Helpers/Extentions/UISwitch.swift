//
//  UISwitch.swift
//  ffexa
//
//  Created by Abdelrahman on 3/13/17.
//  Copyright © 2017 Abdelrahman. All rights reserved.
//


import UIKit

extension UISwitch {
func AddConstraintForSizes(View:UIView  , Iphone4:Float , Iphone5:Float , Iphone6:Float , Iphone7:Float, Ipad9:Float , Ipad12:Float) {
    let screenHeight = UIScreen.main.bounds.height
    
    switch screenHeight {
    case 480:  // 3.5  inch iphone4 (s)
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Iphone4)).isActive = true
    case 568:  // 4    inch iphone5 (c/s/se)
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Iphone5)).isActive = true
    case 667:  // 4.7  inch iphone 6/7 (s)
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Iphone6)).isActive = true
    case 736:  // 5.5  inch iphone 6/7 plus (s)
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Iphone7)).isActive = true
    case 768:  // 9.7  inch ipad 6/7 plus (s)
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Ipad9)).isActive = true
    case 1024:  // 12.9  inch ipad 6/7 plus (s)
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Ipad12)).isActive = true
    default:  // rest of screen sizes
        self.centerXAnchor.constraint(equalTo: View.centerXAnchor,  constant: CGFloat(Iphone7)).isActive = true
    }
}
}
