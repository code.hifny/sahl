//
//  EventsTextField.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import Material
import EasyLocalization

class EventsTextField: UITextField {
    var rightIcon:UIImage = UIImage()
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.textAlignment = EasyLocalization.getLanguage() == .ar ? .right : .left
        self.textColor = .black
        self.shadow = true
        self.shadowColor = Color.grayColor
        self.backgroundColor = UIColor.white
        self.cornerRadius = 12
        self.borderStyle = .none
        self.setIcon(rightIcon)
    }
    
    func whiteField() {
        self.textColor = UIColor(hex: "EBEBEB")
        self.backgroundColor = UIColor.white
        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "",attributes: [NSAttributedStringKey.foregroundColor: Color.grayColor])

    }
    
    func complainTextField() {
//        self.textColor = Color.grayColor
//        self.backgroundColor = UIColor.white
//        self.attributedPlaceholder = NSAttributedString(string:self.placeholder ?? "",attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        
        
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
    }
    
    func setIcon(_ icon:UIImage ,_ iconFrame:CGRect = CGRect(x:12,y:7.5,width:16,height:20)) {
        self.leftView = nil
        self.leftViewMode = .never
        self.rightView = nil
        self.rightViewMode = .never
        let view = UIView(frame: CGRect(x:0,y:0,width:45,height:40))
        let imageView = UIImageView()
        imageView.frame = iconFrame
        imageView.image = icon
        view.addSubview(imageView)
        self.leftView = view
        self.leftViewMode = .always
//        if EasyLocalization.getLanguage() == .en {
//            self.leftView = view
//            self.leftViewMode = .always
//        }
//        else {
//            self.rightView = view
//            self.rightViewMode = .always
//        }
    }

    @IBInspectable var icon: UIImage? {
        set {
            self.rightIcon = newValue ?? UIImage()
        }
        get {
            return self.rightIcon
        }
    }


}

class EventsMaterialTextField: TextField  {
    override func prepare() {
        super.prepare()
        self.textAlignment = EasyLocalization.getLanguage() == .ar ? .right : .left
        self.dividerNormalColor = Color.greenColor
        self.dividerActiveColor = Color.greenColor
        self.placeholderActiveColor = Color.greenColor
        self.placeholderNormalColor = Color.greenColor
        self.placeholderActiveScale = 1
        self.dividerActiveHeight = 0
        self.dividerNormalHeight = 0
        self.placeholderVerticalOffset = 16
        self.placeholderActiveScale = 0.75
    }
}

class EventsActiveTextField: TextField  {
    override func prepare() {
        super.prepare()
        self.textAlignment = .center
        self.dividerActiveColor = Color.grayColor
        self.textColor = Color.blueColor
        self.dividerNormalColor = Color.grayColor
        self.backgroundColor = .clear
        self.placeholderActiveColor = Color.greenColor
        self.placeholderNormalColor = Color.grayColor
        self.dividerActiveHeight = 2.5
        self.dividerNormalHeight = 2.5
        
//        self.placeholderVerticalOffset = 16
    }
    
    override func deleteBackward() {
        if self.text?.count == 0 {
            super.deleteBackward()
            // If conforming to our extension protocol
//            if let pinDelegate = self.delegate as? PinTexFieldDelegate {
//                pinDelegate.didPressBackspace(textField: self)
//            }
        }else{
            super.deleteBackward()
        }
        
    }
}
class EventsProfileTextField: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.textAlignment = EasyLocalization.getLanguage() == .ar ? .right : .left
        self.textColor = .darkGray
        self.backgroundColor = UIColor.white
        self.borderStyle = .none
    }
    
}


