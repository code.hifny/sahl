//
//  BaseStaticTableViewController.swift
//  Events
//
//  Created by abdelrahman on 2/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class BaseStaticTableViewController: UITableViewController {
    var indexs:[Int] = []
    var padding:CGFloat = 16
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.separatorInset.left = indexs.contains(indexPath.row) ? padding : view.frame.width
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
 
}
