//
//  FavouriteButton.swift
//  events
//
//  Created by Macbook Pro on 2/28/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import NVActivityIndicatorView

extension UIButton : NVActivityIndicatorViewable {
    
    func loadingButton()  {
        let size = CGSize(width: 50, height: 50)
        NVActivityIndicatorView.DEFAULT_COLOR = Color.greenColor
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR =  UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
//        self.startAnimating(size, message: "", type: NVActivityIndicatorType.ballScaleMultiple )
    }
    
}
