//
//  EventsTextView.swift
//  events
//
//  Created by abdelrahman on 2/26/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import Material
import EasyLocalization
@IBDesignable
class EventsTextView : TextView {
    
    override func prepare() {
        super.prepare()
        self.textContainerInset = UIEdgeInsetsMake(4, 4, 4, 4);
        self.placeholderColor = Color.grayColor

    }
    
    
    @IBInspectable var localizeKeyPlaceholder: String? {
        set {
            // set new value from dictionary
            DispatchQueue.main.async{
                self.placeholder = newValue?.locale
                self.textAlignment = rtlLang.contains(language ?? .en) ? .right : .left
            }
        }
        get {
            return self.placeholder
        }
    }
}
