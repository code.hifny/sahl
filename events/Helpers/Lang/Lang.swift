//
//  Lang.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//
//
//import Foundation
//
//
//enum locale {
//    case ar
//    case en
//}
//
//var langs:[locale:[String:String]] = [
//    .ar:arabicArray,
//    .en:englishArray,
//];
//
////extension String{
////    var locale: String {
////        get {
////            for (key,value) in langs {
////                if(Config.locale == key) , value[self] != nil{
////                    return value[self]!
////                }
////            }
////            return "lang.\(self)"
////        }
////        
////    }
////}
//
//@IBDesignable extension UILabel {
//    
//    @IBInspectable var localizeKey: String? {
//        set {
//            self.text = newValue?.locale
//        }
//        get {
//            return self.text
//        }
//    }
//}
//
//@IBDesignable extension UIButton {
//    
//    @IBInspectable var localizeKey: String? {
//        set {
//            self.setTitle(newValue?.locale, for: .normal)
//        }
//        get {
//            return self.titleLabel?.text
//        }
//    }
//}
//
//@IBDesignable extension UITextView {
//    
//    @IBInspectable var localizeKey: String? {
//        set {
//            self.text = newValue?.locale
//        }
//        get {
//            return self.text
//        }
//    }
//}
//
//@IBDesignable extension UITextField {
//    
////    @IBInspectable var placeholderLocalizeKey: String? {
////        set {
////            self.placeholder = newValue?.locale
////        }
////        get {
////            return self.placeholder
////        }
////    }
//    @IBInspectable var localizeKey: String? {
//        set {
//            self.placeholder = newValue?.locale
//        }
//        get {
//            return self.placeholder
//        }
//    }
//}
//
//@IBDesignable extension UINavigationItem {
//    
//    @IBInspectable var localizeKey: String? {
//        set {
//            self.title = newValue?.locale
//        }
//        get {
//            return self.title
//        }
//    }
//}

