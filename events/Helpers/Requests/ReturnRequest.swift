//
//  ReturnRequest.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ReturnRequest :NSObject{
    /// Returns the associated value of the result if it is a success, `nil` otherwise.
    var data: JSON?
    /// Returns the associated error value if the result if it is a failure, `nil` otherwise.
    var haveError: Bool = false
    var error: String = "Connection Error".locale
    /// Returns the associated unauthenticated status if the result if it is a failure, `nil` otherwise.
    var unauthenticated: Bool?
    
    init(_ data:JSON = JSON.null ,haveError:Bool = false, error:String = "Connection Error".locale , unauthenticated : Bool = false) {
        self.data = data
        self.haveError = haveError
        self.error = error
        self.unauthenticated = unauthenticated
        if unauthenticated {
            //TODO:- return to login if unauthenticated user
            UIApplication.topViewController()?.goToLogin()
        }
    }
}

