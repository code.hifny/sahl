//
//  Requests.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Requests {
    
    static var instance: Requests {
        return Requests()
    }
    public var XApiKey:String{
        var apikey : String = "k4o8ocs8skg8os88o8k4kc0kcgosc8cwkkcc4gsc"
        if UserUtil.loadUser() != nil {
            apikey = (UserUtil.loadUser()?.api_key)!
        }
        return apikey
    }
    func request(link:String , method:HTTPMethod ,parameters:[String: Any] = [:] , completion: @escaping (ReturnRequest) -> ()) {
        Alamofire.request(link, method: method, parameters:parameters, headers: ["Accept": "application/json","X-API-KEY":  "k4o8ocs8skg8os88o8k4kc0kcgosc8cwkkcc4gsc"]).responseJSON { response in
            self.result(response, completion: { (data) in
                completion(data)
            })
        }
    }
    
    func requestMultipart(link:String , method:HTTPMethod ,parameters:[String: Any] = [:] , completion: @escaping (ReturnRequest) -> ()) {
        Alamofire.upload(multipartFormData: { multipartFormData in
            // parametiers
            parameters.forEach({ (key,value) in
                if key == "image"{
                    multipartFormData.append((UIImageJPEGRepresentation(value as! UIImage, 1.0) as Data?)!, withName: "image", fileName: "UserImage.jpg", mimeType: "image/jpg")
                }else{
                    multipartFormData.append("\(value)".data(using: .utf8)!, withName: key)
                }
            })
        }, to: link, method: .post , headers: ["Accept": "application/json" ], encodingCompletion: { result in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress { progress in // main queue by default
                    // Start the upload
                    print("Upload Progress: \(progress.fractionCompleted)")
                }
                upload.responseJSON { response in
                    self.result(response, completion: { (data) in
                        completion(data)
                    })
                }
            case .failure(let encodingError):
                completion(ReturnRequest(haveError : true, error : encodingError.localizedDescription))
                break
            }
        })
        
    }
    
    func result(_ response: DataResponse<Any>, completion: @escaping (ReturnRequest) -> ()){
        print(response)
        print(response.result)

        print(completion)
        switch response.result {
        case .success:
            if let data = response.result.value{
                print(data)
                let json = JSON(data)
                print(json)
                if json["error"].string == "Invalid API key "{
                    completion(ReturnRequest(unauthenticated : true))
                }else{
                    completion(ReturnRequest(json))
                }
            }else{
                completion(ReturnRequest(haveError : true))
            }
        case .failure(let encodingError):
            print(encodingError)
            completion(ReturnRequest(haveError : true, error : encodingError.localizedDescription))
            break
        }
    }
    
}
