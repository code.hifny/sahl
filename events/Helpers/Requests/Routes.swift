//
//  Routes.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation

/*
 *  Routes to return Links
 */

extension String {
    var route: String {
        get {
            if let link = linksArray[self]{
                return Config.apiUrl + link
            }else{
                return Config.baseUrl
            }
        }
        
    }
}

