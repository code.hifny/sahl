//
//  helper.swift
//  TwitterApp
//
//  Created by Mina Gad on 2/3/18.
//  Copyright © 2018 Mina Gad. All rights reserved.
//

import UIKit

class helper: NSObject {
    
//        // help for calculate text view size based on Text size........
    class func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 1000, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], context: nil)
    }
    
    
    private func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], context: nil)
    }
    
    // state that observe if user on chat or people
    static var chat_tap = "people"
    
    // very important.......... add observer on spacific view controller for Generics
    /// network stuff here we add observer and simple status function on UIViewController Class to alert user that network status changed
    static var connected: Bool = false // for sending requests to server.........
    class func addNetworkObserver(on viewController: UIViewController) {
        NotificationCenter.default.addObserver(viewController, selector: #selector(viewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        Reach().monitorReachabilityChanges()
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            connected = false
        case .online(.wwan):
            connected = true
        case .online(.wiFi):
            connected = true
        }
    }
    
    // remove from  viewcontroller when it disappear from window for memory leaks.............
    class func removeNetworkObserver(from viewController: UIViewController) {
        NotificationCenter.default.removeObserver(viewController, name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
    }
    ////////////////// finish network Stuffffffff
}

