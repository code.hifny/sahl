//
//  Constants.swift
//  Events
//
//  Created by abdelrahman on 1/30/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import UIKit
import EasyLocalization
//MARK:- Base configrations
enum Config
{
    static let baseUrl = "http://localhost/sahl"
    static let locale:locale = EasyLocalization.getLanguage()
    static let apiUrl = baseUrl + "/\(locale)/api/"
    static let imageUrl = "http://localhost/sahl/assets/uploads/files/"
    static let present_phone_activation_code = "present_phone_activation_code"
    static let PeopleCell = "PeopleCell"
    static let ChatCell = "ChatCell"
    static let view_profile = "view_profile"
    static let open_message = "open_message"
    static let ChatMessageCell = "ChatMessageCell"
    static let myMessageColor = UIColor.lightGray
    static var user_id: Int = 0
}

enum Color {
    static let grayColor =  UIColor.hex("f2f3f5")
    static let darkBlueColor =  UIColor.hex("221c4a")
    
    static let greenColor =  UIColor.hex("ff363e")//14cc9d
    static let blueColor = UIColor(hex:"692569")//027ac1
    static let blakColor = UIColor(hex:"c1212323")

}
