//
//  AppDelegate.swift
//  events
//
//  Created by abdelrahman on 2/8/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import UserNotifications
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import EasyLocalization
import Google
import GoogleSignIn
import FBSDKLoginKit
import SwiftyJSON
import UserNotifications
import Firebase
import FirebaseInstanceID

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        FIRApp.configure()
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
//            FIRMessaging.messaging().remoteMessageDelegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in
            })
        }else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            application.registerUserNotificationSettings(settings)
        }
        
        if UserDefaults.standard.integer(forKey: "selectedNotifiy") == 1 {
            application.registerForRemoteNotifications()
        }else{
            application.unregisterForRemoteNotifications()
        }
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        EasyLocalization.config(
            locale: .en,
            languageDictionary: [
                .ar : arabicArray,
                .en : englishArray,
                ]
        )

        //        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotificaiton),
        //            name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        IQKeyboardManager.shared.enable = true
        checkLogin()
        //        print("mobile_token",)÷
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.hexString
        
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
        FIRInstanceID.instanceID().token()
        // Print it to console
//        print("FIRInstanceID device token: \(FIRInstanceID.instanceID().token())")
        print("APNs device token: \(deviceTokenString)")
        print("APNs device token: \(FIRInstanceID.instanceID().token() ?? "")")
        // UserData.string(forKey: "UserDeviceToken")
//        UserDefaults.standard.setValue("\(FIRInstanceID.instanceID().token() ?? "")", forKey: "mobile_token")
        UserDefaults.standard.synchronize()

//        print("mobile_token====>" , UserDefaults.standard.value(forKey: "mobile_token"))
        // Persist it in your backend in case it's new
    }
    //    @objc func tokenRefreshNotificaiton(notification: NSNotification) {
    //        let refreshedToken = FIRInstanceID.instanceID().token()!
    //        print("InstanceID token: \(refreshedToken)")
    //        // Connect to FCM since connection may have failed when attempted before having a token.
    //        connectToFcm()
    //    }
    //    // [END refresh_token]
    //
    //    // [START connect_to_fcm]
    //    func connectToFcm() {
    //        FIRMessaging.messaging().connect { (error) in
    //            if (error != nil) {
    //                print("Unable to connect with FCM. \(error)")
    //            } else {
    //                print("Connected to FCM.")
    //            }
    //        }
    //    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    func checkLogin() {
        let vc =
            //UserUtil.loadUser() != nil && UserUtil.loadUser()?.activation == 1 && UserUtil.loadUser()?.objectives?.count != 0 ? UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "   ") as! HomeTabBarController :
            UIStoryboard(name: "Guide", bundle: nil).instantiateViewController(withIdentifier: "GuideViewController") as! GuideViewController
            // UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "LoginTableViewController") as! LoginTableViewController
        
        let navigationController = UserUtil.loadUser() != nil ? vc : EventsNavigationController(rootViewController: vc)
        
        let transition = CATransition()
        transition.duration = 0.5
        transition.type = kCATransitionFade
        
        window?.layer.add(transition, forKey: kCATransition)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    func prepareAppFont(_ name:String) {
        UILabel.appearance().substituteFontName = name
        UIButton.appearance().substituteFontName = name
        UITextView.appearance().substituteFontName = name
        UITextField.appearance().substituteFontName = name
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        // Saves changes in the application's managed object context before the application terminates.
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        completionHandler(.newData) // indicates that new data was successfully fetched
    }
    
    
//    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
//        print("recive remote Message now ................")
//    }
    
}
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Notification being triggered")
        if UserUtil.loadUser() != nil {
            completionHandler( [.alert,.sound,.badge])
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if UserUtil.loadUser() != nil {
            let data = response.notification.request.content.userInfo
            let allData = data as! Dictionary<String, AnyObject>
            print("P1:",allData);
//            print(type)
//            if type == "3" {
//                let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                //let rootVC = storyboard.instantiateViewController(withIdentifier: "HomeTabBarController") as! HomeTabBarController
//                    rootVC.selectedIndex = 3
//                self.window!.rootViewController = rootVC
//            }else if type  == "2" || type == "1" || type == "4" {
//                //let rootVC = UIStoryboard(name: "People", bundle: nil).instantiateViewController(withIdentifier: "MainTableViewController") as! MainTableViewController
//                rootVC.pageType = "appDelegete"
//                let navigationController = EventsNavigationController(rootViewController: rootVC)
//
//                self.window!.rootViewController = navigationController
//            }
            
        }
    }
    
}
extension Data {
    var hexString: String {
        return map { String(format: "%02.2hhx", arguments: [$0]) }.joined()
    }
}


