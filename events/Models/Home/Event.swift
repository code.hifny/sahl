//
//  Event.swift
//  events
//
//  Created by abdelrahman on 2/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import Realm
import RealmSwift

class Event: Object {

    var id = RealmOptional<Int>()
    var going_number = RealmOptional<Int>()


    @objc dynamic var title:String?
    @objc dynamic var info:String?
    @objc dynamic var address:String?
    @objc dynamic var latitude:String?
    @objc dynamic var longitude:String?
    @objc dynamic var date_from:String?
    @objc dynamic var date_to:String?
    @objc dynamic var time_from:String?
    @objc dynamic var time_to:String?
    @objc dynamic var created_at:String?
    @objc dynamic var updated_at:String?
    @objc dynamic var formatted_date:String?
    @objc dynamic var formatted_time:String?
//    var news:[News]?
//    var going:[User]?
    let news = List<News>()
    let going = List<UserCash>()

    required  init(_ data:JSON) {
        super.init()
        self.id = RealmOptional(data["id"].int)
        self.going_number = RealmOptional(data["going_number"].int)
        self.title = data["DataTitle".locale].string
        self.info = data["DataInfo".locale].string
        self.address = data["DataAddress".locale].string
        self.latitude = data["latitude"].string != "" ? data["latitude"].string : "0"
        self.longitude = data["longitude"].string != "" ? data["longitude"].string : "0"
        self.date_from = data["date_from"].string
        self.date_to = data["date_to"].string
        self.time_from = data["time_from"].string
        self.time_to = data["time_to"].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
        self.formatted_date = data["formatted_date"].string
        self.formatted_time = data["formatted_time"].string
        
        data["news"].forEach({ (item) in
           self.news.append(News(item.1))
        })
            
        data["going"].forEach({ (item) in
            self.going.append(UserCash(item.1))
        })
    
    }
 
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value:value , schema:schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm , schema:schema)
    }
}
