//
//  News.swift
//  events
//
//  Created by abdelrahman on 2/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import Realm
import RealmSwift

class News: Object {
    
    var id = RealmOptional<Int>()
    @objc dynamic var title:String?
    @objc dynamic var descrip:String?
    @objc dynamic var image:String?
    var event_id = RealmOptional<Int>()
    @objc dynamic var created_at:String?
    @objc dynamic var updated_at:String?
    @objc dynamic var formatted_date:String?

    

    required  init(_ data:JSON) {
        super.init()
        self.id = RealmOptional(data["id"].int)
        self.title = data["DataTitle".locale].string
        self.descrip = data["DataDescription".locale].string
        self.image = data["image"].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
        self.formatted_date = data["formatted_date"].string

    }

    
    required init() {
        super.init()
    }
    
    required init(value: Any, schema: RLMSchema) {
        super.init(value:value , schema:schema)
    }
    
    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm:realm , schema:schema)
    }

}
