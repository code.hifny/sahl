//
//  EventTest.swift
//  events
//
//  Created by abdelrahman on 2/26/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//


import Foundation
import SwiftyJSON

class EventTest:  NSObject, NSCoding {
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("agenda")
    
    var id:Int?
    var title:String?
    var info:String?
    var address:String?
    var latitude:String?
    var longitude:String?
    var date_from:String?
    var date_to:String?
    var time_from:String?
    var time_to:String?
    var created_at:String?
    var updated_at:String?
    var formatted_date:String?
    var formatted_time:String?
    var news:[News]?
    var going:[User]?
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.title = data["DataTitle".locale].string
        self.info = data["DataInfo".locale].string
        self.address = data["DataAddress".locale].string
        self.latitude = data["latitude"].string != "" ? data["latitude"].string : "0"
        self.longitude = data["longitude"].string != "" ? data["longitude"].string : "0"
        self.date_from = data["date_from"].string
        self.date_to = data["date_to"].string
        self.time_from = data["time_from"].string
        self.time_to = data["time_to"].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
        self.formatted_date = data["formatted_date"].string
        self.formatted_time = data["formatted_time"].string
//        self.news = data["news"].map{News($0.1)}
//        self.going = data["news"].map{User($0.1)}
//
        
    }
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        title = aDecoder.decodeObject(forKey: "title") as? String
        info = aDecoder.decodeObject(forKey: "info") as? String
        address = aDecoder.decodeObject(forKey: "address") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        date_from = aDecoder.decodeObject(forKey: "date_from") as? String
        date_to = aDecoder.decodeObject(forKey: "date_to") as? String
        time_from = aDecoder.decodeObject(forKey: "time_from") as? String
        time_to = aDecoder.decodeObject(forKey: "time_to") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        formatted_date = aDecoder.decodeObject(forKey: "formatted_date") as? String
        formatted_time = aDecoder.decodeObject(forKey: "formatted_time") as? String
        news = aDecoder.decodeObject(forKey: "news") as? [News]
        going = aDecoder.decodeObject(forKey: "going") as? [User]
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(info, forKey: "info")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(date_from, forKey: "date_from")
        aCoder.encode(date_to, forKey: "date_to")
        aCoder.encode(time_from, forKey: "time_from")
        aCoder.encode(time_to, forKey: "time_to")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        aCoder.encode(formatted_date, forKey: "formatted_date")
        aCoder.encode(formatted_time, forKey: "formatted_time")
        aCoder.encode(news, forKey: "news")
        aCoder.encode(going, forKey: "going")
        
        
        
    }
    
}
//class EventUtil {
//    
//    static func save(_ item: Event?) {
//        if item != nil {
//            NSKeyedArchiver.archiveRootObject(item!, toFile: Event.ArchiveURL.path)
//        }
//    }
//    
//    static func load() -> Event? {
//        return NSKeyedUnarchiver.unarchiveObject(withFile: Event.ArchiveURL.path) as? Event
//    }
//}

