//
//  Notifications.swift
//  events
//
//  Created by Intcore on 3/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON

class Notifications: NSObject, NSCoding {
    
    var sessionId:String?
    var sessionTitle:String?
    var sessionDescrip:String?
    var created_at:String?
    var type:String?
    var minutesType:String?
    var user_id:Int?
    var status:Bool?
    var read_at:String?
    var title_en:String?
    var title_ar:String?
    var message_en:String?
    var message_ar:String?

    var notifiable_id:Int?
    var notifications_id:String?
    var sessionDateString:String?

    required  init(_ data:JSON , type: Int = 0) {
        super.init()
        
        if type == 1 {
            self.title_en = data["title_en"].string
            self.title_ar = data["title_ar"].string
            self.message_en = data["message_en"].string
            self.message_ar = data["message_ar"].string
            self.user_id = data["data"]["user_id"].int
            self.sessionTitle = data["data"]["DataTitle".locale].string
            self.sessionDescrip = data["data"]["DataMessage".locale].string
            self.type = "\(data["data"]["type"].int ?? 0)"
            self.created_at = data["created_at"].string
            self.status = data["status"].int == 0 ? false : true
            self.read_at = data["read_at"].string
            self.notifications_id = data["id"].string
            self.notifiable_id = data["notifiable_id"].int
        }else{
            self.sessionId = data["sessionId"].string
            self.sessionTitle = data["sessionTitle"].string
            self.sessionDescrip = data["sessionDescrip"].string
            self.type = data["type"].string
            self.created_at = data["created_at"].string
            self.minutesType = data["minutesType"].string
            self.status = data["status"].int == 0 ? false : true
            self.sessionDateString = data["startSession"].string
        }
        



    }
    
    
    required init(coder aDecoder: NSCoder) {
        notifications_id = aDecoder.decodeObject(forKey: "notifications_id") as? String
        title_ar = aDecoder.decodeObject(forKey: "title_ar") as? String
        title_en = aDecoder.decodeObject(forKey: "title_en") as? String
        message_en = aDecoder.decodeObject(forKey: "message_en") as? String
        message_ar = aDecoder.decodeObject(forKey: "message_ar") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        sessionTitle = aDecoder.decodeObject(forKey: "DataTitle") as? String
        sessionDescrip = aDecoder.decodeObject(forKey: "sessionDescrip") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(notifications_id, forKey: "notifications_id")
        aCoder.encode(title_ar, forKey: "title_ar")
        aCoder.encode(title_en, forKey: "title_en")
        aCoder.encode(message_en, forKey: "message_en")
        aCoder.encode(message_ar, forKey: "message_ar")
        aCoder.encode(sessionTitle, forKey: "DataTitle")
        aCoder.encode(sessionDescrip, forKey: "sessionDescrip")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(type, forKey: "type")
    }
}
