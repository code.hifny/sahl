//
//  about.swift
//  events
//
//  Created by Intcore on 3/27/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import EasyLocalization

class about: NSObject , NSCoding {
    
    var id:Int?
    var content_ar:String?
    var content_en:String?
    var content:String?
    var facebook:String?
    var twitter:String?
    var youtube:String?
    var created_at:String?
    var updated_at:String?
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.content_ar = data["content_ar"].string
        self.content_en = data["content_en"].string
        self.content = EasyLocalization.getLanguage() == .en ?  data["content_en"].string :  data["content_ar"].string
        self.facebook = data["facebook"].string
        self.twitter = data["twitter"].string
        self.youtube = data["youtube"].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
        
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        content_ar = aDecoder.decodeObject(forKey: "content_ar") as? String
        content_en = aDecoder.decodeObject(forKey: "content_en") as? String
        content = aDecoder.decodeObject(forKey: "DataContent") as? String
        facebook = aDecoder.decodeObject(forKey: "facebook") as? String
        twitter = aDecoder.decodeObject(forKey: "twitter") as? String
        youtube = aDecoder.decodeObject(forKey: "youtube") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(content_ar, forKey: "content_ar")
        aCoder.encode(content_en, forKey: "content_en")
        aCoder.encode(content, forKey: "DataContent")
        aCoder.encode(facebook, forKey: "facebook")
        aCoder.encode(twitter, forKey: "twitter")
        aCoder.encode(youtube, forKey: "youtube")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        
    }
    
}
