//
//  Material.swift
//  events
//
//  Created by Intcore on 3/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import EasyLocalization
class Material: NSObject, NSCoding {
    var id:Int?
    var title_ar:String?
    var title_en:String?
    var materialTitle:String?
    var description_en:String?
    var description_ar:String?
    var materialDescription:String?
    var file:String?
    var created_at:String?
    var updated_at:String?
    var elementDownload:Bool = false

    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.title_ar = data["title_ar"].string
        self.title_en = data["title_en"].string
        self.materialTitle =  data["DataTitle".locale].string
        self.description_en = data["description_en"].string
        self.description_ar = data["description_ar"].string
        self.materialDescription = data["DataDescription".locale].string
        self.file = data["file"].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
    }
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        title_ar = aDecoder.decodeObject(forKey: "title_ar") as? String
        title_en = aDecoder.decodeObject(forKey: "title_en") as? String
        materialTitle = aDecoder.decodeObject(forKey: "materialTitle") as? String
        description_en = aDecoder.decodeObject(forKey: "description_en") as? String
        description_ar = aDecoder.decodeObject(forKey: "description_ar") as? String
        materialDescription = aDecoder.decodeObject(forKey: "materialDescription") as? String
        file = aDecoder.decodeObject(forKey: "file") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title_ar, forKey: "title_ar")
        aCoder.encode(title_en, forKey: "title_en")
        aCoder.encode(materialTitle, forKey: "materialTitle")
        aCoder.encode(description_en, forKey: "description_en")
        aCoder.encode(description_ar, forKey: "description_ar")
        aCoder.encode(materialDescription, forKey: "materialDescription")
        aCoder.encode(file, forKey: "file")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        
    }
    
    
}
