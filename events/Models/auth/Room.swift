//
//  Messageable.swift
//  events
//
//  Created by Mina Gad on 2/21/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON

class Room: NSObject, NSCoding
{
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(user_id_1, forKey: "user_id_1")
        aCoder.encode(user_id_2, forKey: "user_id_2")
        aCoder.encode(status, forKey: "status")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        aCoder.encode(date, forKey: "date")
        aCoder.encode(last_message, forKey: "last_message")
        aCoder.encode(user, forKey: "user")
        aCoder.encode(messages, forKey: "messages")
        aCoder.encode(seen, forKey: "seen")
        aCoder.encode(last_message_time, forKey: "last_message_time")
    }

    required init?(coder aDecoder: NSCoder)
    {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        user_id_1 = aDecoder.decodeObject(forKey: "user_id_1") as? Int
        user_id_2 = aDecoder.decodeObject(forKey: "user_id_2") as? Int
        status = aDecoder.decodeObject(forKey: "status") as? Int
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        date = aDecoder.decodeObject(forKey: "date") as? String
        last_message = aDecoder.decodeObject(forKey: "last_message") as? String
        user = aDecoder.decodeObject(forKey: "user") as? User
        messages = aDecoder.decodeObject(forKey: "messages") as? [Message]
        seen = aDecoder.decodeObject(forKey: "seen") as? Int
        last_message_time = aDecoder.decodeObject(forKey: "last_message_time") as? String
    }
    
    var id: Int?
    var user_id_1: Int?
    var user_id_2: Int?
    var status: Int?
    var created_at: String?
    var updated_at: String?
    var date: String?
    var last_message: String?
    var user: User?
    var messages: [Message]?
    var seen: Int?
    var last_message_time: String?
    init?(_ data: JSON, user: User?, messages: [Message]? = nil) {
        self.id = data["id"].int
        self.user_id_1 = data["user_id_1"].int
        self.user_id_2 = data["user_id_2"].int
        self.status = data["status"].int
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
        self.date = data["date"].string
        self.last_message = data["last_message"].string
        self.user = user
        self.messages = messages
        self.seen = data["seen"].int
        self.last_message_time = data["last_message_time"].string
    }
    
}
