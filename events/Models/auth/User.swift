//
//  User.swift
//  Events
//
//  Created by abdelrahman on 1/31/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON
import CoreData


class User: NSObject, NSCoding {
    var username: String?
    var email: String?
    var phone: String?
    var image: String?
    var api_key: String?

    
    
    required  init(_ data: JSON) {
        self.username = data["username"].string
        self.email = data["email"].string
        self.phone = data["phone"].string
        self.image = data["image"].string
        self.api_key = data["api_key"].string
    }
    
    required init(coder aDecoder: NSCoder) {
        username = aDecoder.decodeObject(forKey: "username") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        api_key = aDecoder.decodeObject(forKey: "api_key") as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(username, forKey: "username")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(api_key, forKey: "api_key")
    }
    
    
}

