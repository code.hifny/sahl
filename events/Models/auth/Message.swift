//
//  Messages.swift
//  events
//
//  Created by Mina Gad on 2/24/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON

class Message: NSObject, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(chat_room_id, forKey: "chat_room_id")
        aCoder.encode(sender_id, forKey: "sender_id")
        aCoder.encode(message, forKey: "message")
        aCoder.encode(seen, forKey: "seen")
        aCoder.encode(date, forKey: "date")
        aCoder.encode(sender, forKey: "sender")
    }
    
    required init?(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        chat_room_id = aDecoder.decodeObject(forKey: "chat_room_id") as? Int
        sender_id = aDecoder.decodeObject(forKey: "sender_id") as? Int
        message = aDecoder.decodeObject(forKey: "message") as? String
        seen = aDecoder .decodeObject(forKey: "date") as? Int
        date = aDecoder.decodeObject(forKey: "date") as? String
        sender = aDecoder.decodeObject(forKey: "sender") as? User
    }
    
    var id: Int?
    var chat_room_id: Int?
    var sender_id: Int?
    var message: String?
    var seen: Int?
    var date: String?
    var sender: User?
    
    init?(_ data: JSON, sender: User? = nil) {
        self.id = data["id"].int
        self.chat_room_id = data["chat_room_id"].int
        self.sender_id = data["sender_id"].int
        self.message = data["message"].string
        self.seen = data["seen"].int
        self.date = data["date"].string
        self.sender =  User(data["sender"])
    }
    
}
