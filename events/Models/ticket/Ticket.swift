//
//  Ticket.swift
//  events
//
//  Created by Macbook Pro on 2/24/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON


class Ticket: NSObject , NSCoding {
   
    var id:Int?
    var user_id:Int?
    var package_id:Int?
    var promo_code:String?
    var ticket_image:String?
    var created_at:String?
    var updated_at:String?
    var latitude:String?
    var longitude:String?
    var date:String?
    var addressEn:String?
    var addressAr:String?
    var package:Packages?
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.user_id = data["user_id"].int
        self.package_id = data["package_id"].int
        self.promo_code = data["promo_code"].string
        self.ticket_image = data["ticket"].string
        self.created_at = data["created_at"].string
        self.date = data["date"].string
        self.updated_at = data["updated_at"].string
        self.latitude = data["event_latitude"].string
        self.longitude = data["event_longitude"].string
        self.addressEn = data["event_address_en"].string
        self.addressAr = data["event_address_ar"].string
        self.longitude = data["event_longitude"].string != "" ? data["event_longitude"].string : "0"
        self.latitude = data["event_latitude"].string != "" ? data["event_latitude"].string : "0"

        self.package = Packages(data["package"])
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        user_id = aDecoder.decodeObject(forKey: "user_id") as? Int
        package_id = aDecoder.decodeObject(forKey: "package_id") as? Int
        ticket_image = aDecoder.decodeObject(forKey: "ticket_image") as? String
        promo_code = aDecoder.decodeObject(forKey: "promo_code") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        date = aDecoder.decodeObject(forKey: "date") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "longitude") as? String
        addressEn = aDecoder.decodeObject(forKey: "event_address_en") as? String
        addressAr = aDecoder.decodeObject(forKey: "event_address_ar") as? String
        latitude = aDecoder.decodeObject(forKey: "event_latitude") as? String
        longitude = aDecoder.decodeObject(forKey: "event_longitude") as? String
        package = aDecoder.decodeObject(forKey: "package") as? Packages

        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(user_id, forKey: "user_id")
        aCoder.encode(package_id, forKey: "package_id")
        aCoder.encode(promo_code, forKey: "promo_code")
        aCoder.encode(ticket_image, forKey: "ticket_image")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        aCoder.encode(date, forKey: "date")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(addressEn, forKey: "event_address_en")
        aCoder.encode(addressAr, forKey: "event_address_ar")
        aCoder.encode(latitude, forKey: "event_latitude")
        aCoder.encode(longitude, forKey: "event_longitude")
        aCoder.encode(package, forKey: "package")

    }

}
class TicketUtil {
    private static let TicketKey = "Data"
    
    private static func archiveUser(_ ticket :Ticket) -> NSData {
        return NSKeyedArchiver.archivedData(withRootObject: ticket as Ticket) as NSData
    }
    static func loadTicket() -> Ticket? {
        if let unarchivedObject = UserDefaults.standard.object(forKey: TicketKey) as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: unarchivedObject as Data) as? Ticket
        }
        return nil
    }
    
    static func saveTicket(_ ticket : Ticket?) {
        let archivedObject = archiveUser(ticket!)
        UserDefaults.standard.set(archivedObject, forKey: TicketKey)
        UserDefaults.standard.synchronize()
    }
}
