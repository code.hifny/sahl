//
//  AgendaItem.swift
//  events
//
//  Created by abdelrahman on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON

class AgendaItem: NSObject , NSCoding {
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("agenda")
    
    var date:String?
    var agenda:[Agenda]?
    
    required  init(_ date:String ,_ agenda:JSON) {
        super.init()
        self.date = date
        self.agenda = agenda.map{Agenda($0.1)}
    }
    
    required init(coder aDecoder: NSCoder) {
        date = aDecoder.decodeObject(forKey: "date") as? String
        agenda = aDecoder.decodeObject(forKey: "agenda") as? [Agenda]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(date, forKey: "date")
        aCoder.encode(agenda, forKey: "agenda")
    }
}

