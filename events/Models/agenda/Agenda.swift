//
//  Agenda.swift
//  events
//
//  Created by abdelrahman on 2/21/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import Foundation
import SwiftyJSON

class Agenda: NSObject  , NSCoding {
    
    var id:Int?
    var date:String?
    var time_from:String?
    var time_to:String?
    var title:String?
    var info:String?
    var location:String?
    var speaker_id:Int?
    var session_rate:Int?
    var created_at:String?
    var updated_at:String?
    var speaker:Speakers?
    var formatted_time_from:String?
    var formatted_time_to:String?
    var is_reminded:Bool?
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.date = data["date"].string
        self.time_from = data["time_from"].string
        self.time_to = data["time_to"].string
        self.title = data["DataTitle".locale].string
        self.info = data["DataInfo".locale].string
        self.location = data["location"].string
        self.speaker_id = data["speaker_id"].int
        self.session_rate = data["session_rate"].int
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
        self.speaker = Speakers(data["speaker"])
        self.formatted_time_from = data["formatted_time_from"].string
        self.formatted_time_to = data["formatted_time_to"].string
        self.is_reminded = data["is_reminded"].int == 0 ? false : true
        
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        date = aDecoder.decodeObject(forKey: "date") as? String
        time_from = aDecoder.decodeObject(forKey: "time_from") as? String
        time_to = aDecoder.decodeObject(forKey: "time_to") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        info = aDecoder.decodeObject(forKey: "info") as? String
        location = aDecoder.decodeObject(forKey: "location") as? String
        speaker_id = aDecoder.decodeObject(forKey: "speaker_id") as? Int
        session_rate = aDecoder.decodeObject(forKey: "session_rate") as? Int
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        speaker = aDecoder.decodeObject(forKey: "speaker") as? Speakers
        formatted_time_from = aDecoder.decodeObject(forKey: "formatted_time_from") as? String
        formatted_time_to = aDecoder.decodeObject(forKey: "formatted_time_to") as? String
        is_reminded = aDecoder.decodeObject(forKey: "is_reminded") as? Bool
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(date, forKey: "date")
        aCoder.encode(time_from, forKey: "time_from")
        aCoder.encode(time_to, forKey: "time_to")
        aCoder.encode(title, forKey: "title")
        aCoder.encode(info, forKey: "info")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(speaker_id, forKey: "speaker_id")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(session_rate, forKey: "session_rate")
        aCoder.encode(speaker, forKey: "speaker")
        aCoder.encode(formatted_time_from, forKey: "formatted_time_from")
        aCoder.encode(formatted_time_to, forKey: "formatted_time_to")
        aCoder.encode(is_reminded, forKey: "is_reminded")
        
    }
}


