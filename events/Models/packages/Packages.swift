//
//  Packages.swift
//  events
//
//  Created by Macbook Pro on 2/24/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON

class Packages: NSObject, NSCoding {
    
    var id:Int?
    var price:Int?
    var date_from:String?
    var date_to:String?
    var name_ar:String?
    var name_en:String?
    var packageName:String?
    var created_at:String?
    var updated_at:String?
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.price = data["price"].int
        self.date_from = data["date_from"].string
        self.date_to = data["date_to"].string
        self.name_ar = data["name_ar"].string
        self.name_en = data["name_en"].string
        self.packageName = data["DataName".locale].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string
    }
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        price = aDecoder.decodeObject(forKey: "price") as? Int
        date_from = aDecoder.decodeObject(forKey: "date_from") as? String
        date_to = aDecoder.decodeObject(forKey: "date_to") as? String
        name_ar = aDecoder.decodeObject(forKey: "name_ar") as? String
        name_en = aDecoder.decodeObject(forKey: "name_en") as? String
        packageName = aDecoder.decodeObject(forKey: "packageName") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(date_from, forKey: "date_from")
        aCoder.encode(date_to, forKey: "date_to")
        aCoder.encode(name_ar, forKey: "name_ar")
        aCoder.encode(name_en, forKey: "name_en")
        aCoder.encode(packageName, forKey: "packageName")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        
    }


}
