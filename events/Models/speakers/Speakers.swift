//
//  Speakers.swift
//  events
//
//  Created by Macbook Pro on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON

class Speakers: NSObject , NSCoding {
    
    var id:Int?
    var username:String?
    var email:String?
    var phone:String?
    var company:String?
    var job_title:String?
    var activation:Int?
    var twitter:String?
    var linkedin:String?
    var type:Int?
    var social_id:Int?
    var social_type:String?
    var image:String?
    var bio:String?
    var reset_password_code:Int?
    var api_token:String?
    var updated_at:String?
    var created_at:String?
    var temp_phone:String?
    var reset_phone_code:Int?
    var messageable:String?
    var autherized_to_accept_chat:Bool?
    var is_favorited:Int?
    
    
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.username = data["username"].string
        self.email = data["email"].string
        self.phone = data["phone"].string
        self.activation = data["activation"].int
        self.type = data["type"].int
        self.image = data["image"].string
        self.reset_password_code = data["reset_password_code"].int
        self.api_token = data["api_token"].string
        self.updated_at = data["updated_at"].string
        self.created_at = data["created_at"].string
        self.temp_phone = data["temp_phone"].string
        self.reset_phone_code = data["reset_phone_code"].int
        self.company = data["company"].string
        self.job_title = data["job_title"].string
        self.twitter = data["twitter"].string
        self.linkedin = data["linkedin"].string
        self.social_id = data["social_id"].int
        self.social_type = data["social_type"].string
        self.bio = data["bio"].string
        self.messageable = data["messageable"].string
        self.autherized_to_accept_chat = data["autherized_to_accept_chat"].bool
        self.is_favorited = data["is_favorited"].int
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        username = aDecoder.decodeObject(forKey: "username") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        phone = aDecoder.decodeObject(forKey: "phone") as? String
        activation = aDecoder.decodeObject(forKey: "activation") as? Int
        type = aDecoder.decodeObject(forKey: "type") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        reset_password_code = aDecoder.decodeObject(forKey: "reset_password_code") as? Int
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        api_token = aDecoder.decodeObject(forKey: "api_token") as? String
        reset_phone_code = aDecoder.decodeObject(forKey: "reset_phone_code") as? Int
        company = aDecoder.decodeObject(forKey: "company") as? String
        job_title = aDecoder.decodeObject(forKey: "job_title") as? String
        twitter = aDecoder.decodeObject(forKey: "twitter") as? String
        linkedin = aDecoder.decodeObject(forKey: "linkedin") as? String
        social_id = aDecoder.decodeObject(forKey: "social_id") as? Int
        social_type = aDecoder.decodeObject(forKey: "social_type") as? String
        bio = aDecoder.decodeObject(forKey: "bio") as? String
        messageable = aDecoder.decodeObject(forKey: "messageable") as? String
        autherized_to_accept_chat = aDecoder.decodeObject(forKey: "autherized_to_accept_chat") as? Bool
        is_favorited = aDecoder.decodeObject(forKey: "is_favorited") as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(phone, forKey: "phone")
        aCoder.encode(activation, forKey: "activation")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(reset_password_code, forKey: "reset_password_code")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        aCoder.encode(api_token, forKey: "api_token")
        aCoder.encode(reset_phone_code, forKey: "reset_phone_code")
        aCoder.encode(company, forKey: "company")
        aCoder.encode(job_title, forKey: "job_title")
        aCoder.encode(twitter, forKey: "twitter")
        aCoder.encode(linkedin, forKey: "linkedin")
        aCoder.encode(social_id, forKey: "social_id")
        aCoder.encode(social_type, forKey: "social_type")
        aCoder.encode(bio, forKey: "bio")
        aCoder.encode(messageable, forKey: "messageable")
        aCoder.encode(autherized_to_accept_chat, forKey: "autherized_to_accept_chat")
        aCoder.encode(is_favorited, forKey: "is_favorited")
    }

    
}


