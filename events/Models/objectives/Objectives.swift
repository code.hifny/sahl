//
//  Objectives.swift
//  events
//
//  Created by Intcore on 3/25/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//


import UIKit
import SwiftyJSON
import EasyLocalization

class Objectives: NSObject , NSCoding {
    
    var id:Int?
    var title_ar:String?
    var title_en:String?
    var objectiveName:String?
    var created_at:String?
    var updated_at:String?
    var isSelected:Bool = false

    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.title_ar = data["title_ar"].string
        self.title_en = data["title_en"].string
        self.objectiveName = data["DataTitle".locale].string
        self.created_at = data["created_at"].string
        self.updated_at = data["updated_at"].string

    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        title_ar = aDecoder.decodeObject(forKey: "title_ar") as? String
        title_en = aDecoder.decodeObject(forKey: "title_en") as? String
        objectiveName = aDecoder.decodeObject(forKey: "DataTitle") as? String
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(title_ar, forKey: "title_ar")
        aCoder.encode(title_en, forKey: "title_en")
        aCoder.encode(objectiveName, forKey: "DataTitle")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        
    }
    
}
