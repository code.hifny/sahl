//
//  Sponsers.swift
//  events
//
//  Created by Macbook Pro on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import SwiftyJSON


class Sponsers: NSObject , NSCoding {
    var id:Int?
    var image:String?
    var category_name_ar:String?
    var category_name_en:String?
    var categoryName:String?
    var type:Int?
    var category_id:Int?
    var updated_at:String?
    var created_at:String?
    
    required  init(_ data:JSON) {
        super.init()
        self.id = data["id"].int
        self.category_name_ar = data["category_name_ar"].string
        self.category_name_en = data["category_name_en"].string
        self.categoryName = data["DataCategoryName".locale].string
        self.category_id = data["category_id"].int
        self.type = data["type"].int
        self.image = data["image"].string
        self.updated_at = data["updated_at"].string
        self.created_at = data["created_at"].string
    }
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: "id") as? Int
        category_name_ar = aDecoder.decodeObject(forKey: "category_name_ar") as? String
        category_name_en = aDecoder.decodeObject(forKey: "category_name_en") as? String
        categoryName = aDecoder.decodeObject(forKey: "DataCategoryName") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
        type = aDecoder.decodeObject(forKey: "type") as? Int
        category_id = aDecoder.decodeObject(forKey: "category_id") as? Int
        created_at = aDecoder.decodeObject(forKey: "created_at") as? String
        updated_at = aDecoder.decodeObject(forKey: "updated_at") as? String
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(category_name_ar, forKey: "category_name_ar")
        aCoder.encode(category_name_en, forKey: "category_name_en")
        aCoder.encode(categoryName, forKey: "DataCategoryName")
        aCoder.encode(type, forKey: "type")
        aCoder.encode(category_id, forKey: "category_id")
        aCoder.encode(image, forKey: "image")
        aCoder.encode(created_at, forKey: "created_at")
        aCoder.encode(updated_at, forKey: "updated_at")
        
    }
}

class SponsersItem: NSObject , NSCoding {
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("sponsers")
    
    var mainSponser:Sponsers?
    var sponsers:[Sponsers]?
    
    required  init(_ data:JSON) {
        super.init()
        self.mainSponser = Sponsers(data["main_sponsor"])
        self.sponsers = data["other_sponsors"].map{Sponsers($0.1)}
    }
    required init(coder aDecoder: NSCoder) {
        mainSponser = aDecoder.decodeObject(forKey: "main_sponsor") as? Sponsers
        sponsers = aDecoder.decodeObject(forKey: "other_sponsors") as? [Sponsers]
        
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(mainSponser, forKey: "main_sponsor")
        aCoder.encode(sponsers, forKey: "other_sponsors")
    }
    
}

class SponsersUtil {
    
    static func save(_ item: SponsersItem?) {
        if item != nil {
           NSKeyedArchiver.archiveRootObject(item!, toFile: SponsersItem.ArchiveURL.path)
        }
    }
    
    static func load() -> SponsersItem? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: SponsersItem.ArchiveURL.path) as? SponsersItem
    }
}

