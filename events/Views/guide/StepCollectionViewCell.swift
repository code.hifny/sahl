//
//  StepCollectionViewCell.swift
//  Sahl
//
//  Created by code on 11/8/18.
//  Copyright © 2018 MR code. All rights reserved.
//

import UIKit

class StepCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var StepImage: UIImageView!
 
    @IBOutlet weak var StepText: UILabel!
    
    @IBOutlet weak var StepDescription: UILabel!
    
    @IBOutlet var SkipBtn: UIButton!
}
