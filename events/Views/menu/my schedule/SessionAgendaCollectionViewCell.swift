//
//  SessionAgendaCollectionViewCell.swift
//  events
//
//  Created by Intcore on 3/12/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import UserNotifications

class SessionAgendaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet open var avatar: UIImageView!
    @IBOutlet open var timeLabel: UILabel!
    @IBOutlet open var nameLabel: UILabel!
    @IBOutlet open var sessionNameLabel: UILabel!
    @IBOutlet open var notificationBtn: UIButton!
    @IBOutlet open var statusView: UIView!
    
//    var type: String = "MyAgenda"
//    open override func awakeFromNib() {
//        super.awakeFromNib()
//        localSessionsNotify()
//        if UserUtil.loadUser() == nil {
//            notificationBtn.isHidden = true
//        }
//        let triggerDate = Date()
//        let date = Calendar.current.date(byAdding: .second, value: 10, to: triggerDate)
//        print("triggerDate1",date)
//        NotificationsUtil.set("testNot", title: "test", body: "test body", date: date!, userInfo: ["AnyHashable" : "Any"])
//
//    }
//    
    // MARK: - Notification Setup
    
    func localSessionsNotify(){
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound , .alert]) { (dane, error) in
            if error != nil{
                print("Notification Error")
            }else{
                print("Notification Sucess")
            }
        }
    }
    
    func timeNotification(time: DateComponents , completion: @escaping (_ Success: Bool) -> ()){
        let trigger = UNCalendarNotificationTrigger(dateMatching: time, repeats: false)
        
        let content = UNMutableNotificationContent()
        content.title = "Notification"
        content.subtitle = "Done"
        content.body = "Send Notification Done"
        let request = UNNotificationRequest(identifier: "customNotification", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil{
                completion(false)
            }else{
                completion(true)
            }
        }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        avatar.layer.cornerRadius = avatar.frame.width/2
    }
    
    func config(_ data:Agenda) {
        if data.is_reminded ?? false {
            notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agendaa-notifications"), for: .normal)
        }else{
            notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agenda-notifications"), for: .normal)
        }
        
        avatar.af_setImage(withURL: URL(string: Config.imageUrl + (data.speaker?.image ?? "")! )!)
    
        let from = (data.formatted_time_from ?? "") + " "
        let to =  " " + (data.formatted_time_to ?? "")
        
        timeLabel.text = from +  "To".locale + to
        nameLabel.text = data.speaker?.username
        sessionNameLabel.text = data.title
        
        let now = Date()
        
        if NSCalendar.current.isDateInToday((data.date?.date) ?? now) || (data.date?.date) ?? now < now {
            let hours = data.time_from?.dropLast(6)
            let minutes = ("\(data.time_from?.dropLast(3) ?? "")").dropFirst(3)
            
            let sessionTime = now.dateAt(hours: Int("\(hours ?? "")")!, minutes: Int("\( minutes)")!)
            
//            dateAtBeforeFiftheenSessions(hours: Int("\(hours ?? "")")!, minutes: Int("\( minutes)")!)
//
//            dateAtBeforeFifeSessions(hours: Int("\(hours ?? "")")!, minutes: Int("\( minutes)")!)

            if sessionTime <= now {
//                timeNotification(time: 3) { (done) in
//                    if done{
//                        print("Send Notification Succesuf")
//                    }
//                }

                //myDate is earlier than Now (date and time)
                statusView.backgroundColor = Color.greenColor
            } else {
                //myDate is equal or after than Now (date and time)
                statusView.backgroundColor = Color.grayColor
            }
        }else{
            statusView.backgroundColor = Color.grayColor
        }
        
    }
}

extension SessionAgendaCollectionViewCell {
    
//    func dateAtBeforeFiftheenSessions(hours: Int, minutes: Int)
//    {
//        //get the month/day/year componentsfor today's date.
//        var date_components = DateComponents()
//
//        //Create an NSDate for the specified time today.
//        date_components.hour = hours
//        date_components.minute = -15
//        date_components.second = 0
//        print(date_components)
////        timeNotification(time: date_components) { (done) in
////            if done{
////                print("Send Notification Succesuf")
////            }
////        }
//
//    }
//
//    func dateAtBeforeFifeSessions(hours: Int, minutes: Int)
//    {
//        var date_components = DateComponents()
//        //Create an NSDate for the specified time today.
//        date_components.hour = hours
//        date_components.minute = -5
//        date_components.second = 0
//        print(date_components)
////
////        timeNotification(time: date_components) { (done) in
////            if done{
////                print("Send Notification Succesuf")
////            }
////        }
//
//    }
}
