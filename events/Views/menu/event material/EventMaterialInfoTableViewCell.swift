//
//  EventMaterialInfoTableViewCell.swift
//  events
//
//  Created by Intcore on 3/5/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import EasyLocalization
//var indexDownloadObject:Int = 0


class EventMaterialInfoTableViewCell: UITableViewCell , URLSessionDownloadDelegate{
    
    @IBOutlet weak var materialName: UILabel!
    @IBOutlet weak var descriptionOFMaterial: UITextView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var upload: SVUploader!
    @IBOutlet weak var progressLabel: UILabel!
    
    var eventMaterial: Material?
    var urlCuting: String = ""
    var typeofURL: String = ""
    var status:Bool = false
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession = URLSession(configuration: .default)
    let saveTypeOfFile = UserDefaults.standard
    let saveTitleOfFile = UserDefaults.standard


    override func awakeFromNib() {
        super.awakeFromNib()
        if let top = UIApplication.topViewController() as? MaterialTableViewController {
            helper.addNetworkObserver(on: top.self)
        }
        UIApplication.topViewController()?.uploaderConfig(Uploader:upload)
        upload.shadowColor = .clear
        upload.backgroundColor = .clear
        self.upload.isHidden = true
        upload.loadingLabel.isHidden = true

        let backgroundSessionConfiguration = URLSessionConfiguration.default
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func config(_ data: Material){
        eventMaterial = data
        materialName.text = data.materialTitle
        descriptionOFMaterial.text = data.materialDescription
        print("data.elementDownload" , data.elementDownload)
    }
    

    @IBAction func download(_ sender: UIButton) {
//        indexDownloadObject = sender.tag
        if helper.connected{
            if downloadTask != nil{
                eventMaterial?.elementDownload = false
                downloadTask.cancel()
                progressLabel.text = "Download".locale
                self.upload.isHidden = true
                downloadButton.setImage(#imageLiteral(resourceName: "event matrial-download") , for: .normal )
                eventMaterial?.elementDownload = false
            }else{
                if let top = UIApplication.topViewController() as? MaterialTableViewController {
                    top.StartLoading()
                    top.searchBar.endEditing(false)
//                    top.view.endEditing(true)
                }
                
                eventMaterial?.elementDownload = true
                downloadButton.setImage(#imageLiteral(resourceName: "cancelDownload") , for: .normal )
                let url = URL(string: "\( Config.baseUrl + (eventMaterial?.file ?? "") )")
                print(url!)
                let type = cutString(eventMaterial?.file ?? "")
                let title = eventMaterial?.title_en ?? ""
                saveTypeOfFile.set(type, forKey:"typeofFile")
                saveTitleOfFile.set(title, forKey: "titleOfFile")
                downloadTask = backgroundSession.downloadTask(with: url!)
                downloadTask.resume()
            }
        }else{
            if let top = UIApplication.topViewController() as? MaterialTableViewController {
                top.DangerAlert(message: "The Internaet Connection appears to offline" )
            }

        }
    }
    
    func cutString(_ url: String) -> String {
        print(url)
        let index = url.index(of: ".")!
        let newStr = url[index...]

        print(newStr)
        return String(newStr)
    }

    //MARK: URLSessionDownloadDelegate
    // 1
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
   
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        
        let typeoffiletodownload = saveTypeOfFile.string(forKey: "typeofFile") ?? ""
        let titleDocument = saveTitleOfFile.string(forKey: "titleOfFile") ?? ""
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(titleDocument).\(typeoffiletodownload)"))
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            if let top = UIApplication.topViewController() as? MaterialTableViewController {
                top.showFileWithPath(path: destinationURLForFile.path)
            }
        }
        else{
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
//                fileManager.copyItem(at: location, to: destinationURLForFile)
                if let top = UIApplication.topViewController() as? MaterialTableViewController {
                   top.showFileWithPath(path: destinationURLForFile.path)
                 }
            }catch{
                print("An error occurred while moving file to destination url")
            }
        }
    }
    // 2
    
     func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
        
        self.upload.isHidden = false
        var bytesStr = "\(bytesWritten)"
        bytesStr.remove(at:bytesStr.startIndex)
        let prefix = bytesStr.prefix(2)
        bytesStr = String(prefix)
        let byteFloat = Float(bytesStr)
        
        print(CGFloat(byteFloat ?? 0) , "   "  , CGFloat(totalBytesExpectedToWrite))
        let count = CGFloat(totalBytesWritten) / CGFloat(totalBytesExpectedToWrite)
        print("count" , count)
        _ = Int(count * 100)
        
        print("\(byteFloat ?? 0)%")
        progressLabel.text = "\(byteFloat ?? 0)%"
        
        upload.progress = CGFloat(byteFloat ?? 0)
        
    }

    //MARK: URLSessionTaskDelegate
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?){
         downloadTask = nil
         upload.progress = CGFloat(0)
        if (error != nil) {
            print(error!.localizedDescription)
        }else{
            eventMaterial?.elementDownload = false
            self.upload.isHidden = true
            downloadButton.setImage(#imageLiteral(resourceName: "event matrial-download") , for: .normal )
            self.progressLabel.text = "Download".locale
            print("The task finished transferring data successfully")

        }
    }
    
    
}

