//
//  ChatTableViewCell.swift
//  events
//
//  Created by Macbook Pro on 2/18/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChatCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateOFMessageLabel: UILabel!
    

    var room: Room? {
        didSet {
            if let room = room {
                if let status = room.status, let user_id_1 = room.user_id_1, let user_id_2 = room.user_id_2, let date = room.last_message_time, let seen = room.seen {
                    let last_message = room.last_message?.decode()
                    // 1- username
                    if let userName = room.user?.username {
                        userNameLabel.text = userName
                    }
//                    2 - image
                    if let imageURl = room.user?.image {
                        let fullImageURL = Config.imageUrl + imageURl
                        let url = URL(string: fullImageURL)
                        userImageView.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .flipFromRight(0.4), runImageTransitionIfCached: false, completion: nil)
                    }
                    // 3- date of message
                    dateOFMessageLabel.text = date
                    // 4 - message
                    if last_message == nil {
                        messageLabel.text = " New request "
                        messageLabel.font = UIFont.boldSystemFont(ofSize: 16)
                        messageLabel.cornerRadius = 5
                        messageLabel.layer.borderColor = UIColor.lightGray.cgColor
                        messageLabel.layer.borderWidth = 1

                    }
                    
                    if last_message != nil
                    {

                        if seen == 0
                        {
                            messageLabel.font = UIFont.boldSystemFont(ofSize: 16)
                            messageLabel.text = last_message
                            messageLabel.layer.borderWidth = 0
                            self.backgroundColor = UIColor(hex: "69246A", alpha: 0.1)
                        }
                        
                        if seen == 1 {
                            messageLabel.font = UIFont.systemFont(ofSize: 12)
                            messageLabel.text = last_message
                            messageLabel.layer.borderWidth = 0
                            self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        }
                       
                    }
                }
            }
        }
    }
    
}
