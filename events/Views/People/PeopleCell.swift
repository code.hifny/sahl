import UIKit
import Alamofire

class PeopleCell: UITableViewCell {

    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var jobTitleUser: UILabel!
    @IBOutlet weak var companyOFUser: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageUser.image = nil
        nameUser.text = nil
        jobTitleUser.text = nil
        companyOFUser.text = nil
    }
    
    var people: User? {
        didSet {
            if let image = people?.image , let url = URL(string: Config.imageUrl + image) {
                imageUser.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: UIImageView.ImageTransition.flipFromLeft(0.4), runImageTransitionIfCached: false, completion: nil)
            }
            if let userName = people?.username {
                nameUser.text = userName
            }
            if let jopname = people?.job_title
            {
                jobTitleUser.text = jopname
            }
            if let companyName = people?.company
            {
                companyOFUser.text = companyName
            }
            
        }
    }
}
