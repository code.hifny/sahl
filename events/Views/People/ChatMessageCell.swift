//
//  ChatMessageCell.swift
//  events
//
//  Created by Mina Gad on 2/24/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Material
import Alamofire

typealias closureAction = () -> (String)

class ChatMessageCell: UITableViewCell , TextViewDelegate {
    
    var labelText: closureAction?
    
    @IBOutlet weak var friend_ImageViewWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var my_ImageViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var textLeftPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var textRightPaddingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textLeftPaddingConstraint2: NSLayoutConstraint!
    @IBOutlet weak var textRightPaddingConstraint2: NSLayoutConstraint!
    
    
    @IBOutlet weak var messageTextView: PaddingLabel! {
        didSet {
            messageTextView.cornerRadius = 10
            messageTextView.clipsToBounds = true
            messageTextView.font = UIFont.systemFont(ofSize: 14)
        }
    }

    @IBOutlet weak var friend_ImageView: UIImageView! {
        didSet {
            friend_ImageView.layer.cornerRadius = 22
            friend_ImageView.clipsToBounds = true
            friend_ImageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet weak var my_ImageView: UIImageView! {
        didSet {
            my_ImageView.layer.cornerRadius = 22
            my_ImageView.clipsToBounds = true
            my_ImageView.contentMode = .scaleAspectFill
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        messageTextView.text = nil
        friend_ImageView.image = nil
        my_ImageView.image = nil
    }
    
    var message: Message? {
        didSet {
            if let message = message {
                messageTextView.text = message.message?.decode()?.trimmed
                let detectorPhoneNumber = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
                let detectorLink = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                

            
                let matches1 = detectorPhoneNumber.matches(in: message.message!, options: [], range: NSRange(location: 0, length: message.message!.utf16.count))
                
                let matches2 = detectorLink.matches(in: message.message!, options: [], range: NSRange(location: 0, length: message.message!.utf16.count))
                
                if matches1.count > 0 {
                    print(matches1.count)
                    for match in matches1 {
                        guard let range = Range(match.range, in: message.message!) else { continue }
                        let url = message.message![range]
                        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(url))
                        attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(makeCall))
                        messageTextView.addGestureRecognizer(tapGesture)
                        messageTextView.attributedText = attributeString
                    }
                }
                if matches2.count > 0 {
                    print(matches2.count)
                    for match in matches2 {
                        guard let range = Range(match.range, in: message.message!) else { continue }
                        let url = message.message![range]
                        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(url))
                        attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openSafari))
                        messageTextView.addGestureRecognizer(tapGesture)
                        messageTextView.attributedText = attributeString
                    }
                }
                
                print(message)
                let my_Id = UserUtil.loadUser()?.id ?? 0
                if message.sender_id != my_Id {
//                    my_ImageView.isHidden = true
//                    friend_ImageView.isHidden = false
                    if let imageURL = message.sender?.image, let fullImageURL = URL(string: Config.imageUrl + imageURL) {
                        friend_ImageView.af_setImage(withURL: fullImageURL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
                    }
                }
                else if message.sender_id == my_Id {
//                    my_ImageView.isHidden = false
//                    friend_ImageView.isHidden = true
                    if let imageURL = message.sender?.image, let fullImageURL = URL(string: Config.imageUrl + imageURL) {
                        my_ImageView.af_setImage(withURL: fullImageURL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
                    }
                }
            }
            
        }
    }
    
    @objc func makeCall() {
        
        
        if let message = message, let tel = message.message {
            let detectorPhoneNumber = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            
            let matches1 = detectorPhoneNumber.matches(in: tel, options: [], range: NSRange(location: 0, length: tel.utf16.count))
            
            if matches1.count > 0 {
                print(matches1.count)
                for match in matches1 {
                    guard let range = Range(match.range, in: message.message!) else { continue }
                    let t = tel[range]
                    if let url = URL(string: "tel://\(t)") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                }
            }
            
        }
    }
    
    @objc func openSafari() {
        if let message = message, let msg = message.message {
            print(msg)
            let detectorLink = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches2 = detectorLink.matches(in: msg, options: [], range: NSRange(location: 0, length: msg.utf16.count))
            if matches2.count > 0 {
                print(matches2.count)
                for match in matches2 {
                    guard let range = Range(match.range, in: msg) else { continue }
                    let url = msg[range]
                    if let url = URL(string: "http://\(url)") {
                        let status = UIApplication.shared.canOpenURL(url)
                        if  status {
                            UIApplication.shared.open(url, options: [:], completionHandler: nil)
                        }
                    }
                }
            }
        }
    }
}
