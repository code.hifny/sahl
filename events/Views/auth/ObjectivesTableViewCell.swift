//
//  ObjectivesTableViewCell.swift
//  events
//
//  Created by abdelrahman on 3/19/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class ObjectivesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func config(_ checked:Bool) {
        if (checked == true) {
            self.checkImageView.image = #imageLiteral(resourceName: "tick")
        }else{
            self.checkImageView.image = #imageLiteral(resourceName: "Black & White 1")
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
}
