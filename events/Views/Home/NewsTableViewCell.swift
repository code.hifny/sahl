//
//  NewsTableViewCell.swift
//  events
//
//  Created by abdelrahman on 2/13/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDescription: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    func config(_ news:News) {
        newsTitle.text = news.title
        newsDescription.text = news.descrip
        newsImage.af_setImage(withURL: URL(string: Config.imageUrl + "\(news.image ?? "")")!)
        newsDate.text = "Posted".locale + " " + (news.formatted_date ?? "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
