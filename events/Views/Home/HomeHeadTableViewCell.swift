//
//  HomeHeadTableViewCell.swift
//  events
//
//  Created by abdelrahman on 2/12/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
class HomeHeadTableViewCell: UITableViewCell {

    @IBOutlet weak var topViewGradiant: UIView!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventInfo: UILabel!
    @IBOutlet weak var goingImage1: UIImageView!
    @IBOutlet weak var goingImage2: UIImageView!
    @IBOutlet weak var goingImage3: UIImageView!
    @IBOutlet weak var going: UILabel!
    @IBOutlet weak var eventAddress: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    var event: Event!
    @IBOutlet weak var buttonTiketOutlet: EventsButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        let Header = UIView()
        let screenHeight = UIScreen.main.bounds.height
        switch screenHeight {
        case 568:  // 4    inch iphone5 (c/s/se)
            Header.frame = CGRect(x: 0, y: 0,width: self.topViewGradiant.bounds.size.width - 40, height: self.topViewGradiant.bounds.size.height)
            break
        default:
            Header.frame = CGRect(x:0, y: 0,width: self.topViewGradiant.bounds.size.width + 40 , height: self.topViewGradiant.bounds.size.height)
        }
        Header.backgroundColor =  UIColor.fromGradientWithDirection(.leftToRight,frame: Header.frame, colors:[ Color.blueForGradiantColor,Color.greenForGradiantColor])
        //        self.stretchyHeader.delegate = self // this is completely optional
        self.topViewGradiant.addSubview(Header)
//        topViewGradiant.backgroundColor = UIColor.fromGradientWithDirection(.leftToRight, frame: self.bounds)
        // Initialization code
    }
    
    
    // MARK: - Actions
    @IBAction func openInMap(_ sender: Any) {
    MapsHelper.instance.showActionSheet(fromLatitude: nil, fromLongitude: nil, toLatitude: Float((event.latitude ?? "0"))!, toLongitude: Float((event.longitude ?? "0"))!)

    }
    
    
    func config(_ event:Event) {
//        print(UserUtil.loadUser()?.ticket_info?.addressEn)
        if UserUtil.loadUser()?.ticket_info?.addressEn == nil{
            buttonTiketOutlet.setTitle("Buy Ticket".locale, for: .normal)
        }else{
            buttonTiketOutlet.setTitle("My Ticket".locale, for: .normal)
        }
        self.event = event
        eventTitle.text = event.title
        eventInfo.text = event.info
        eventDate.text = (event.formatted_date ?? "") + " | " +  (event.formatted_time ?? "" )
        eventAddress.text = event.address
        images(goingImage1, event:event , index:0)
        images(goingImage2, event:event , index:1)
        images(goingImage3, event:event , index:2)
        

        if event.going.count > 3 {
            going.text = " + \(event.going.count ) " + "Going".locale
        }else{
            print("event.going.count" , event.going_number)
            going.text = "\(event.going_number.value ?? 0) " + "Going".locale
        }
    }

    
    func images(_ imageView:UIImageView, event:Event , index:Int){
        imageView.isHidden = true
        if event.going.count > index{
            imageView.isHidden = false
            imageView.af_setImage(withURL: URL(string: Config.imageUrl + "\((event.going[index] ).image ?? "")")!)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
