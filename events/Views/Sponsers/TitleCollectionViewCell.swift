//
//  TitleCollectionViewCell.swift
//  events
//
//  Created by Macbook Pro on 2/22/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class TitleCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
}
