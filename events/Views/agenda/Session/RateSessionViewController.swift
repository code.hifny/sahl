//
//  RateSessionViewController.swift
//  events
//
//  Created by abdelrahman on 2/25/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit
import Cosmos
import Material
class RateSessionViewController: UIViewController {
    
    @IBOutlet weak var feedbackTextView: TextView!
    @IBOutlet weak var rateView: CosmosView!
    
    var rate: rateAction!
    var session_id:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("session_id",session_id)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.rate.showRate(rateCount: Double(self.rateView.rating) , type: "rateSessionPage")
    }
    

    @IBAction func sendFeedback(_ sender: Any) {
        //FIXME:- send your feedback
        self.StartLoading()
//        dismiss(animated: true, completion: nil)
        
        if rateView.rating == 0.0 {
            self.StopLoading()
            self.DangerAlert(message: "Rate is Requried".locale)
        }
        else
        {
            let param:[String:Any] = [
                "session_id":session_id,
                "feedback":feedbackTextView.text,
                "rate":rateView.rating,
                "api_token":UserUtil.loadUser()?.api_token ?? ""
            ]
            Requests.instance.request(
                link: "rate".route,
                method: .post,
                parameters: param) { (response) in
                    print("response===>",response)
                    switch response.haveError {
                    case true :
                        self.DangerAlert(message: response.error)
                        break
                    case false :
                        self.SuccessAlert(message: response.data?["data"]["message"][0].string ?? "rated successfully")
                        self.rate.showRate(rateCount: Double(self.rateView.rating) , type: "rateSessionPage")
                        UIApplication.topViewController()?.dismiss(animated: true, completion: nil)
                        break
                    }
                    self.StopLoading()
                    
            }
        }
    }
    
    @IBAction func dismissButton(_ sender: Any) {
//        self.rate.showRate(rateCount: Double(self.rateView.rating) ,type: "rateSessionPage")
       dismiss(animated: true, completion: nil)
    }
}

