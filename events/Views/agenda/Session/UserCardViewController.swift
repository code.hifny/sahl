//
//  UserCardViewController.swift
//  GarlandView
//
//  Copyright © 2017 Ramotion. All rights reserved.
//

import Foundation
import UIKit
import Cosmos

protocol rateAction {
    func showRate(rateCount: Double , type: String)
}

class UserCardViewController: UIViewController , rateAction {
    
    @IBOutlet weak var cardTop: NSLayoutConstraint!
    @IBOutlet var closeButton: UIButton!
    @IBOutlet open var garlandCardCollection: UICollectionView!
    @IBOutlet open var avatar: UIImageView!
    @IBOutlet open var card: UIView!
    @IBOutlet open var background: UIView!
    @IBOutlet open var headerImageView: UIImageView!
    @IBOutlet open var cardConstraits: [NSLayoutConstraint]!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var jobTitleLabel: UILabel!
    @IBOutlet weak var sessionNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var rate: CosmosView!
    var pageTyep: String!
    fileprivate let userCardPresentAnimationController = UserCardPresentAnimationController()
    fileprivate let userCardDismissAnimationController = UserCardDismissAnimationController()
    
    var agenda:Agenda?
    var countOFRate: Double?
    var typeOFPassData: String?
    var rateActionDelegate:  rateAction?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        modalPresentationStyle = .custom
        transitioningDelegate = self
        rate.isUserInteractionEnabled = false
        setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (agenda?.session_rate) ?? 0 > 0 {
            rateBtn.isUserInteractionEnabled = false
            rateBtn.setTitle("Your Rate".locale , for: .normal)
            self.rate.rating = Double((agenda?.session_rate) ?? 0)
            rate.alpha = 1
        }else{
            rateBtn.isUserInteractionEnabled = true
            rateBtn.setTitle("Rate".locale , for: .normal)
            self.rate.rating = Double((agenda?.session_rate) ?? 0)
            rate.alpha = 0
        }
        
        
    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.tabBarController?.tabBar.isHidden = false
//    }

    
    fileprivate func setupCard() {
        card.layer.cornerRadius = GarlandConfig.shared.cardRadius
        avatar.layer.masksToBounds = true
        avatar.layer.cornerRadius = avatar.frame.width/2
        avatar.layer.borderWidth = 3.0
        avatar.layer.borderColor = #colorLiteral(red: 0.6901960784, green: 0.8196078431, blue: 0.4588235294, alpha: 1)
    }
    
    func setup()  {
        if pageTyep == "MyAgeda"{
            closeButton.alpha = 0
            cardTop.constant = 74
        }else{
            closeButton.alpha = 1
            cardTop.constant = 140
        }

        setupCard()
        headerImageView.image = UIImage.fromGradientWithDirection(.leftToRight, frame: self.headerImageView.frame, colors:[ Color.blueForGradiantColor,Color.greenForGradiantColor])
        view.frame = UIScreen.main.bounds
        view.backgroundColor = .gray
        garlandCardCollection.bounces = true
        
        if UserUtil.loadUser() == nil {
            rateBtn.isHidden = true
            rate.isHidden = true
            notificationBtn.isHidden = true
        }
        
        avatar.af_setImage(withURL: URL(string: Config.imageUrl + (agenda?.speaker?.image ?? "")! )!)
        usernameLabel.text = agenda?.speaker?.username
        jobTitleLabel.text = agenda?.speaker?.job_title
        sessionNameLabel.text = agenda?.title
        dateLabel.text = agenda?.date
        let time_from = (agenda?.formatted_time_from ?? "") + " "
        let time_to =  " " + (agenda?.formatted_time_to ?? "")
        timeLabel.text = time_from +  "To".locale + time_to
        
        locationLabel.text = agenda?.location
        if Config.locale == .ar {
            closeButton.titleEdgeInsets.left = 25
            closeButton.titleEdgeInsets.right = -25
        }
        
        if agenda?.is_reminded ?? false {
            notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agendaa-notifications"), for: .normal)
        }else{
            notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agenda-notifications"), for: .normal)
        }
        
        closeButton.addTarget(self, action: #selector(closeButtonAction), for: .touchDown)
        
        let nib = UINib(nibName: "CardCollectionCell", bundle: nil)
        garlandCardCollection.register(nib, forCellWithReuseIdentifier: "CardCollectionCell")
        
    }
    
    func showRate(rateCount: Double , type: String) {
        self.countOFRate = rateCount
        self.typeOFPassData = type
        self.agenda?.session_rate = Int(countOFRate ?? 0.0)
        self.rate.rating = Double(countOFRate ?? 0)
        if rateCount != 0.0{
            rate.alpha = 1
            rateBtn.isUserInteractionEnabled = false
            rateBtn.setTitle("Your Rate".locale , for: .normal)
            self.rate.rating = Double((agenda?.session_rate) ?? 0)
        }
        
       

    }
    @IBAction func openRate(_ sender: Any) {
        let controller = RateSessionViewController.init(nibName: "RateSessionViewController", bundle: nil)
        controller.modalPresentationStyle = .custom
        controller.session_id = "\(agenda?.id ?? 0)"
        controller.modalTransitionStyle = .crossDissolve
        controller.rate = self
        self.present(controller, animated: true, completion: nil)
    }
    
    
    @IBAction func gotoSpeakerProfile(_ sender: UIButton) {
        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "SpeakerProfileViewController") as! SpeakerProfileViewController
        viewController.speakerInfo = agenda?.speaker
        viewController.pageType = "Agenda"
        let navigationController = EventsNavigationController(rootViewController: viewController)
        self.present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func addFavourite(_ sender: UIButton) {
        StartLoading()
        let sessionId = agenda?.id ?? 0
        print("sessionId",sessionId)
        Requests.instance.request(link: Config.apiUrl + "user/session/\(sessionId)/remind", method: .post ,parameters: ["api_token":UserUtil.loadUser()?.api_token ?? ""]) { (response) in
            if response.haveError{
                self.DangerAlert(message: response.error )
            }else{
                if response.data?["status"].bool == true {
                    let sessionId = "\(self.agenda?.id ?? 0)"
                    let sessionDateString = (self.agenda?.date ?? "") + " " + (self.agenda?.time_from ?? "")
                    let sessionTitle = "\(self.agenda?.title ?? "")"
                    let sessionDate = sessionDateString.timestamp
                    var sessionDescrip = ""
                    let dateBefore15Minute = Calendar.current.date(byAdding: .minute , value: -15, to: sessionDate)
                    let dateBefore5Minute = Calendar.current.date(byAdding: .minute, value: -5, to: sessionDate)
                    
                    if self.agenda?.is_reminded ?? true {
                        self.agenda?.is_reminded  = false
                        self.notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agenda-notifications"), for: .normal)
                        NotificationsUtil.remove(sessionId)
                        print("session remove ====>",sessionId)
//                        AgendaUtil.save(agenda)
                    }else{
                        self.agenda?.is_reminded  = true
                        self.notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agendaa-notifications"), for: .normal)
//                        AgendaUtil.save(self.agendaItems)
                        print("dateBefore5Minute" , dateBefore5Minute!)
                        NotificationsUtil.set(sessionId+"-5", title: sessionTitle, body: sessionDescrip, date: dateBefore5Minute!, userInfo: ["sessionId" : sessionId+"-5" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 5 minutes" , "created_at" : dateBefore5Minute ?? "" , "status": true , "type" : "local"])
                        
                        NotificationsUtil.set(sessionId+"-15", title: sessionTitle, body: sessionDescrip, date: dateBefore15Minute!, userInfo: [  "sessionId" : sessionId+"-15" ,"sessionTitle": sessionTitle , "sessionDescrip": "\(sessionTitle) will start after 15 minutes" , "created_at" : dateBefore5Minute ?? "" , "status": true , "type" : "local"])
                        
                        print("session add ====>",sessionId)
                    }
                }
            }
            self.StopLoading()
        }
    }
    
}


//MARK: Actions
extension UserCardViewController {
    @objc fileprivate func closeButtonAction() {
       UIApplication.topViewController()?.dismiss(animated: true, completion: nil)
    }
}


//MARK: UICollection view delegate & data source methods
extension UserCardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func getStringSizeForFont(font: UIFont, myText: String) -> CGSize {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = (myText as NSString).size(withAttributes: fontAttributes)
        
        return size
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let stringSizeAsText: CGSize = getStringSizeForFont(font: UIFont.systemFont(ofSize: 18), myText: (agenda?.info) ?? "")
        // label width and height are hardcoded here, but can easily be made dynamic based on the screen width
        // and the constant proportion of the width the UILabel will take up
        // the same across all devices
        let labelWidth: CGFloat = collectionView.bounds.width - 16
        let originalLabelHeight: CGFloat = 20
        // the label can only hold its width worth of text, so we can get the ammount of lines from a specific string this way
        let labelLines: CGFloat = CGFloat(ceil(Float(stringSizeAsText.width/labelWidth)))
        
        // each line will approximately take up the original label's height
        let height = originalLabelHeight + CGFloat(labelLines*stringSizeAsText.height)
        
        
        return CGSize(width: collectionView.bounds.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionCell", for: indexPath)
        guard let cell = collectionCell as? CardCollectionCell else {
            return UICollectionViewCell()
        }
        
        cell.contentMode = .scaleToFill
        cell.bioLabel.text = agenda?.info
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = indexPath.item
        print("Selected item #\(item)")
    }
}


//MARK: Transition delegate methods
extension UserCardViewController: UIViewControllerTransitioningDelegate {
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return userCardPresentAnimationController
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return userCardDismissAnimationController
    }
}
