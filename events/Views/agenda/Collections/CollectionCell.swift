//
//  CollectionCell.swift
//  GarlandView
//
//  Copyright © 2017 Ramotion. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications


class CollectionCell: UICollectionViewCell {
    
    @IBOutlet open var avatar: UIImageView!
    @IBOutlet open var timeLabel: UILabel!
    @IBOutlet open var nameLabel: UILabel!
    @IBOutlet open var sessionNameLabel: UILabel!
    @IBOutlet open var notificationBtn: UIButton!
    @IBOutlet open var statusView: UIView!

//    var type: String = "MyAgenda"

    open override func awakeFromNib() {
        super.awakeFromNib()
        if UserUtil.loadUser() == nil {
            notificationBtn.isHidden = true
        }
        setupUI()
    }
    
    func setupUI() {
        avatar.layer.masksToBounds = true
        contentView.layer.masksToBounds = false
        layer.masksToBounds = false
        
        let config = GarlandConfig.shared
//        if type == "MyAgenda"{
//            layer.borderWidth = 1
//            layer.borderColor = Color.grayColor.cgColor
//            layer.cornerRadius = 10
//
//        }else{
            layer.cornerRadius  = config.cardRadius
            layer.shadowOffset = config.cardShadowOffset
            layer.shadowColor = config.cardShadowColor.cgColor
            layer.shadowOpacity = config.cardShadowOpacity
            layer.shadowRadius = config.cardShadowRadius
            
            layer.shouldRasterize = true
            layer.rasterizationScale = UIScreen.main.scale
//        }
       
    }


    open override func layoutSubviews() {
        super.layoutSubviews()
        avatar.layer.cornerRadius = avatar.frame.width/2
    }
//     , typePage: String
    func config(_ data:Agenda) {
//        type = typePage

        if data.is_reminded ?? false {
            notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agendaa-notifications"), for: .normal)
        }else{
            notificationBtn.setBackgroundImage(#imageLiteral(resourceName: "agenda-notifications"), for: .normal)
        }
        
        avatar.af_setImage(withURL: URL(string: Config.imageUrl + (data.speaker?.image ?? "")! )!)
        
        let from = (data.formatted_time_from ?? "") + " "
        let to =  " " + (data.formatted_time_to ?? "")

        timeLabel.text = from +  "To".locale + to
        nameLabel.text = data.speaker?.username
        sessionNameLabel.text = data.title
        let now = Date()
        
        if NSCalendar.current.isDateInToday((data.date?.date) ?? now) || (data.date?.date) ?? now < now {
            let hours = data.time_from?.dropLast(6)
            let minutes = ("\(data.time_from?.dropLast(3) ?? "")").dropFirst(3)
//            print(hours)
//            print(minutes)
            let sessionTime = data.date?.date?.dateAt(hours: Int("\(hours ?? "")")!, minutes: Int("\( minutes)")!)
            print(sessionTime)
            if sessionTime! <= now {
                //myDate is earlier than Now (date and time)
                statusView.backgroundColor = Color.greenColor
            } else {
                //myDate is equal or after than Now (date and time)
                statusView.backgroundColor = Color.grayColor
            }
        }else{
            
            statusView.backgroundColor = Color.grayColor
        }

    }

}


extension Date
{
    func dateAt(hours: Int, minutes: Int) -> Date
    {
        let calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        
        //get the month/day/year componentsfor today's date.
        var date_components = calendar.components(
            [NSCalendar.Unit.year,
             NSCalendar.Unit.month,
             NSCalendar.Unit.day],
            from: self)
        
        //Create an NSDate for the specified time today.
        date_components.hour = hours
        date_components.minute = minutes
        date_components.second = 0
        
        let newDate = calendar.date(from: date_components)!
        return newDate
    }
}
