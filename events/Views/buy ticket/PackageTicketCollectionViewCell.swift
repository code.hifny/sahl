//
//  PackageTicketCollectionViewCell.swift
//  events
//
//  Created by Macbook Pro on 2/24/18.
//  Copyright © 2018 abdelrahman. All rights reserved.
//

import UIKit

class PackageTicketCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var typeOFTicket: UILabel!
    @IBOutlet weak var priceOfTicket: UILabel!
    
}
